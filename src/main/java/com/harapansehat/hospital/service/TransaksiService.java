package com.harapansehat.hospital.service;

import com.google.zxing.WriterException;
import com.harapansehat.hospital.dto.transaksi.InfoTransaksi;
import com.harapansehat.hospital.dto.transaksi.TransaksiRequest;
import com.harapansehat.hospital.dto.transaksi.TransaksiResponse;
import com.harapansehat.hospital.dto.transaksi.*;
import com.harapansehat.hospital.dto.treatment.DataRincianObat;
import com.harapansehat.hospital.entity.*;
import com.harapansehat.hospital.exception.BadRequestException;
import com.harapansehat.hospital.exception.ResourceNotFoundException;
import com.harapansehat.hospital.repository.*;
import com.harapansehat.hospital.util.QRCodeGenerator;
import com.harapansehat.hospital.util.ExceptionMessageAccessor;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class TransaksiService {

    private static final String TRANSACTION_ALREADY_EXISTS = "transaction_already_exists";
    private static final String TRANSACTION_DOES_NOT_EXIST = "transaction_does_not_exist";
    private static final String SURGERY_COST_NOT_EMPTY = "surgery_cost_not_empty";
    private static final String DAYS_OF_HOSPITALIZATION_NOT_EMPTY = "days_of_hospitalization_not_empty";
    private static final String STATUS_INCOMING = "Incoming";
    private final TransaksiRepository transaksiRepository;
    private final DokterService dokterService;
    private final PasienService pasienService;
    private final TreatmentService treatmentService;
    private final ResepsionisService resepsionisService;
    private final RincianObatService rincianObatService;
    private final ObatService obatService;
    private final ExceptionMessageAccessor exceptionMessageAccessor;

    public BigDecimal biayaObat(Integer tratmentId) {
        BigDecimal totalBiayaObat = new BigDecimal(0);
        for (DataRincianObat rincianObat : rincianObatService.getAllRincianObatByTreatmentId(tratmentId)) {
            Obat obat = obatService.obatExistValidation(rincianObat.getObatId());

            totalBiayaObat = totalBiayaObat.add(obat.getHarga().multiply(new BigDecimal(rincianObat.getKuantitas())));
        }

        return totalBiayaObat;
    }

    public Transaksi transaksiExistValidation(Integer transaksiId) {
        Transaksi transaksiExist = transaksiRepository.findById(transaksiId)
                .orElseThrow(() -> new ResourceNotFoundException(
                        exceptionMessageAccessor.getMessage(null, TRANSACTION_DOES_NOT_EXIST, transaksiId)));

        if (Boolean.TRUE.equals(transaksiExist.getIsDeleted())) {
            throw new ResourceNotFoundException(
                    exceptionMessageAccessor.getMessage(null, TRANSACTION_DOES_NOT_EXIST, transaksiId));
        }

        return transaksiExist;
    }

    public List<TransaksiResponse> getAllTransaksi() {
        List<TransaksiResponse> listTransaksi = new ArrayList<>();
        for (Transaksi transaksi : transaksiRepository.findAll()) {
            if (Boolean.FALSE.equals(transaksi.getIsDeleted())) {

                BigDecimal biayaLayanan = new BigDecimal(0);

                if (transaksi.getBiayaOperasi() != null && transaksi.getTotalHariRawatInap() != null) {
                    biayaLayanan = transaksi.getTreatment().getKamar().getHargaKamar().multiply(new BigDecimal(transaksi.getTotalHariRawatInap())).add(transaksi.getBiayaOperasi());
                } else if (transaksi.getTotalHariRawatInap() != null) {
                    biayaLayanan = transaksi.getTreatment().getKamar().getHargaKamar().multiply(new BigDecimal(transaksi.getTotalHariRawatInap()));
                }

                listTransaksi.add(TransaksiResponse.builder()
                        .id(transaksi.getId())
                        .treatmentId(transaksi.getTreatment().getId())
                        .pasienId(transaksi.getTreatment().getPasien().getId())
                        .dokterId(transaksi.getTreatment().getDokter().getId())
                        .resepsionisId(transaksi.getResepsionis().getId())
                        .namaResepsionis(transaksi.getResepsionis().getNamaResepsionis())
                        .biayaAdmin(transaksi.getBiayaAdmin())
                        .biayaObat(biayaObat(transaksi.getTreatment().getId()))
                        .biayaDokter(transaksi.getTreatment().getDokter().getSpesialisasi().getHarga())
                        .biayaLayanan(biayaLayanan)
                        .tanggalTransaksi(transaksi.getTanggalTransaksi())
                        .build());
            }
        }
        return listTransaksi;
    }

    public List<TransaksiResponse> getAllTransaksiByPasienId(Integer pasienId) {
        List<TransaksiResponse> listTransaksi = new ArrayList<>();
        for (Transaksi transaksi : transaksiRepository.findAll()) {
            if (Boolean.FALSE.equals(transaksi.getIsDeleted()) && transaksi.getTreatment().getPasien().getId().equals(pasienId)) {

                BigDecimal biayaLayanan = new BigDecimal(0);

                if (transaksi.getBiayaOperasi() != null && transaksi.getTotalHariRawatInap() != null) {
                    biayaLayanan = transaksi.getTreatment().getKamar().getHargaKamar().multiply(new BigDecimal(transaksi.getTotalHariRawatInap())).add(transaksi.getBiayaOperasi());
                } else if (transaksi.getTotalHariRawatInap() != null) {
                    biayaLayanan = transaksi.getTreatment().getKamar().getHargaKamar().multiply(new BigDecimal(transaksi.getTotalHariRawatInap()));
                }

                listTransaksi.add(TransaksiResponse.builder()
                        .id(transaksi.getId())
                        .treatmentId(transaksi.getTreatment().getId())
                        .pasienId(transaksi.getTreatment().getPasien().getId())
                        .dokterId(transaksi.getTreatment().getDokter().getId())
                        .resepsionisId(transaksi.getResepsionis().getId())
                        .namaResepsionis(transaksi.getResepsionis().getNamaResepsionis())
                        .biayaAdmin(transaksi.getBiayaAdmin())
                        .biayaObat(biayaObat(transaksi.getTreatment().getId()))
                        .biayaDokter(transaksi.getTreatment().getDokter().getSpesialisasi().getHarga())
                        .biayaLayanan(biayaLayanan)
                        .tanggalTransaksi(transaksi.getTanggalTransaksi())
                        .build());
            }
        }
        return listTransaksi;
    }

    public InfoTransaksi getTransaksiById(Integer transaksiId) throws IOException, WriterException {
        Transaksi transaksi = transaksiExistValidation(transaksiId);
        Treatment treatment = treatmentService.treatmentExistValidation(transaksi.getTreatment().getId());
        Pasien pasien = pasienService.pasienExistValidation(treatment.getPasien().getId());
        Dokter dokter = dokterService.dokterExistValidation(treatment.getDokter().getId());
        Resepsionis resepsionis = resepsionisService.resepsionisExistValidation(transaksi.getResepsionis().getId());
        List<RincianTransaksiObat> rincianObat = rincianObatService.getAllRincianTransaksiObatByTreatmentId(treatment.getId());

        return InfoTransaksi.builder()
                .nik(pasien.getNik())
                .namaPasien(pasien.getNamaPasien())
                .alamat(pasien.getAlamat())
                .namaDokter(dokter.getNamaDokter())
                .namaSpesialis(dokter.getSpesialisasi().getNamaSpesialis())
                .namaResepsionis(resepsionis.getNamaResepsionis())
                .tanggalTransaksi(transaksi.getTanggalTransaksi())
                .biayaAdmin(transaksi.getBiayaAdmin())
                .biayaDokter(dokter.getSpesialisasi().getHarga())
                .biayaOperasi(transaksi.getBiayaOperasi())
                .kamar(treatment.getKamar())
                .totalHariRawatInap(transaksi.getTotalHariRawatInap())
                .obat(rincianObat)
                .qrCode(generateQRCodebyId(transaksiId))
                .build();
    }

    public TransaksiResponse addTransaksi(TransaksiRequest req){
        Treatment treatment = treatmentService.treatmentExistValidation(req.getTreatmentId());
        Dokter dokter = dokterService.dokterExistValidation(treatment.getDokter().getId());
        Resepsionis resepsionis = resepsionisService.resepsionisExistValidation(req.getResepsionisId());

        Boolean transaksiExists = transaksiRepository.existsByTreatmentId(treatment.getId());

        if (Boolean.TRUE.equals(transaksiExists)) {
            throw new BadRequestException(TRANSACTION_ALREADY_EXISTS);
        }

        if (treatment.getStatus().equals(STATUS_INCOMING)) {
            throw new BadRequestException("The treatment has not been completed yet");
        }

        if (treatment.getLayanan().getNamaLayanan().equals("Operasi") && req.getBiayaOperasi() == null) {
            throw new BadRequestException(SURGERY_COST_NOT_EMPTY);
        }

        if (treatment.getKamar() != null && req.getTotalHariRawatInap() == null) {
            throw new BadRequestException(DAYS_OF_HOSPITALIZATION_NOT_EMPTY);
        }

        Transaksi transaksi = new Transaksi();
        transaksi.setTreatment(treatment);
        transaksi.setResepsionis(resepsionis);
        transaksi.setBiayaAdmin(req.getBiayaAdmin());
        transaksi.setBiayaOperasi(req.getBiayaOperasi());
        transaksi.setTotalHariRawatInap(req.getTotalHariRawatInap());
        transaksi.setTanggalTransaksi(new Date());
        transaksi.setIsDeleted(false);
        transaksiRepository.save(transaksi);

        BigDecimal biayaLayanan = new BigDecimal(0);

        if (transaksi.getBiayaOperasi() != null && transaksi.getTotalHariRawatInap() != null) {
            biayaLayanan = transaksi.getTreatment().getKamar().getHargaKamar().multiply(new BigDecimal(req.getTotalHariRawatInap())).add(req.getBiayaOperasi());
        } else if (transaksi.getTotalHariRawatInap() != null) {
            biayaLayanan = transaksi.getTreatment().getKamar().getHargaKamar().multiply(new BigDecimal(req.getTotalHariRawatInap()));
        }

        return TransaksiResponse.builder()
                .id(transaksi.getId())
                .treatmentId(transaksi.getTreatment().getId())
                .pasienId(transaksi.getTreatment().getPasien().getId())
                .dokterId(transaksi.getTreatment().getDokter().getId())
                .resepsionisId(transaksi.getResepsionis().getId())
                .namaResepsionis(transaksi.getResepsionis().getNamaResepsionis())
                .biayaAdmin(transaksi.getBiayaAdmin())
                .biayaObat(biayaObat(treatment.getId()))
                .biayaDokter(dokter.getSpesialisasi().getHarga())
                .biayaLayanan(biayaLayanan)
                .tanggalTransaksi(transaksi.getTanggalTransaksi())
                .build();
    }
    
    public  byte [] generateQRCodebyId(Integer id) throws IOException, WriterException{
        Optional<Transaksi> optionalTransaksi = transaksiRepository.findById(id);
        if (optionalTransaksi.isPresent()) {
            Transaksi transaksi = optionalTransaksi.get();
            String text = "ID Transaksi : " + transaksi.getId() + "\nID Treatment : " + transaksi.getTreatment() + "\nTanggal : " + transaksi.getTanggalTransaksi();
            return  QRCodeGenerator.generateQRCode(text);
        } else {
            return new byte[0];
        }
    }
}
