package com.harapansehat.hospital.service;

import com.harapansehat.hospital.dto.dokter.DokterRequest;
import com.harapansehat.hospital.dto.pasien.PasienRequest;
import com.harapansehat.hospital.dto.resepsionis.ResepsionisRequest;
import com.harapansehat.hospital.exception.BadRequestException;
import com.harapansehat.hospital.repository.DokterRepository;
import com.harapansehat.hospital.repository.PasienRepository;
import com.harapansehat.hospital.repository.UserRepository;
import com.harapansehat.hospital.util.ExceptionMessageAccessor;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class UserValidationService {

    private static final String NIP_ALREADY_EXISTS = "nip_already_exists";
    private static final String NIK_ALREADY_EXISTS = "nik_already_exists";
    private static final String USERNAME_ALREADY_EXISTS = "username_already_exists";
    private static final String NIK_VALIDATION = "nik_validation";

    private final DokterRepository dokterRepository;
    private final PasienRepository pasienRepository;
    private final UserRepository userRepository;

    private final ExceptionMessageAccessor exceptionMessageAccessor;

    public void validateDokter(DokterRequest request) {

        final String nip = request.getNip();

        checkNip(nip);
    }

    private void checkNip(String nip) {

        final boolean existsByNip = dokterRepository.existsByNip(nip);

        if (existsByNip) {

            final String existsNip = exceptionMessageAccessor.getMessage(null, NIP_ALREADY_EXISTS);
            throw new BadRequestException(existsNip);
        }

    }

    public void validatePasien(PasienRequest request) {

        final String nik = request.getNik();

        if (nik.matches("\\d{16}")) {
            throw new BadRequestException(NIK_VALIDATION);
        }

        checkNik(nik);
    }

    private void checkNik(String nik) {

        final boolean existsByNik = pasienRepository.existsByNik(nik);

        if (existsByNik) {

            final String existsNik = exceptionMessageAccessor.getMessage(null, NIK_ALREADY_EXISTS);
            throw new BadRequestException(existsNik);
        }

    }

    public void validateResepsionis(ResepsionisRequest request) {

        final String username = request.getUsername();

        checkUsername(username);
    }

    private void checkUsername(String username) {

        final boolean existsByUsername = userRepository.existsByUsername(username);

        if (existsByUsername) {

            final String existsUsername = exceptionMessageAccessor.getMessage(null, USERNAME_ALREADY_EXISTS);
            throw new BadRequestException(existsUsername);
        }

    }
}
