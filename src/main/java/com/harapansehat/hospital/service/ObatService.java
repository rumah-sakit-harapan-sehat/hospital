package com.harapansehat.hospital.service;

import com.harapansehat.hospital.dto.obat.ObatRequest;
import com.harapansehat.hospital.dto.obat.ObatResponse;
import com.harapansehat.hospital.entity.Obat;
import com.harapansehat.hospital.exception.BadRequestException;
import com.harapansehat.hospital.exception.ResourceNotFoundException;
import com.harapansehat.hospital.repository.ObatRepository;
import com.harapansehat.hospital.util.ExceptionMessageAccessor;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class ObatService {

    private static final String MEDICINE_NAME_ALREADY_EXISTS = "medicine_name_already_exists";
    private static final String MEDICINE_DOES_NOT_EXIST = "medicine_does_not_exist";
    private final ObatRepository obatRepository;
    private final ExceptionMessageAccessor exceptionMessageAccessor;

    public void validateNamaObat(ObatRequest request) {

        final String namaObat = request.getNamaObat();

        checkNamaObat(namaObat);
    }

    public void checkNamaObat(String namaObat) {

        final boolean existsByNamaObat = obatRepository.existsByNamaObatIgnoreCase(namaObat);

        if (existsByNamaObat) {
            final String message = exceptionMessageAccessor.getMessage(null, MEDICINE_NAME_ALREADY_EXISTS);
            throw new BadRequestException(message);
        }
    }

    public Obat obatExistValidation(Integer obatId) {
        Obat obatExist = obatRepository.findById(obatId)
                .orElseThrow(() -> new ResourceNotFoundException(
                        exceptionMessageAccessor.getMessage(null, MEDICINE_DOES_NOT_EXIST, obatId)));

        if (Boolean.TRUE.equals(obatExist.getIsDeleted())) {
            throw new ResourceNotFoundException(
                    exceptionMessageAccessor.getMessage(null, MEDICINE_DOES_NOT_EXIST, obatId));
        }

        return obatExist;
    }

    public List<ObatResponse> getAllObat() {
        List<ObatResponse> listObat = new ArrayList<>();
        for (Obat obat : obatRepository.findAll()){
            if (Boolean.FALSE.equals(obat.getIsDeleted())) {
                listObat.add(ObatResponse.builder()
                        .id(obat.getId())
                        .namaObat(obat.getNamaObat())
                        .jenisObat(obat.getNamaObat())
                        .harga(obat.getHarga())
                        .keterangan(obat.getKeterangan())
                        .isDeleted(obat.getIsDeleted())
                        .build());
            }
        }
        return listObat;
    }

    public Obat getObatById(Integer id) {
        return obatExistValidation(id);
    }

    public ObatResponse addObat(ObatRequest req){
        Obat obat = new Obat();
        validateNamaObat(req);
        obat.setNamaObat(req.getNamaObat());
        obat.setJenisObat(req.getJenisObat());
        obat.setHarga(req.getHarga());
        obat.setKeterangan(req.getKeterangan());
        obat.setIsDeleted(false);
        obatRepository.save(obat);

        return ObatResponse.builder()
                .id(obat.getId())
                .namaObat(obat.getNamaObat())
                .jenisObat(obat.getJenisObat())
                .harga(obat.getHarga())
                .keterangan(obat.getKeterangan())
                .isDeleted(obat.getIsDeleted())
                .build();
    }

    public ObatResponse updateObat(Integer id, ObatRequest req) {
        Obat obat = obatExistValidation(id);
        obat.setNamaObat(req.getNamaObat());
        obat.setJenisObat(req.getJenisObat());
        obat.setHarga(req.getHarga());
        obat.setKeterangan(req.getKeterangan());
        obat.setIsDeleted(false);
        obatRepository.save(obat);

        return ObatResponse.builder()
                .id(obat.getId())
                .namaObat(obat.getNamaObat())
                .jenisObat(obat.getJenisObat())
                .harga(obat.getHarga())
                .keterangan(obat.getKeterangan())
                .isDeleted(obat.getIsDeleted())
                .build();
    }

    public ObatResponse deleteObat(Integer id) {
        Obat obat = obatExistValidation(id);
        obat.setIsDeleted(true);
        obatRepository.save(obat);

        return ObatResponse.builder()
                .id(obat.getId())
                .namaObat(obat.getNamaObat())
                .jenisObat(obat.getJenisObat())
                .harga(obat.getHarga())
                .keterangan(obat.getKeterangan())
                .isDeleted(obat.getIsDeleted())
                .build();
    }
}
