package com.harapansehat.hospital.service;

import com.google.zxing.WriterException;
import com.harapansehat.hospital.dto.treatment.*;
import com.harapansehat.hospital.dto.treatment.DataRincianObat;
import com.harapansehat.hospital.dto.visit.VisitRequest;
import com.harapansehat.hospital.dto.visit.VisitResponse;
import com.harapansehat.hospital.entity.*;
import com.harapansehat.hospital.exception.BadRequestException;
import com.harapansehat.hospital.exception.ResourceNotFoundException;
import com.harapansehat.hospital.repository.*;
import com.harapansehat.hospital.util.ExceptionMessageAccessor;
import com.harapansehat.hospital.util.QRCodeGenerator;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class TreatmentService {

    private static final String TREATMENT_DOES_NOT_EXIST = "treatment_does_not_exist";
    private static final String ROOM_ID_NOT_EMPTY = "room_id_not_empty";
    private static final String STATUS_INCOMING = "Incoming";
    private static final String STATUS_COMPLETE = "Complete";
    private final TreatmentRepository treatmentRepository;
    private final RincianObatRepository rincianObatRepository;
    private final RekamMedisRepository rekamMedisRepository;
    private final RincianObatService rincianObatService;
    private final DokterService dokterService;
    private final PasienService pasienService;
    private final LayananService layananService;
    private final KamarService kamarService;
    private final ObatService obatService;
    private final ExceptionMessageAccessor exceptionMessageAccessor;

    public Treatment treatmentExistValidation(Integer treatmentId) {
        Treatment treatmentExist = treatmentRepository.findById(treatmentId)
                .orElseThrow(() -> new ResourceNotFoundException(
                        exceptionMessageAccessor.getMessage(null, TREATMENT_DOES_NOT_EXIST, treatmentId)));

        if (Boolean.TRUE.equals(treatmentExist.getIsDeleted())) {
            throw new ResourceNotFoundException(
                    exceptionMessageAccessor.getMessage(null, TREATMENT_DOES_NOT_EXIST, treatmentId));
        }

        return treatmentExist;
    }

    public List<TreatmentResponse> getAllTreatment() {
        List<TreatmentResponse> listTreatment = new ArrayList<>();
        for (Treatment treatment : treatmentRepository.findAll()) {
            if (Boolean.FALSE.equals(treatment.getIsDeleted())) {
                if (treatment.getStatus().equalsIgnoreCase(STATUS_INCOMING)) {
                    listTreatment.add(TreatmentResponse.builder()
                            .id(treatment.getId())
                            .dokterId(treatment.getDokter().getId())
                            .namaDokter(treatment.getDokter().getNamaDokter())
                            .spesialisasi(treatment.getDokter().getSpesialisasi().getNamaSpesialis())
                            .pasienId(treatment.getPasien().getId())
                            .namaPasien(treatment.getPasien().getNamaPasien())
                            .tanggalKonsul(treatment.getTanggalKonsul())
                            .status(treatment.getStatus())
                            .build());
                } else {
                    listTreatment.add(TreatmentResponse.builder()
                            .id(treatment.getId())
                            .dokterId(treatment.getDokter().getId())
                            .namaDokter(treatment.getDokter().getNamaDokter())
                            .spesialisasi(treatment.getDokter().getSpesialisasi().getNamaSpesialis())
                            .pasienId(treatment.getPasien().getId())
                            .namaPasien(treatment.getPasien().getNamaPasien())
                            .obat(rincianObatService.getAllRincianObatByTreatmentId(treatment.getId()))
                            .layananId(treatment.getLayanan().getId())
                            .diagnosis(treatment.getDiagnosis())
                            .tanggalKonsul(treatment.getTanggalKonsul())
                            .pesan(treatment.getPesan())
                            .status(treatment.getStatus())
                            .build());
                }
            }
        }
        return listTreatment;
    }

    public List<TreatmentResponse> getAllTreatmentByDokterId(Integer dokterId) {
        List<TreatmentResponse> listTreatment = new ArrayList<>();
        for (Treatment treatment : treatmentRepository.findAll()) {
            if (treatment.getDokter().getId().equals(dokterId)) {
                if (treatment.getStatus().equalsIgnoreCase(STATUS_INCOMING)) {
                    listTreatment.add(TreatmentResponse.builder()
                            .id(treatment.getId())
                            .dokterId(treatment.getDokter().getId())
                            .namaDokter(treatment.getDokter().getNamaDokter())
                            .spesialisasi(treatment.getDokter().getSpesialisasi().getNamaSpesialis())
                            .pasienId(treatment.getPasien().getId())
                            .namaPasien(treatment.getPasien().getNamaPasien())
                            .tanggalKonsul(treatment.getTanggalKonsul())
                            .status(treatment.getStatus())
                            .build());
                } else {
                    listTreatment.add(TreatmentResponse.builder()
                            .id(treatment.getId())
                            .dokterId(treatment.getDokter().getId())
                            .namaDokter(treatment.getDokter().getNamaDokter())
                            .spesialisasi(treatment.getDokter().getSpesialisasi().getNamaSpesialis())
                            .pasienId(treatment.getPasien().getId())
                            .namaPasien(treatment.getPasien().getNamaPasien())
                            .obat(rincianObatService.getAllRincianObatByTreatmentId(treatment.getId()))
                            .layananId(treatment.getLayanan().getId())
                            .diagnosis(treatment.getDiagnosis())
                            .tanggalKonsul(treatment.getTanggalKonsul())
                            .pesan(treatment.getPesan())
                            .status(treatment.getStatus())
                            .build());
                }
            }
        }
        return listTreatment;
    }

    public List<TreatmentResponse> getAllTreatmentByPasienId(Integer pasienId) {
        List<TreatmentResponse> listTreatment = new ArrayList<>();
        for (Treatment treatment : treatmentRepository.findAll()) {
            if (treatment.getPasien().getId().equals(pasienId)) {
                if (treatment.getStatus().equalsIgnoreCase(STATUS_INCOMING)) {
                    listTreatment.add(TreatmentResponse.builder()
                            .id(treatment.getId())
                            .dokterId(treatment.getDokter().getId())
                            .namaDokter(treatment.getDokter().getNamaDokter())
                            .spesialisasi(treatment.getDokter().getSpesialisasi().getNamaSpesialis())
                            .pasienId(treatment.getPasien().getId())
                            .namaPasien(treatment.getPasien().getNamaPasien())
                            .tanggalKonsul(treatment.getTanggalKonsul())
                            .status(treatment.getStatus())
                            .build());
                } else {
                    listTreatment.add(TreatmentResponse.builder()
                            .id(treatment.getId())
                            .dokterId(treatment.getDokter().getId())
                            .namaDokter(treatment.getDokter().getNamaDokter())
                            .spesialisasi(treatment.getDokter().getSpesialisasi().getNamaSpesialis())
                            .pasienId(treatment.getPasien().getId())
                            .namaPasien(treatment.getPasien().getNamaPasien())
                            .obat(rincianObatService.getAllRincianObatByTreatmentId(treatment.getId()))
                            .layananId(treatment.getLayanan().getId())
                            .diagnosis(treatment.getDiagnosis())
                            .tanggalKonsul(treatment.getTanggalKonsul())
                            .pesan(treatment.getPesan())
                            .status(treatment.getStatus())
                            .build());
                }
            }
        }
        return listTreatment;
    }

    public Treatment getTreatmentById(Integer id) {
        return treatmentExistValidation(id);
    }

    public DetailTreatment getDetailTreatment(Integer pasienId){
        Pasien pasien = pasienService.pasienExistValidation(pasienId);
        List<InfoTreatment> treatmentList = treatmentRepository.findDetailTreatment(pasienId);
        for (InfoTreatment infoTreatment : treatmentList) {
            List<DataRincianObat> rincianObat = rincianObatService.getAllRincianObatByTreatmentId(infoTreatment.getTreatmentId());
            infoTreatment.setObat(rincianObat);
        }
        return DetailTreatment.builder()
                .namaPasien(pasien.getNamaPasien())
                .tanggalLahir(pasien.getTanggalLahir())
                .jenisKelamin(pasien.getJenisKelamin())
                .alamat(pasien.getAlamat())
                .listTreatment(treatmentList)
                .build();
    }

    public VisitResponse addTreatment(VisitRequest req) {
        Dokter dokterExist = dokterService.dokterExistValidation(req.getDokterId());
        Pasien pasienExist = pasienService.pasienExistValidation(req.getPasienId());

        Treatment treatment = new Treatment();
        treatment.setDokter(dokterExist);
        treatment.setPasien(pasienExist);
        treatment.setTanggalKonsul(req.getTanggalKonsul());
        treatment.setStatus(STATUS_INCOMING);
        treatment.setIsDeleted(false);
        treatmentRepository.save(treatment);

        return VisitResponse.builder()
                .visitId(treatment.getId())
                .dokterId(treatment.getDokter().getId())
                .pasienId(treatment.getPasien().getId())
                .tanggalKonsul(treatment.getTanggalKonsul())
                .build();
    }

    @Transactional
    public TreatmentResponse updateTreatment(Integer id, TreatmentRequest req) {
        Treatment treatment = treatmentExistValidation(id);
        Dokter dokterExist = dokterService.dokterExistValidation(req.getDokterId());
        Pasien pasienExist = pasienService.pasienExistValidation(req.getPasienId());
        Layanan layananExist = layananService.layananExistValidation(req.getLayananId());

        if (treatment.getStatus().equalsIgnoreCase(STATUS_COMPLETE)) {
            throw new BadRequestException("Consultation and treatment have been carried out");
        }

        if ((layananExist.getNamaLayanan().equalsIgnoreCase("rawat inap") || layananExist.getNamaLayanan().equalsIgnoreCase("operasi")) && req.getKamarId() == null) {
            throw new BadRequestException(ROOM_ID_NOT_EMPTY);
        }

        if (req.getKamarId() != null) {
            Kamar kamarExist = kamarService.kamarExistValidation(req.getKamarId());
            treatment.setKamar(kamarExist);
        }

        for (DataRincianObat obat : req.getObat()) {
            Obat obatRequest = obatService.getObatById(obat.getObatId());

            com.harapansehat.hospital.entity.RincianObat rincianObat = new com.harapansehat.hospital.entity.RincianObat();
            rincianObat.setTreatment(treatment);
            rincianObat.setObat(obatRequest);
            rincianObat.setKuantitas(obat.getKuantitas());
            rincianObatRepository.save(rincianObat);
        }

        treatment.setId(id);
        treatment.setDokter(dokterExist);
        treatment.setPasien(pasienExist);
        treatment.setLayanan(layananExist);
        treatment.setDiagnosis(req.getDiagnosis());
        treatment.setPesan(req.getPesan());
        treatment.setStatus(STATUS_COMPLETE);
        treatment.setTanggalKonsul(req.getTanggalKonsul());
        treatment.setIsDeleted(false);
        treatmentRepository.save(treatment);

        return TreatmentResponse.builder()
                .id(treatment.getId())
                .dokterId(treatment.getDokter().getId())
                .namaDokter(treatment.getDokter().getNamaDokter())
                .spesialisasi(treatment.getDokter().getSpesialisasi().getNamaSpesialis())
                .pasienId(treatment.getPasien().getId())
                .namaPasien(treatment.getPasien().getNamaPasien())
                .obat(rincianObatService.getAllRincianObatByTreatmentId(treatment.getId()))
                .layananId(treatment.getLayanan().getId())
                .diagnosis(treatment.getDiagnosis())
                .pesan(treatment.getPesan())
                .status(treatment.getStatus())
                .tanggalKonsul(treatment.getTanggalKonsul())
                .build();
    }

    public  byte [] generateQRCodebyTreatmenntId(Integer id) throws IOException, WriterException {
        Optional<Treatment> optionalTreatment = treatmentRepository.findById(id);
        if (optionalTreatment.isPresent()) {
            Treatment treatment = optionalTreatment.get();
            Optional<RekamMedis> optionalRekamMedis = rekamMedisRepository.findByTreatmentId(id);
            if (optionalRekamMedis.isPresent()) {
                String text = "ID Treatment : " + treatment.getId() + "\nTreatment : " + treatment + "\nRekam Medis : " + optionalRekamMedis.get();
                return  QRCodeGenerator.generateQRCode(text);
            } else {
                String text = "ID Treatment : " + treatment.getId() + "\nTreatment : " + treatment;
                return  QRCodeGenerator.generateQRCode(text);
            }

        } else {
            return new byte[0];
        }
    }
}
