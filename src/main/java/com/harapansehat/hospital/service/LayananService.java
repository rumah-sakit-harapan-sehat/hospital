package com.harapansehat.hospital.service;

import com.harapansehat.hospital.dto.layanan.LayananRequest;
import com.harapansehat.hospital.dto.layanan.LayananResponse;
import com.harapansehat.hospital.entity.Layanan;
import com.harapansehat.hospital.exception.BadRequestException;
import com.harapansehat.hospital.exception.ResourceNotFoundException;
import com.harapansehat.hospital.repository.LayananRepository;
import com.harapansehat.hospital.util.ExceptionMessageAccessor;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class LayananService {

    private static final String SERVICE_NAME_ALREADY_EXISTS = "service_name_already_exists";
    private static final String SERVICE_DOES_NOT_EXIST = "service_does_not_exist";
    private final LayananRepository layananRepository;
    private final ExceptionMessageAccessor exceptionMessageAccessor;

    public void validateNamaLayanan(LayananRequest request) {

        final String namaLayanan = request.getNamaLayanan();

        checkNamaLayanan(namaLayanan);
    }

    private void checkNamaLayanan(String namaLayanan) {

        final boolean existsByNamaLayanan = layananRepository.existsByNamaLayananIgnoreCase(namaLayanan);

        if (existsByNamaLayanan) {
            final String message = exceptionMessageAccessor.getMessage(null, SERVICE_NAME_ALREADY_EXISTS);
            throw new BadRequestException(message);
        }
    }

    public Layanan layananExistValidation(Integer layananId) {
        Layanan layananExist = layananRepository.findById(layananId)
                .orElseThrow(() -> new ResourceNotFoundException(
                        exceptionMessageAccessor.getMessage(null, SERVICE_DOES_NOT_EXIST, layananId)));

        if (Boolean.TRUE.equals(layananExist.getIsDeleted())) {
            throw new ResourceNotFoundException(
                    exceptionMessageAccessor.getMessage(null, SERVICE_DOES_NOT_EXIST, layananId));
        }

        return layananExist;
    }

    public List<LayananResponse> getAllLayanan(){
        List<LayananResponse> listLayanan = new ArrayList<>();
        for (Layanan layanan : layananRepository.findAll()){
            if (Boolean.FALSE.equals(layanan.getIsDeleted())) {
                listLayanan.add(LayananResponse.builder()
                        .id(layanan.getId())
                        .namaLayanan(layanan.getNamaLayanan())
                        .isDeleted(layanan.getIsDeleted())
                        .build());
            }
        }
        return listLayanan;
    }

    public Layanan getLayananById(Integer id) {
        return layananExistValidation(id);
    }

    public LayananResponse addLayanan(LayananRequest req){
        Layanan layanan = new Layanan();
        validateNamaLayanan(req);
        layanan.setNamaLayanan(req.getNamaLayanan());
        layanan.setIsDeleted(false);
        layananRepository.save(layanan);

        return LayananResponse.builder()
                .id(layanan.getId())
                .namaLayanan(layanan.getNamaLayanan())
                .isDeleted(layanan.getIsDeleted())
                .build();
    }

    public LayananResponse updateLayanan(Integer id, LayananRequest req) {
        Layanan layanan = layananExistValidation(id);
        layanan.setNamaLayanan(req.getNamaLayanan());
        layanan.setIsDeleted(false);
        layananRepository.save(layanan);

        return LayananResponse.builder()
                .id(layanan.getId())
                .namaLayanan(layanan.getNamaLayanan())
                .isDeleted(layanan.getIsDeleted())
                .build();
    }

    public LayananResponse deleteLayanan(Integer id) {
        Layanan layanan = layananExistValidation(id);
        layanan.setIsDeleted(true);
        layananRepository.save(layanan);

        return LayananResponse.builder()
                .id(layanan.getId())
                .namaLayanan(layanan.getNamaLayanan())
                .isDeleted(layanan.getIsDeleted())
                .build();
    }
}