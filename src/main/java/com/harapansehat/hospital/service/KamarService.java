package com.harapansehat.hospital.service;

import com.harapansehat.hospital.dto.kamar.KamarRequest;
import com.harapansehat.hospital.dto.kamar.KamarResponse;
import com.harapansehat.hospital.entity.Kamar;
import com.harapansehat.hospital.exception.BadRequestException;
import com.harapansehat.hospital.exception.ResourceNotFoundException;
import com.harapansehat.hospital.repository.KamarRepository;
import com.harapansehat.hospital.util.ExceptionMessageAccessor;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class KamarService {

    private static final String ROOM_TYPE_ALREADY_EXISTS = "room_type_already_exists";
    private static final String ROOM_DOES_NOT_EXIST = "room_does_not_exist";
    private final KamarRepository kamarRepository;
    private final ExceptionMessageAccessor exceptionMessageAccessor;


    public void validateJenisKamar(KamarRequest request) {

        final String jenisKamar = request.getJenisKamar();

        checkJenisKamar(jenisKamar);
    }

    private void checkJenisKamar(String jenisKamar) {

        final boolean existsByJenisKamar = kamarRepository.existsByJenisKamarIgnoreCase(jenisKamar);

        if (existsByJenisKamar) {
            final String message = exceptionMessageAccessor.getMessage(null, ROOM_TYPE_ALREADY_EXISTS);
            throw new BadRequestException(message);
        }
    }

    public Kamar kamarExistValidation(Integer kamarId) {
        Kamar kamarExist = kamarRepository.findById(kamarId)
                .orElseThrow(() -> new ResourceNotFoundException(
                        exceptionMessageAccessor.getMessage(null, ROOM_DOES_NOT_EXIST, kamarId)));

        if (Boolean.TRUE.equals(kamarExist.getIsDeleted())) {
            throw new ResourceNotFoundException(
                    exceptionMessageAccessor.getMessage(null, ROOM_DOES_NOT_EXIST, kamarId));
        }

        return kamarExist;
    }

    public List<KamarResponse> getAllKamar() {
        List<KamarResponse> listKamar = new ArrayList<>();
        for (Kamar kamar : kamarRepository.findAll()){
            if (Boolean.FALSE.equals(kamar.getIsDeleted())) {
                listKamar.add(KamarResponse.builder()
                        .id(kamar.getId())
                        .jenisKamar(kamar.getJenisKamar())
                        .hargaKamar(kamar.getHargaKamar())
                        .deskripsi(kamar.getDeskripsi())
                        .isDeleted(kamar.getIsDeleted())
                        .build());
            }
        }
        return listKamar;
    }

    public Kamar getKamarById(Integer id) {
        return kamarExistValidation(id);
    }

    public KamarResponse addKamar(KamarRequest req) {
        Kamar kamar = new Kamar();
        validateJenisKamar(req);
        kamar.setJenisKamar(req.getJenisKamar());
        kamar.setHargaKamar(req.getHargaKamar());
        kamar.setDeskripsi(req.getDeskripsi());
        kamar.setIsDeleted(false);
        kamarRepository.save(kamar);

        return KamarResponse.builder()
                .id(kamar.getId())
                .jenisKamar(kamar.getJenisKamar())
                .hargaKamar(kamar.getHargaKamar())
                .deskripsi(kamar.getDeskripsi())
                .isDeleted(kamar.getIsDeleted())
                .build();
    }

    public KamarResponse updateKamar(Integer id, KamarRequest req) {
        Kamar kamar = kamarExistValidation(id);
        kamar.setJenisKamar(req.getJenisKamar());
        kamar.setHargaKamar(req.getHargaKamar());
        kamar.setDeskripsi(req.getDeskripsi());
        kamar.setIsDeleted(false);
        kamarRepository.save(kamar);

        return KamarResponse.builder()
                .id(kamar.getId())
                .jenisKamar(kamar.getJenisKamar())
                .hargaKamar(kamar.getHargaKamar())
                .deskripsi(kamar.getDeskripsi())
                .isDeleted(kamar.getIsDeleted())
                .build();
    }

    public KamarResponse deleteKamar(Integer id) {
        Kamar kamar = kamarExistValidation(id);
        kamar.setIsDeleted(true);
        kamarRepository.save(kamar);

        return KamarResponse.builder()
                .id(kamar.getId())
                .jenisKamar(kamar.getJenisKamar())
                .hargaKamar(kamar.getHargaKamar())
                .deskripsi(kamar.getDeskripsi())
                .isDeleted(kamar.getIsDeleted())
                .build();
    }
}
