package com.harapansehat.hospital.service;

import com.harapansehat.hospital.dto.spesialisasi.SpesialisRequest;
import com.harapansehat.hospital.dto.spesialisasi.SpesialisResponse;
import com.harapansehat.hospital.entity.Spesialisasi;
import com.harapansehat.hospital.exception.BadRequestException;
import com.harapansehat.hospital.exception.ResourceNotFoundException;
import com.harapansehat.hospital.repository.SpesialisasiRepository;
import com.harapansehat.hospital.util.ExceptionMessageAccessor;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class SpesialisasiService {

    private static final String SPECIALIZATION_ALREADY_EXISTS = "specialization_already_exists";
    private static final String SPECIALIZATION_DOES_NOT_EXIST = "specialization_does_not_exist";
    private final SpesialisasiRepository spesialisasiRepository;
    private final ExceptionMessageAccessor exceptionMessageAccessor;

    public void validateSpesialis(SpesialisRequest request) {

        final String namaSpesialis = request.getNamaSpesialis();

        checkNamaSpesialis(namaSpesialis);
    }

    private void checkNamaSpesialis(String namaSpesialis) {

        final boolean existsByNamaSpesialis = spesialisasiRepository.existsByNamaSpesialisIgnoreCase(namaSpesialis);

        if (existsByNamaSpesialis) {
            final String message = exceptionMessageAccessor.getMessage(null, SPECIALIZATION_ALREADY_EXISTS);
            throw new BadRequestException(message);
        }
    }

    public Spesialisasi spesialisasiExistValidation(Integer spesialisasiId) {
        Spesialisasi spesialisasiExist = spesialisasiRepository.findById(spesialisasiId)
                .orElseThrow(() -> new ResourceNotFoundException(
                        exceptionMessageAccessor.getMessage(null, SPECIALIZATION_DOES_NOT_EXIST, spesialisasiId)));

        if (Boolean.TRUE.equals(spesialisasiExist.getIsDeleted())) {
            throw new ResourceNotFoundException(
                    exceptionMessageAccessor.getMessage(null, SPECIALIZATION_DOES_NOT_EXIST, spesialisasiId));
        }

        return spesialisasiExist;
    }

    public List<SpesialisResponse> getAll() {
        List<SpesialisResponse> listSpesialis = new ArrayList<>();
        for (Spesialisasi spesialisasi : spesialisasiRepository.findAll()){
            if (Boolean.FALSE.equals(spesialisasi.getIsDeleted())) {
                listSpesialis.add(SpesialisResponse.builder()
                        .id(spesialisasi.getId())
                        .namaSpesialis(spesialisasi.getNamaSpesialis())
                        .hargaSpesialis(spesialisasi.getHarga())
                        .urlImage(spesialisasi.getUrlImage())
                        .isDeleted(spesialisasi.getIsDeleted())
                        .build());
            }
        }
        return listSpesialis;
    }

    public Spesialisasi getSpesialisasiById(Integer id) {
        return spesialisasiExistValidation(id);
    }

    public SpesialisResponse addSpesialis(SpesialisRequest req) {
        Spesialisasi spesialisasi = new Spesialisasi();
        validateSpesialis(req);
        spesialisasi.setNamaSpesialis(req.getNamaSpesialis());
        spesialisasi.setHarga(req.getHargaSpesialis());
        spesialisasi.setUrlImage(req.getUrlImage());
        spesialisasi.setIsDeleted(false);
        spesialisasiRepository.save(spesialisasi);

        return SpesialisResponse.builder()
                .id(spesialisasi.getId())
                .namaSpesialis(spesialisasi.getNamaSpesialis())
                .hargaSpesialis(spesialisasi.getHarga())
                .urlImage(spesialisasi.getUrlImage())
                .isDeleted(spesialisasi.getIsDeleted())
                .build();
    }

    public SpesialisResponse updateSpesialis(Integer id, SpesialisRequest req) {
        Spesialisasi spesialisasi = spesialisasiExistValidation(id);
        spesialisasi.setNamaSpesialis(req.getNamaSpesialis());
        spesialisasi.setHarga(req.getHargaSpesialis());
        spesialisasi.setUrlImage(req.getUrlImage());
        spesialisasi.setIsDeleted(false);
        spesialisasiRepository.save(spesialisasi);

        return SpesialisResponse.builder()
                .id(spesialisasi.getId())
                .namaSpesialis(spesialisasi.getNamaSpesialis())
                .hargaSpesialis(spesialisasi.getHarga())
                .urlImage(spesialisasi.getUrlImage())
                .isDeleted(spesialisasi.getIsDeleted())
                .build();
    }

    public SpesialisResponse deleteSpesialis(Integer id) {
        Spesialisasi spesialisasi = spesialisasiExistValidation(id);
        spesialisasi.setIsDeleted(true);
        spesialisasiRepository.save(spesialisasi);

        return SpesialisResponse.builder()
                .id(spesialisasi.getId())
                .namaSpesialis(spesialisasi.getNamaSpesialis())
                .hargaSpesialis(spesialisasi.getHarga())
                .urlImage(spesialisasi.getUrlImage())
                .isDeleted(spesialisasi.getIsDeleted())
                .build();
    }
}
