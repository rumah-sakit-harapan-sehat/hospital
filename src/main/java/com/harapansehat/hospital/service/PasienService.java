package com.harapansehat.hospital.service;

import com.harapansehat.hospital.dto.pasien.PasienRequest;
import com.harapansehat.hospital.dto.pasien.PasienResponse;
import com.harapansehat.hospital.entity.Pasien;
import com.harapansehat.hospital.entity.User;
import com.harapansehat.hospital.exception.BadRequestException;
import com.harapansehat.hospital.exception.ResourceNotFoundException;
import com.harapansehat.hospital.repository.PasienRepository;
import com.harapansehat.hospital.repository.UserRepository;
import com.harapansehat.hospital.security.mapper.UserMapper;
import com.harapansehat.hospital.util.ExceptionMessageAccessor;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class PasienService {

    private static final String PATIENT_DOES_NOT_EXIST = "patient_does_not_exist";
    private static final String NIK_CANNOT_BE_CHANGED = "nik_cannot_be_changed";
    private static final String DATEOFBIRTH_CANNOT_BE_CHANGED = "dateofbirth_cannot_be_changed";
    private final PasienRepository pasienRepository;
    private final UserRepository userRepository;
    private final ExceptionMessageAccessor exceptionMessageAccessor;
    private final UserService userService;

    public Pasien pasienExistValidation(Integer pasienId) {
        Pasien pasienExist = pasienRepository.findById(pasienId)
                .orElseThrow(() -> new ResourceNotFoundException(
                        exceptionMessageAccessor.getMessage(null, PATIENT_DOES_NOT_EXIST, pasienId)));

        if (Boolean.TRUE.equals(pasienExist.getIsDeleted())) {
            throw new ResourceNotFoundException(
                    exceptionMessageAccessor.getMessage(null, PATIENT_DOES_NOT_EXIST, pasienId));
        }

        return pasienExist;
    }

    public List<PasienResponse> getAllPasien(){
        List<PasienResponse> listPasien = new ArrayList<>();
        for (Pasien pasien : pasienRepository.findAll()){
            if (Boolean.FALSE.equals(pasien.getIsDeleted())) {
                listPasien.add(PasienResponse.builder()
                        .id(pasien.getId())
                        .nik(pasien.getNik())
                        .namaPasien(pasien.getNamaPasien())
                        .alamat(pasien.getAlamat())
                        .jenisKelamin(pasien.getJenisKelamin())
                        .tanggalLahir(pasien.getTanggalLahir())
                        .isDeleted(pasien.getIsDeleted())
                        .build());
            }

        }
        return listPasien;
    }

    public Pasien getPasienById(Integer id) {
        return pasienExistValidation(id);
    }

    public PasienResponse updatePasienById(Integer id, PasienRequest req) {
        Pasien pasienExist = pasienExistValidation(id);

        if (!pasienExist.getNik().equalsIgnoreCase(req.getNik())) {
            throw new BadRequestException(exceptionMessageAccessor.getMessage(null, NIK_CANNOT_BE_CHANGED));
        }

        if (!pasienExist.getTanggalLahir().equalsIgnoreCase(req.getTanggalLahir())) {
            throw new BadRequestException(exceptionMessageAccessor.getMessage(null, DATEOFBIRTH_CANNOT_BE_CHANGED));
        }

        final Pasien pasien = UserMapper.INSTANCE.convertToPasien(req);
        pasien.setId(pasienExist.getId());
        pasien.setIsDeleted(false);
        pasienRepository.save(pasien);

        userService.updatePassword(req.getPassword(), pasien.getNik());

        return PasienResponse.builder()
                .id(pasien.getId())
                .nik(req.getNik())
                .namaPasien(req.getNamaPasien())
                .alamat(req.getAlamat())
                .jenisKelamin(req.getJenisKelamin())
                .tanggalLahir(req.getTanggalLahir())
                .isDeleted(pasien.getIsDeleted())
                .build();
    }

    @Transactional
    public PasienResponse deletePasienById(Integer id) {
        Pasien pasien = pasienExistValidation(id);

        User userExist = userService.userExistValidation(pasien.getNik());

        pasien.setIsDeleted(true);
        pasienRepository.save(pasien);

        userExist.setIsDeleted(true);
        userRepository.save(userExist);

        return PasienResponse.builder()
                .id(pasien.getId())
                .nik(pasien.getNik())
                .namaPasien(pasien.getNamaPasien())
                .alamat(pasien.getAlamat())
                .jenisKelamin(pasien.getJenisKelamin())
                .tanggalLahir(pasien.getTanggalLahir())
                .isDeleted(pasien.getIsDeleted())
                .build();
    }
}
