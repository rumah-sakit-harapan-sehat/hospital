package com.harapansehat.hospital.service;

import com.harapansehat.hospital.dto.transaksi.RincianTransaksiObat;
import com.harapansehat.hospital.dto.treatment.DataRincianObat;
import com.harapansehat.hospital.entity.RincianObat;
import com.harapansehat.hospital.repository.RincianObatRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class RincianObatService {

    private final RincianObatRepository rincianObatRepository;

    public List<DataRincianObat> getAllRincianObatByTreatmentId(Integer treatmentId){
        List<DataRincianObat> dataRincianObat = new ArrayList<>();
        for (RincianObat rincianObat : rincianObatRepository.findAll()){
            if (rincianObat.getTreatment().getId().equals(treatmentId)) {
                dataRincianObat.add(DataRincianObat.builder()
                        .obatId(rincianObat.getObat().getId())
                        .kuantitas(rincianObat.getKuantitas())
                        .build()
                );
            }
        }
        return dataRincianObat;
    }

    public List<RincianTransaksiObat> getAllRincianTransaksiObatByTreatmentId(Integer treatmentId){
        List<RincianTransaksiObat> dataRincianObat = new ArrayList<>();
        for (RincianObat rincianObat : rincianObatRepository.findAll()){
            if (rincianObat.getTreatment().getId().equals(treatmentId)) {
                dataRincianObat.add(RincianTransaksiObat.builder()
                        .namaObat(rincianObat.getObat().getNamaObat())
                        .hargaObat(rincianObat.getObat().getHarga())
                        .kuantitas(rincianObat.getKuantitas())
                        .build()
                );
            }
        }
        return dataRincianObat;
    }
}
