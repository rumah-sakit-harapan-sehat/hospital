package com.harapansehat.hospital.service;

import com.harapansehat.hospital.dto.dokter.DokterRequest;
import com.harapansehat.hospital.dto.dokter.DokterResponse;
import com.harapansehat.hospital.entity.Dokter;
import com.harapansehat.hospital.entity.Spesialisasi;
import com.harapansehat.hospital.entity.User;
import com.harapansehat.hospital.exception.BadRequestException;
import com.harapansehat.hospital.exception.ResourceNotFoundException;
import com.harapansehat.hospital.repository.DokterRepository;
import com.harapansehat.hospital.repository.SpesialisasiRepository;
import com.harapansehat.hospital.repository.UserRepository;
import com.harapansehat.hospital.security.mapper.UserMapper;
import com.harapansehat.hospital.util.ExceptionMessageAccessor;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class DokterService {

    private static final String SPECIALIZATION_DOES_NOT_EXIST = "specialization_does_not_exist";
    private static final String DOCTOR_DOES_NOT_EXIST = "doctor_does_not_exist";
    private static final String NIP_CANNOT_BE_CHANGED = "nip_cannot_be_changed";
    private static final String DATEOFBIRTH_CANNOT_BE_CHANGED = "dateofbirth_cannot_be_changed";
    private final DokterRepository dokterRepository;
    private final UserRepository userRepository;
    private final SpesialisasiRepository spesialisasiRepository;
    private final ExceptionMessageAccessor exceptionMessageAccessor;
    private final UserService userService;

    public Dokter dokterExistValidation(Integer dokterId) {
        Dokter dokterExist = dokterRepository.findById(dokterId)
                .orElseThrow(() -> new ResourceNotFoundException(
                        exceptionMessageAccessor.getMessage(null, DOCTOR_DOES_NOT_EXIST, dokterId)));

        if (Boolean.TRUE.equals(dokterExist.getIsDeleted())) {
            throw new ResourceNotFoundException(
                    exceptionMessageAccessor.getMessage(null, DOCTOR_DOES_NOT_EXIST, dokterId));
        }

        return dokterExist;
    }

    public List<DokterResponse> getAllDokter(){
        List<DokterResponse> listDokter = new ArrayList<>();
        for (Dokter dokter : dokterRepository.findAll()){
            if (Boolean.FALSE.equals(dokter.getIsDeleted())) {
                listDokter.add(DokterResponse.builder()
                        .id(dokter.getId())
                        .nip(dokter.getNip())
                        .namaDokter(dokter.getNamaDokter())
                        .alamat(dokter.getAlamat())
                        .jenisKelamin(dokter.getJenisKelamin())
                        .tanggalLahir(dokter.getTanggalLahir())
                        .namaSpesialis(dokter.getSpesialisasi().getNamaSpesialis())
                        .isDeleted(dokter.getIsDeleted())
                        .build()
                );
            }
        }
        return listDokter;
    }

    public List<DokterResponse> getAllDokterBySpesialisasi(String spesialisasi){
        List<DokterResponse> listDokter = new ArrayList<>();
        for (Dokter dokter : dokterRepository.findAll()){
            if (Boolean.FALSE.equals(dokter.getIsDeleted()) && dokter.getSpesialisasi().getNamaSpesialis().equalsIgnoreCase(spesialisasi)) {
                listDokter.add(DokterResponse.builder()
                        .id(dokter.getId())
                        .nip(dokter.getNip())
                        .namaDokter(dokter.getNamaDokter())
                        .alamat(dokter.getAlamat())
                        .jenisKelamin(dokter.getJenisKelamin())
                        .tanggalLahir(dokter.getTanggalLahir())
                        .namaSpesialis(dokter.getSpesialisasi().getNamaSpesialis())
                        .isDeleted(dokter.getIsDeleted())
                        .build()
                );
            }
        }
        return listDokter;
    }

    public Dokter getDokterById(Integer id) {
        return dokterExistValidation(id);
    }

    public DokterResponse updateDokterById(Integer id, DokterRequest req) {

        Dokter dokterExist = dokterExistValidation(id);

        Spesialisasi spesialisasi = spesialisasiRepository.findById(req.getSpesialisId())
                .orElseThrow(() -> new ResourceNotFoundException(
                        exceptionMessageAccessor.getMessage(null, SPECIALIZATION_DOES_NOT_EXIST, id)));

        if (!dokterExist.getNip().equalsIgnoreCase(req.getNip())) {
            throw new BadRequestException(exceptionMessageAccessor.getMessage(null, NIP_CANNOT_BE_CHANGED));
        }

        if (!dokterExist.getTanggalLahir().equalsIgnoreCase(req.getTanggalLahir())) {
            throw new BadRequestException(exceptionMessageAccessor.getMessage(null, DATEOFBIRTH_CANNOT_BE_CHANGED));
        }

        final Dokter dokter = UserMapper.INSTANCE.convertToDokter(req);
        dokter.setId(dokterExist.getId());
        dokter.setSpesialisasi(spesialisasi);
        dokter.setIsDeleted(false);
        dokterRepository.save(dokter);

        userService.updatePassword(req.getPassword(), dokter.getNip());

        return DokterResponse.builder()
                .id(dokter.getId())
                .nip(dokter.getNip())
                .namaDokter(dokter.getNamaDokter())
                .alamat(dokter.getAlamat())
                .jenisKelamin(dokter.getJenisKelamin())
                .tanggalLahir(dokter.getTanggalLahir())
                .namaSpesialis(dokter.getSpesialisasi().getNamaSpesialis())
                .isDeleted(dokter.getIsDeleted())
                .build();
    }

    @Transactional
    public DokterResponse deleteDokterById(Integer id) {
        Dokter dokter = dokterExistValidation(id);

        User userExist = userService.userExistValidation(dokter.getNip());

        dokter.setIsDeleted(true);
        dokterRepository.save(dokter);

        userExist.setIsDeleted(true);
        userRepository.save(userExist);

        return DokterResponse.builder()
                .id(dokter.getId())
                .nip(dokter.getNip())
                .namaDokter(dokter.getNamaDokter())
                .alamat(dokter.getAlamat())
                .jenisKelamin(dokter.getJenisKelamin())
                .tanggalLahir(dokter.getTanggalLahir())
                .namaSpesialis(dokter.getSpesialisasi().getNamaSpesialis())
                .isDeleted(dokter.getIsDeleted())
                .build();
    }
}
