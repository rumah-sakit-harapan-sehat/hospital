package com.harapansehat.hospital.service;

import com.harapansehat.hospital.dto.rekamMedis.DetailRekamMedis;
import com.harapansehat.hospital.dto.rekamMedis.InfoRekamMedis;
import com.harapansehat.hospital.dto.rekamMedis.RekamMedisRequest;
import com.harapansehat.hospital.dto.rekamMedis.RekamMedisResponse;
import com.harapansehat.hospital.entity.Pasien;
import com.harapansehat.hospital.entity.RekamMedis;
import com.harapansehat.hospital.entity.Treatment;
import com.harapansehat.hospital.exception.ResourceNotFoundException;
import com.harapansehat.hospital.repository.RekamMedisRepository;
import com.harapansehat.hospital.util.ExceptionMessageAccessor;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class RekamMedisService {

    private static final String MEDICAL_RECORD_DOES_NOT_EXIST = "medical_record_does_not_exist";
    private final RekamMedisRepository rekamMedisRepository;
    private final PasienService pasienService;
    private final TreatmentService treatmentService;
    private final ExceptionMessageAccessor exceptionMessageAccessor;

    public RekamMedis rekamMedisExistValidation(Integer rekamMedisId) {
        RekamMedis rekamMedisExist = rekamMedisRepository.findById(rekamMedisId)
                .orElseThrow(() -> new ResourceNotFoundException(
                        exceptionMessageAccessor.getMessage(null, MEDICAL_RECORD_DOES_NOT_EXIST, rekamMedisId)));

        if (Boolean.TRUE.equals(rekamMedisExist.getIsDeleted())) {
            throw new ResourceNotFoundException(
                    exceptionMessageAccessor.getMessage(null, MEDICAL_RECORD_DOES_NOT_EXIST, rekamMedisId));
        }

        return rekamMedisExist;
    }

    public List<RekamMedisResponse> getAll(){
        List<RekamMedisResponse> listRekamMedis = new ArrayList<>();
        for (RekamMedis rekamMedis : rekamMedisRepository.findAll()) {
            if (Boolean.FALSE.equals(rekamMedis.getIsDeleted())) {
                listRekamMedis.add(RekamMedisResponse.builder()
                        .id(rekamMedis.getId())
                        .nik(rekamMedis.getPasien().getNik())
                        .namaPasien(rekamMedis.getPasien().getNamaPasien())
                        .alamat(rekamMedis.getPasien().getAlamat())
                        .jenisKelamin(rekamMedis.getPasien().getJenisKelamin())
                        .tanggalLahir(rekamMedis.getPasien().getTanggalLahir())
                        .tinggiBadan(rekamMedis.getTinggiBadan())
                        .beratBadan(rekamMedis.getBeratBadan())
                        .tensiDarah(rekamMedis.getTensiDarah())
                        .gejala(rekamMedis.getGejala())
                        .alergi(rekamMedis.getAlergi())
                        .tanggalTreatment(rekamMedis.getTreatment().getTanggalKonsul())
                        .build());
            }
        }
        return listRekamMedis;
    }

    public List<RekamMedisResponse> getAllByPasienId(Integer pasienId){
        List<RekamMedisResponse> listRekamMedis = new ArrayList<>();
        for (RekamMedis rekamMedis : rekamMedisRepository.findAll()) {
            if (Boolean.FALSE.equals(rekamMedis.getIsDeleted()) && (rekamMedis.getPasien().getId().equals(pasienId))) {
                listRekamMedis.add(RekamMedisResponse.builder()
                        .id(rekamMedis.getId())
                        .nik(rekamMedis.getPasien().getNik())
                        .namaPasien(rekamMedis.getPasien().getNamaPasien())
                        .alamat(rekamMedis.getPasien().getAlamat())
                        .jenisKelamin(rekamMedis.getPasien().getJenisKelamin())
                        .tanggalLahir(rekamMedis.getPasien().getTanggalLahir())
                        .tinggiBadan(rekamMedis.getTinggiBadan())
                        .beratBadan(rekamMedis.getBeratBadan())
                        .tensiDarah(rekamMedis.getTensiDarah())
                        .gejala(rekamMedis.getGejala())
                        .alergi(rekamMedis.getAlergi())
                        .tanggalTreatment(rekamMedis.getTreatment().getTanggalKonsul())
                        .build());
            }
        }
        return listRekamMedis;
    }

    public RekamMedisResponse addRekamMedis(RekamMedisRequest req) {
        Pasien pasien = pasienService.pasienExistValidation(req.getPasienId());
        Treatment treatment = treatmentService.treatmentExistValidation(req.getTreatmentId());

        RekamMedis rekamMedis = new RekamMedis();
        rekamMedis.setPasien(pasien);
        rekamMedis.setBeratBadan(req.getBeratBadan());
        rekamMedis.setTinggiBadan(req.getTinggiBadan());
        rekamMedis.setTensiDarah(req.getTensiDarah());
        rekamMedis.setGejala(req.getGejala());
        rekamMedis.setAlergi(req.getAlergi());
        rekamMedis.setTreatment(treatment);
        rekamMedis.setIsDeleted(false);
        rekamMedisRepository.save(rekamMedis);

        return RekamMedisResponse.builder()
                .id(rekamMedis.getId())
                .nik(rekamMedis.getPasien().getNik())
                .namaPasien(rekamMedis.getPasien().getNamaPasien())
                .alamat(rekamMedis.getPasien().getAlamat())
                .jenisKelamin(rekamMedis.getPasien().getJenisKelamin())
                .tanggalLahir(rekamMedis.getPasien().getTanggalLahir())
                .tinggiBadan(rekamMedis.getTinggiBadan())
                .beratBadan(rekamMedis.getBeratBadan())
                .tensiDarah(rekamMedis.getTensiDarah())
                .gejala(rekamMedis.getGejala())
                .alergi(rekamMedis.getAlergi())
                .tanggalTreatment(rekamMedis.getTreatment().getTanggalKonsul())
                .build();
    }

    public DetailRekamMedis getDetailRekamMedis(Integer rekamMedisId){
        RekamMedis rekamMedis = rekamMedisExistValidation(rekamMedisId);
        List<InfoRekamMedis> listRekmMedis = rekamMedisRepository.findDetailRekamMedis(rekamMedisId);

        return DetailRekamMedis.builder()
                .nik(rekamMedis.getPasien().getNik())
                .namaPasien(rekamMedis.getPasien().getNamaPasien())
                .listRekamMedis(listRekmMedis)
                .build();
    }
}
