package com.harapansehat.hospital.service;

import com.harapansehat.hospital.dto.dokter.DokterRequest;
import com.harapansehat.hospital.dto.pasien.PasienRequest;
import com.harapansehat.hospital.dto.resepsionis.ResepsionisRequest;
import com.harapansehat.hospital.entity.*;
import com.harapansehat.hospital.exception.BadRequestException;
import com.harapansehat.hospital.exception.ResourceNotFoundException;
import com.harapansehat.hospital.repository.*;
import com.harapansehat.hospital.security.dto.AuthenticatedUserDto;
import com.harapansehat.hospital.security.dto.RegistrationResponse;
import com.harapansehat.hospital.security.mapper.UserMapper;
import com.harapansehat.hospital.util.ExceptionMessageAccessor;
import com.harapansehat.hospital.util.GeneralMessageAccessor;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@Service
@RequiredArgsConstructor
public class UserService {
    private static final String REGISTRATION_SUCCESSFUL = "registration_successful";
    private static final String NAME_ONLY_ALPHABET = "name_only_alphabet";
    private static final String PASSWORD_VALIDATION = "password_validation";
    private static final String SPECIALIZATION_DOES_NOT_EXIST = "specialization_does_not_exist";
    private static final String USER_DOES_NOT_EXIST = "user_does_not_exist";
    private final UserRepository userRepository;
    private final DokterRepository dokterRepository;
    private final PasienRepository pasienRepository;
    private final ResepsionisRepository resepsionisRepository;
    private final SpesialisasiRepository spesialisasiRepository;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;
    private final UserValidationService userValidationService;
    private final GeneralMessageAccessor generalMessageAccessor;
    private final ExceptionMessageAccessor exceptionMessageAccessor;

    public User userExistValidation(String username) {
        User userExist = userRepository.findByUsername(username)
                .orElseThrow(() -> new ResourceNotFoundException(
                        exceptionMessageAccessor.getMessage(null, USER_DOES_NOT_EXIST, username)));

        if (Boolean.TRUE.equals(userExist.getIsDeleted())) {
            throw new ResourceNotFoundException(
                    exceptionMessageAccessor.getMessage(null, USER_DOES_NOT_EXIST, username));
        }

        return userExist;
    }

    public boolean isValidString(String str) {
        return str.matches("[a-zA-Z]+");
    }

    public boolean isValidPassword(String password) {
        return password.matches("^(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{8,}$");
    }

    public void isValidNameAndPassword(String name, String password) {
        if (!isValidString(name)) {
            throw new BadRequestException(NAME_ONLY_ALPHABET);
        }

        if (!isValidPassword(password)) {
            throw new BadRequestException(PASSWORD_VALIDATION);
        }
    }

    @Transactional
    public RegistrationResponse registrationDokter(DokterRequest request) {

        isValidNameAndPassword(request.getNamaDokter(), request.getPassword());

        userValidationService.validateDokter(request);

        Spesialisasi spesialisasi = spesialisasiRepository.findById(request.getSpesialisId())
                .orElseThrow(() -> new BadRequestException(SPECIALIZATION_DOES_NOT_EXIST));

        final Dokter dokter = UserMapper.INSTANCE.convertToDokter(request);
        final User user = new User();

        dokter.setSpesialisasi(spesialisasi);
        dokter.setIsDeleted(false);
        dokterRepository.save(dokter);

        user.setDokter(dokter);
        user.setPassword(bCryptPasswordEncoder.encode(request.getPassword()));
        user.setUserRole(UserRole.DOKTER);
        user.setUsername(dokter.getNip());
        user.setIsDeleted(false);
        userRepository.save(user);

        final String username = request.getNip();
        final String registrationSuccessMessage = generalMessageAccessor.getMessage(null, REGISTRATION_SUCCESSFUL, username);

        return new RegistrationResponse(registrationSuccessMessage);
    }

    @Transactional
    public RegistrationResponse registrationPasien(PasienRequest request) {

        isValidNameAndPassword(request.getNamaPasien(), request.getPassword());

        userValidationService.validatePasien(request);

        final Pasien pasien = UserMapper.INSTANCE.convertToPasien(request);
        final User user = new User();

        pasien.setIsDeleted(false);
        pasienRepository.save(pasien);

        user.setPasien(pasien);
        user.setPassword(bCryptPasswordEncoder.encode(request.getPassword()));
        user.setUserRole(UserRole.PASIEN);
        user.setUsername(pasien.getNik());
        user.setIsDeleted(false);
        userRepository.save(user);

        final String username = request.getNik();
        final String registrationSuccessMessage = generalMessageAccessor.getMessage(null, REGISTRATION_SUCCESSFUL, username);

        return new RegistrationResponse(registrationSuccessMessage);
    }

    @Transactional
    public RegistrationResponse registrationResepsionis(ResepsionisRequest request) {

        isValidNameAndPassword(request.getNamaResepsionis(), request.getPassword());

        userValidationService.validateResepsionis(request);

        final Resepsionis resepsionis = UserMapper.INSTANCE.convertToResepsionis(request);
        final User user = new User();

        resepsionis.setNamaResepsionis(request.getNamaResepsionis());
        resepsionis.setIsDeleted(false);
        resepsionisRepository.save(resepsionis);

        user.setResepsionis(resepsionis);
        user.setPassword(bCryptPasswordEncoder.encode(request.getPassword()));
        user.setUserRole(UserRole.RESEPSIONIS);
        user.setUsername(request.getUsername());
        user.setIsDeleted(false);
        userRepository.save(user);

        final String username = request.getUsername();
        final String registrationSuccessMessage = generalMessageAccessor.getMessage(null, REGISTRATION_SUCCESSFUL, username);

        return new RegistrationResponse(registrationSuccessMessage);
    }

    public AuthenticatedUserDto findAuthenticatedUserByUsername(String username) {

        final User user = userExistValidation(username);

        return UserMapper.INSTANCE.convertToAuthenticatedUserDto(user);
    }

    public void updatePassword(String password, String username) {
        final User user = UserMapper.INSTANCE.convertToUser(userExistValidation(username));
        user.setPassword(bCryptPasswordEncoder.encode(password));
        userRepository.save(user);
    }
}
