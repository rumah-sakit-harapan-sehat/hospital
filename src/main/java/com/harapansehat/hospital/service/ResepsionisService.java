package com.harapansehat.hospital.service;

import com.harapansehat.hospital.dto.resepsionis.ResepsionisRequest;
import com.harapansehat.hospital.dto.resepsionis.ResepsionisResponse;
import com.harapansehat.hospital.entity.Resepsionis;
import com.harapansehat.hospital.entity.User;
import com.harapansehat.hospital.exception.ResourceNotFoundException;
import com.harapansehat.hospital.repository.ResepsionisRepository;
import com.harapansehat.hospital.repository.UserRepository;
import com.harapansehat.hospital.security.mapper.UserMapper;
import com.harapansehat.hospital.util.ExceptionMessageAccessor;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class ResepsionisService {

    private static final String RECEPTIONIST_DOES_NOT_EXIST = "receptionist_does_not_exist";
    private static final String USER_DOES_NOT_EXIST = "user_does_not_exist";
    private final ResepsionisRepository resepsionisRepository;
    private final UserRepository userRepository;
    private final ExceptionMessageAccessor exceptionMessageAccessor;
    private final UserService userService;

    public Resepsionis resepsionisExistValidation(Integer resepsionisId) {
        Resepsionis resepsionisExist = resepsionisRepository.findById(resepsionisId)
                .orElseThrow(() -> new ResourceNotFoundException(
                        exceptionMessageAccessor.getMessage(null, RECEPTIONIST_DOES_NOT_EXIST, resepsionisId)));

        if (Boolean.TRUE.equals(resepsionisExist.getIsDeleted())) {
            throw new ResourceNotFoundException(
                    exceptionMessageAccessor.getMessage(null, RECEPTIONIST_DOES_NOT_EXIST, resepsionisId));
        }

        return resepsionisExist;
    }

    public List<ResepsionisResponse> getAll() {
        List<ResepsionisResponse> listResepsionis = new ArrayList<>();
        for (Resepsionis resepsionis : resepsionisRepository.findAll()){
            if (Boolean.FALSE.equals(resepsionis.getIsDeleted())) {
                listResepsionis.add(ResepsionisResponse.builder()
                        .id(resepsionis.getId())
                        .nama(resepsionis.getNamaResepsionis())
                        .jenisKelamin(resepsionis.getJenisKelamin())
                        .alamat(resepsionis.getAlamat())
                        .isDeleted(resepsionis.getIsDeleted())
                        .build());
            }
        }
        return listResepsionis;
    }

    public Resepsionis getResepsionisById(Integer id) {
        return resepsionisExistValidation(id);
    }

    public ResepsionisResponse updateResepsionis(Integer id, ResepsionisRequest req) {
        Resepsionis resepsionisExist = resepsionisExistValidation(id);

        final Resepsionis resepsionis = UserMapper.INSTANCE.convertToResepsionis(req);
        resepsionis.setId(resepsionisExist.getId());
        resepsionis.setIsDeleted(false);
        resepsionisRepository.save(resepsionis);

        userService.updatePassword(req.getPassword(), req.getUsername());

        return ResepsionisResponse.builder()
                .id(resepsionis.getId())
                .nama(resepsionis.getNamaResepsionis())
                .jenisKelamin(resepsionis.getJenisKelamin())
                .alamat(resepsionis.getAlamat())
                .isDeleted(resepsionis.getIsDeleted())
                .build();
    }

    @Transactional
    public ResepsionisResponse deleteResepsionis(Integer id) {
        Resepsionis resepsionis = resepsionisExistValidation(id);

        User userExist = userRepository.findByResepsionisId(resepsionis.getId())
                .orElseThrow(() -> new ResourceNotFoundException(USER_DOES_NOT_EXIST));

        resepsionis.setIsDeleted(true);
        resepsionisRepository.save(resepsionis);

        userExist.setIsDeleted(true);
        userRepository.save(userExist);

        return ResepsionisResponse.builder()
                .id(resepsionis.getId())
                .nama(resepsionis.getNamaResepsionis())
                .jenisKelamin(resepsionis.getJenisKelamin())
                .alamat(resepsionis.getAlamat())
                .isDeleted(resepsionis.getIsDeleted())
                .build();
    }
}
