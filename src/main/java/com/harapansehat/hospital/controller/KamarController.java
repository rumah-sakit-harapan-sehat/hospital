package com.harapansehat.hospital.controller;

import com.harapansehat.hospital.dto.BaseResponse;
import com.harapansehat.hospital.dto.kamar.KamarRequest;
import com.harapansehat.hospital.dto.kamar.KamarResponse;
import com.harapansehat.hospital.entity.Kamar;
import com.harapansehat.hospital.service.KamarService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/kamar")
@RequiredArgsConstructor
public class KamarController {

    private final KamarService kamarService;

    @GetMapping
    public ResponseEntity<BaseResponse<List<KamarResponse>>> getAllKamar() {
        List<KamarResponse> response = kamarService.getAllKamar();
        return ResponseEntity.status(HttpStatus.OK).body(new BaseResponse<>(response));
    }

    @GetMapping("/{id}")
    public ResponseEntity<BaseResponse<Kamar>> getKamarById(@PathVariable("id") Integer id) {
        Kamar response = kamarService.getKamarById(id);
        return ResponseEntity.status(HttpStatus.OK).body(new BaseResponse<>(response));
    }

    @PostMapping("/add")
    public ResponseEntity<BaseResponse<KamarResponse>> addKamar(@Valid @RequestBody KamarRequest req){
        KamarResponse response = kamarService.addKamar(req);
        return ResponseEntity.status(HttpStatus.CREATED).body(new BaseResponse<>(response));
    }

    @PutMapping("/{id}")
    public ResponseEntity<BaseResponse<KamarResponse>> updateKamar(@PathVariable("id") Integer id, @Valid @RequestBody KamarRequest req) {
        KamarResponse response = kamarService.updateKamar(id, req);
        return ResponseEntity.status(HttpStatus.OK).body(new BaseResponse<>(response));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<BaseResponse<KamarResponse>> deleteKamar(@PathVariable("id") Integer id) {
        KamarResponse response = kamarService.deleteKamar(id);
        return ResponseEntity.status(HttpStatus.OK).body(new BaseResponse<>(response));
    }
}
