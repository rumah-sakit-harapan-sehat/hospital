package com.harapansehat.hospital.controller;

import com.harapansehat.hospital.dto.BaseResponse;
import com.harapansehat.hospital.dto.dokter.DokterRequest;
import com.harapansehat.hospital.dto.pasien.PasienRequest;
import com.harapansehat.hospital.dto.resepsionis.ResepsionisRequest;
import com.harapansehat.hospital.security.dto.RegistrationResponse;
import com.harapansehat.hospital.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@CrossOrigin
@RestController
@RequiredArgsConstructor
@RequestMapping("/register")
public class UserController {

    private final UserService userService;

    @PostMapping("/dokter")
    public ResponseEntity<BaseResponse<RegistrationResponse>> registrationDokter(@Valid @RequestBody DokterRequest request)  {

        final RegistrationResponse registrationResponse = userService.registrationDokter(request);

        return ResponseEntity.status(HttpStatus.CREATED).body(new BaseResponse<>(registrationResponse));
    }

    @PostMapping("/pasien")
    public ResponseEntity<BaseResponse<RegistrationResponse>> registrationPasien(@Valid @RequestBody PasienRequest request)  {

        final RegistrationResponse registrationResponse = userService.registrationPasien(request);

        return ResponseEntity.status(HttpStatus.CREATED).body(new BaseResponse<>(registrationResponse));
    }

    @PostMapping("/resepsionis")
    public ResponseEntity<BaseResponse<RegistrationResponse>> registrationResepsionis(@Valid @RequestBody ResepsionisRequest request)  {

        final RegistrationResponse registrationResponse = userService.registrationResepsionis(request);

        return ResponseEntity.status(HttpStatus.CREATED).body(new BaseResponse<>(registrationResponse));
    }
}
