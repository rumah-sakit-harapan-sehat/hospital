package com.harapansehat.hospital.controller;

import com.harapansehat.hospital.dto.BaseResponse;
import com.harapansehat.hospital.dto.obat.ObatRequest;
import com.harapansehat.hospital.dto.obat.ObatResponse;
import com.harapansehat.hospital.entity.Obat;
import com.harapansehat.hospital.service.ObatService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/obat")
@RequiredArgsConstructor
public class ObatController {

    private final ObatService obatService;

    @GetMapping
    public ResponseEntity<BaseResponse<List<ObatResponse>>> getAllObat() {
        List<ObatResponse> response = obatService.getAllObat();
        return ResponseEntity.status(HttpStatus.OK).body(new BaseResponse<>(response));
    }

    @GetMapping("/{id}")
    public ResponseEntity<BaseResponse<Obat>> getObatById(@PathVariable("id") Integer id) {
        Obat response = obatService.getObatById(id);
        return ResponseEntity.status(HttpStatus.OK).body(new BaseResponse<>(response));
    }

    @PostMapping("/add")
    public ResponseEntity<BaseResponse<ObatResponse>> addObat(@Valid @RequestBody ObatRequest req) {
        ObatResponse response = obatService.addObat(req);
        return ResponseEntity.status(HttpStatus.CREATED).body(new BaseResponse<>(response));
    }

    @PutMapping("/{id}")
    public ResponseEntity<BaseResponse<ObatResponse>> updateObat(@PathVariable("id") Integer id, @Valid @RequestBody ObatRequest req) {
        ObatResponse response = obatService.updateObat(id, req);
        return ResponseEntity.status(HttpStatus.OK).body(new BaseResponse<>(response));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<BaseResponse<ObatResponse>> deleteObat(@PathVariable("id") Integer id) {
        ObatResponse response = obatService.deleteObat(id);
        return ResponseEntity.status(HttpStatus.OK).body(new BaseResponse<>(response));
    }
}
