package com.harapansehat.hospital.controller;

import com.harapansehat.hospital.dto.BaseResponse;
import com.harapansehat.hospital.dto.spesialisasi.SpesialisRequest;
import com.harapansehat.hospital.dto.spesialisasi.SpesialisResponse;
import com.harapansehat.hospital.entity.Spesialisasi;
import com.harapansehat.hospital.service.SpesialisasiService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/spesialis")
@RequiredArgsConstructor
public class SpesialisasiController {

    private final SpesialisasiService spesialisasiService;

    @GetMapping
    public ResponseEntity<BaseResponse<List<SpesialisResponse>>> getAll() {
        List<SpesialisResponse> response = spesialisasiService.getAll();
        return ResponseEntity.status(HttpStatus.OK).body(new BaseResponse<>(response));
    }

    @GetMapping("/{id}")
    public ResponseEntity<BaseResponse<Spesialisasi>> getSpesialisById(@PathVariable("id") Integer id) {
        Spesialisasi response = spesialisasiService.getSpesialisasiById(id);
        return ResponseEntity.status(HttpStatus.OK).body(new BaseResponse<>(response));
    }

    @PostMapping("/add")
    public ResponseEntity<BaseResponse<SpesialisResponse>> addSpesialis(@Valid @RequestBody SpesialisRequest req) {
        SpesialisResponse response = spesialisasiService.addSpesialis(req);
        return ResponseEntity.status(HttpStatus.CREATED).body(new BaseResponse<>(response));
    }

    @PutMapping("/{id}")
    public ResponseEntity<BaseResponse<SpesialisResponse>> updateSpesialis(@PathVariable("id") Integer id, @Valid @RequestBody SpesialisRequest req) {
        SpesialisResponse response = spesialisasiService.updateSpesialis(id, req);
        return ResponseEntity.status(HttpStatus.OK).body(new BaseResponse<>(response));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<BaseResponse<SpesialisResponse>> deleteSpesialis(@PathVariable("id") Integer id) {
        SpesialisResponse response = spesialisasiService.deleteSpesialis(id);
        return ResponseEntity.status(HttpStatus.OK).body(new BaseResponse<>(response));
    }
}
