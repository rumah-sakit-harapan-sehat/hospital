package com.harapansehat.hospital.controller;

import com.google.zxing.WriterException;
import com.harapansehat.hospital.dto.BaseResponse;
import com.harapansehat.hospital.dto.treatment.DetailTreatment;
import com.harapansehat.hospital.dto.treatment.TreatmentRequest;
import com.harapansehat.hospital.dto.treatment.TreatmentResponse;
import com.harapansehat.hospital.dto.visit.VisitRequest;
import com.harapansehat.hospital.dto.visit.VisitResponse;
import com.harapansehat.hospital.entity.Treatment;
import com.harapansehat.hospital.service.TreatmentService;
import com.harapansehat.hospital.util.PdfGenerator;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/treatment")
@RequiredArgsConstructor
public class TreatmentController {

    private final TreatmentService treatmentService;

    @GetMapping
    public ResponseEntity<BaseResponse<List<TreatmentResponse>>> getAllTreatment(){
        List<TreatmentResponse> response = treatmentService.getAllTreatment();
        return ResponseEntity.status(HttpStatus.OK).body(new BaseResponse<>(response));
    }

    @GetMapping("/{id}/detail")
    public ResponseEntity<BaseResponse<DetailTreatment>> getDetailTreatment(@PathVariable("id") Integer pasienId){
        DetailTreatment detailTreatment = treatmentService.getDetailTreatment(pasienId);
        return ResponseEntity.status(HttpStatus.OK).body(new BaseResponse<>(detailTreatment));
    }

    @PostMapping("/add")
    public ResponseEntity<BaseResponse<VisitResponse>> addTreatment(@Valid @RequestBody VisitRequest req){
        VisitResponse response = treatmentService.addTreatment(req);
        return ResponseEntity.status(HttpStatus.CREATED).body(new BaseResponse<>(response));
    }

    @PutMapping("/{id}")
    public ResponseEntity<BaseResponse<TreatmentResponse>> updateTreatment(@PathVariable("id") Integer id, @Valid @RequestBody TreatmentRequest req) {
        TreatmentResponse response = treatmentService.updateTreatment(id, req);
        return ResponseEntity.status(HttpStatus.OK).body(new BaseResponse<>(response));
    }

    @GetMapping("/{id}")
    public ResponseEntity<BaseResponse<Treatment>> getTreatmentById(@PathVariable("id") Integer id) {
        Treatment response = treatmentService.getTreatmentById(id);
        return ResponseEntity.status(HttpStatus.OK).body(new BaseResponse<>(response));
    }

    @GetMapping("/dokter/{dokterId}")
    public ResponseEntity<BaseResponse<List<TreatmentResponse>>> getAllTreatmentByDokterId(@PathVariable("dokterId") Integer dokterId){
        List<TreatmentResponse> response = treatmentService.getAllTreatmentByDokterId(dokterId);
        return ResponseEntity.status(HttpStatus.OK).body(new BaseResponse<>(response));
    }

    @GetMapping("/pasien/{pasienId}")
    public ResponseEntity<BaseResponse<List<TreatmentResponse>>> getAllTreatmentByPasienId(@PathVariable("pasienId") Integer pasienId){
        List<TreatmentResponse> response = treatmentService.getAllTreatmentByPasienId(pasienId);
        return ResponseEntity.status(HttpStatus.OK).body(new BaseResponse<>(response));
    }

    @GetMapping("/export-to-pdf/{id}")
    public void generatePdfFile(@PathVariable("id") Integer treatmendId, HttpServletResponse response) {
        response.setContentType("application/pdf");
        DateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd");
        String currentDateTime = dateFormat.format(new Date());
        String headerkey = "Content-Disposition";
        String headervalue = "attachment; filename=Reporting_Treatment " + currentDateTime + ".pdf";
        response.setHeader(headerkey, headervalue);
        DetailTreatment detailTreatment = treatmentService.getDetailTreatment(treatmendId);
        PdfGenerator generator = new PdfGenerator();
        generator.generate(detailTreatment, response);
    }

    @GetMapping(value = "/{id}/generate-qrcode", produces = MediaType.IMAGE_PNG_VALUE)
    public byte[] generateQRCode(@PathVariable Integer id) throws IOException, WriterException {
        return treatmentService.generateQRCodebyTreatmenntId(id);
    }
}
