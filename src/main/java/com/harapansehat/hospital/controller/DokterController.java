package com.harapansehat.hospital.controller;

import com.harapansehat.hospital.dto.BaseResponse;
import com.harapansehat.hospital.dto.dokter.DokterRequest;
import com.harapansehat.hospital.dto.dokter.DokterResponse;
import com.harapansehat.hospital.entity.Dokter;
import com.harapansehat.hospital.service.DokterService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/dokter")
@RequiredArgsConstructor
public class DokterController {

    private final DokterService dokterService;

    @GetMapping
    public ResponseEntity<BaseResponse<List<DokterResponse>>> getAllDokter(){
        List<DokterResponse> response = dokterService.getAllDokter();
        return ResponseEntity.status(HttpStatus.OK).body(new BaseResponse<>(response));
    }

    @PutMapping("/{id}")
    public ResponseEntity<BaseResponse<DokterResponse>> updateDokter(@PathVariable("id") Integer id, @Valid @RequestBody DokterRequest req) {
        DokterResponse response = dokterService.updateDokterById(id, req);
        return ResponseEntity.status(HttpStatus.OK).body(new BaseResponse<>(response));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<BaseResponse<DokterResponse>> deleteDokter(@PathVariable("id") Integer id) {
        DokterResponse response = dokterService.deleteDokterById(id);
        return ResponseEntity.status(HttpStatus.OK).body(new BaseResponse<>(response));
    }

    @GetMapping("/{id}")
    public ResponseEntity<BaseResponse<Dokter>> getDokterById(@PathVariable("id") Integer id) {
        Dokter response = dokterService.getDokterById(id);
        return ResponseEntity.status(HttpStatus.OK).body(new BaseResponse<>(response));
    }

    @GetMapping("/spesialisasi/{spesialisasi}")
    public ResponseEntity<BaseResponse<List<DokterResponse>>> getAllDokterBySpesialisasi(@PathVariable("spesialisasi") String spesialisasi){
        List<DokterResponse> response = dokterService.getAllDokterBySpesialisasi(spesialisasi);
        return ResponseEntity.status(HttpStatus.OK).body(new BaseResponse<>(response));
    }
}
