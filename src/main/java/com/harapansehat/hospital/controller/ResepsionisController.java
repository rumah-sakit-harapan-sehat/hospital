package com.harapansehat.hospital.controller;

import com.harapansehat.hospital.dto.BaseResponse;
import com.harapansehat.hospital.dto.resepsionis.ResepsionisRequest;
import com.harapansehat.hospital.dto.resepsionis.ResepsionisResponse;
import com.harapansehat.hospital.entity.Resepsionis;
import com.harapansehat.hospital.service.ResepsionisService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/resepsionis")
@RequiredArgsConstructor
public class ResepsionisController {

    private final ResepsionisService resepsionisService;

    @GetMapping
    public ResponseEntity<BaseResponse<List<ResepsionisResponse>>> getAll() {
        List<ResepsionisResponse> response = resepsionisService.getAll();
        return ResponseEntity.status(HttpStatus.OK).body(new BaseResponse<>(response));
    }

    @PutMapping("/{id}")
    public ResponseEntity<BaseResponse<ResepsionisResponse>> updateResepsionis(@PathVariable("id") Integer id, @Valid @RequestBody ResepsionisRequest req) {
        ResepsionisResponse response = resepsionisService.updateResepsionis(id, req);
        return ResponseEntity.status(HttpStatus.OK).body(new BaseResponse<>(response));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<BaseResponse<ResepsionisResponse>> deleteResepsionis(@PathVariable("id") Integer id) {
        ResepsionisResponse response = resepsionisService.deleteResepsionis(id);
        return ResponseEntity.status(HttpStatus.OK).body(new BaseResponse<>(response));
    }

    @GetMapping(("/{id}"))
    public ResponseEntity<BaseResponse<Resepsionis>> getResepsionisById(@PathVariable("id") Integer id) {
        Resepsionis response = resepsionisService.getResepsionisById(id);
        return ResponseEntity.status(HttpStatus.OK).body(new BaseResponse<>(response));
    }
}
