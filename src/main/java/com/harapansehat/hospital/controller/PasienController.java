package com.harapansehat.hospital.controller;

import com.harapansehat.hospital.dto.BaseResponse;
import com.harapansehat.hospital.dto.pasien.PasienRequest;
import com.harapansehat.hospital.dto.pasien.PasienResponse;
import com.harapansehat.hospital.entity.Pasien;
import com.harapansehat.hospital.service.PasienService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/pasien")
@RequiredArgsConstructor
public class PasienController {

    private final PasienService pasienService;

    @GetMapping
    public ResponseEntity<BaseResponse<List<PasienResponse>>> getAllPasien(){
        List<PasienResponse> response = pasienService.getAllPasien();
        return ResponseEntity.status(HttpStatus.OK).body(new BaseResponse<>(response));
    }

    @PutMapping("/{id}")
    public ResponseEntity<BaseResponse<PasienResponse>> updatePasien(@PathVariable("id") Integer id, @Valid @RequestBody PasienRequest req) {
        PasienResponse response = pasienService.updatePasienById(id, req);
        return ResponseEntity.status(HttpStatus.OK).body(new BaseResponse<>(response));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<BaseResponse<PasienResponse>> deletePasien(@PathVariable("id") Integer id) {
        PasienResponse response = pasienService.deletePasienById(id);
        return ResponseEntity.status(HttpStatus.OK).body(new BaseResponse<>(response));
    }

    @GetMapping(("/{id}"))
    public ResponseEntity<BaseResponse<Pasien>> getPasienById(@PathVariable("id") Integer id) {
        Pasien response = pasienService.getPasienById(id);
        return ResponseEntity.status(HttpStatus.OK).body(new BaseResponse<>(response));
    }
}
