package com.harapansehat.hospital.controller;

import com.harapansehat.hospital.dto.BaseResponse;
import com.harapansehat.hospital.dto.rekamMedis.DetailRekamMedis;
import com.harapansehat.hospital.dto.rekamMedis.RekamMedisRequest;
import com.harapansehat.hospital.dto.rekamMedis.RekamMedisResponse;
import com.harapansehat.hospital.service.RekamMedisService;
import com.harapansehat.hospital.util.PdfGenerator;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/rekamMedis")
@RequiredArgsConstructor
public class RekamMedisController {

    private final RekamMedisService rekamMedisService;

    @GetMapping
    public ResponseEntity<BaseResponse<List<RekamMedisResponse>>> getAll(){
        List<RekamMedisResponse> response = rekamMedisService.getAll();
        return ResponseEntity.status(HttpStatus.OK).body(new BaseResponse<>(response));
    }

    @GetMapping("/pasien/{pasienId}")
    public ResponseEntity<BaseResponse<List<RekamMedisResponse>>> getAllByPasienId(@PathVariable("pasienId") Integer pasienId){
        List<RekamMedisResponse> response = rekamMedisService.getAllByPasienId(pasienId);
        return ResponseEntity.status(HttpStatus.OK).body(new BaseResponse<>(response));
    }

    @PostMapping("/add")
    public ResponseEntity<BaseResponse<RekamMedisResponse>> addRekamMedis(@Valid @RequestBody RekamMedisRequest req){
        RekamMedisResponse response = rekamMedisService.addRekamMedis(req);
        return ResponseEntity.status(HttpStatus.CREATED).body(new BaseResponse<>(response));
    }

    @GetMapping("/export-to-pdf/{id}")
    public ResponseEntity<BaseResponse<DetailRekamMedis>> generatePdfFile(@PathVariable("id") Integer rekamMedisId,  HttpServletResponse response) {
        response.setContentType("application/pdf");
        DateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd");
        String currentDateTime = dateFormat.format(new Date());
        String headerkey = "Content-Disposition";
        String headervalue = "attachment; filename=Reporting_Treatment " + currentDateTime + ".pdf";
        response.setHeader(headerkey, headervalue);
        DetailRekamMedis detailRekamMedis = rekamMedisService.getDetailRekamMedis(rekamMedisId);
        PdfGenerator generator = new PdfGenerator();
        generator.generateRekamMedis(detailRekamMedis, response);
        return null;
    }
}
