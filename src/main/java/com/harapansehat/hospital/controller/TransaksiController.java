package com.harapansehat.hospital.controller;

import com.google.zxing.WriterException;
import com.harapansehat.hospital.dto.BaseResponse;
import com.harapansehat.hospital.dto.transaksi.InfoTransaksi;
import com.harapansehat.hospital.dto.transaksi.TransaksiRequest;
import com.harapansehat.hospital.dto.transaksi.TransaksiResponse;
import com.harapansehat.hospital.service.TransaksiService;
import com.harapansehat.hospital.util.PdfGenerator;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/transaksi")
@RequiredArgsConstructor
public class TransaksiController {

    private final TransaksiService transaksiService;

    @GetMapping
    public ResponseEntity<BaseResponse<List<TransaksiResponse>>> getAllTransaksi(){
        List<TransaksiResponse> response = transaksiService.getAllTransaksi();
        return ResponseEntity.status(HttpStatus.OK).body(new BaseResponse<>(response));
    }

    @GetMapping("/pasien/{pasienId}")
    public ResponseEntity<BaseResponse<List<TransaksiResponse>>> getAllTransaksiByPasienId(@PathVariable("pasienId") Integer pasienId){
        List<TransaksiResponse> response = transaksiService.getAllTransaksiByPasienId(pasienId);
        return ResponseEntity.status(HttpStatus.OK).body(new BaseResponse<>(response));
    }

    @GetMapping("/{id}/detail")
    public ResponseEntity<BaseResponse<InfoTransaksi>> getTransaksiDetail(@PathVariable("id") Integer id) throws IOException, WriterException {
        InfoTransaksi detailTransaksi = transaksiService.getTransaksiById(id);
        return ResponseEntity.status(HttpStatus.OK).body(new BaseResponse<>(detailTransaksi));
    }

    @PostMapping("/add")
    public ResponseEntity<BaseResponse<TransaksiResponse>> addTransaksi(@Valid @RequestBody TransaksiRequest req) {
        TransaksiResponse response = transaksiService.addTransaksi(req);
        return ResponseEntity.status(HttpStatus.CREATED).body(new BaseResponse<>(response));
    }

    @GetMapping("/export-to-pdf/{id}")
    public void generatePdfFile(@PathVariable("id") Integer id, HttpServletResponse response) throws IOException, WriterException {
        response.setContentType("application/pdf");
        DateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd");
        String currentDateTime = dateFormat.format(new Date());
        String headerkey = "Content-Disposition";
        String headervalue = "attachment; filename=Reporting Transaksi " + currentDateTime + ".pdf";
        response.setHeader(headerkey, headervalue);
        InfoTransaksi infoTransaksi = transaksiService.getTransaksiById(id);
        PdfGenerator generator = new PdfGenerator();
        generator.generateTransaksi(infoTransaksi, response);
    }
    
    @GetMapping(value = "/{id}/generate-qrcode", produces = MediaType.IMAGE_PNG_VALUE)
    public byte[] generateQRCode(@PathVariable Integer id) throws IOException, WriterException {
        return transaksiService.generateQRCodebyId(id);
    }
}
