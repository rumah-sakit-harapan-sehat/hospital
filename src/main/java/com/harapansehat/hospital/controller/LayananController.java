package com.harapansehat.hospital.controller;

import com.harapansehat.hospital.dto.BaseResponse;
import com.harapansehat.hospital.dto.layanan.LayananRequest;
import com.harapansehat.hospital.dto.layanan.LayananResponse;
import com.harapansehat.hospital.entity.Layanan;
import com.harapansehat.hospital.service.LayananService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/layanan")
@RequiredArgsConstructor
public class LayananController {

    private final LayananService layananService;

    @GetMapping
    public ResponseEntity<BaseResponse<List<LayananResponse>>> getAllLayanan(){
        List<LayananResponse> response = layananService.getAllLayanan();
        return ResponseEntity.status(HttpStatus.OK).body(new BaseResponse<>(response));
    }

    @GetMapping("/{id}")
    public ResponseEntity<BaseResponse<Layanan>> getLayananById(@PathVariable("id") Integer id) {
        Layanan response = layananService.getLayananById(id);
        return ResponseEntity.status(HttpStatus.OK).body(new BaseResponse<>(response));
    }

    @PostMapping("/add")
    public ResponseEntity<BaseResponse<LayananResponse>> addLayanan(@Valid @RequestBody LayananRequest req){
        LayananResponse response = layananService.addLayanan(req);
        return ResponseEntity.status(HttpStatus.CREATED).body(new BaseResponse<>(response));
    }

    @PutMapping("/{id}")
    public ResponseEntity<BaseResponse<LayananResponse>> updateLayanan(@PathVariable("id") Integer id, @Valid @RequestBody LayananRequest req) {
        LayananResponse response = layananService.updateLayanan(id, req);
        return ResponseEntity.status(HttpStatus.OK).body(new BaseResponse<>(response));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<BaseResponse<LayananResponse>> deleteLayanan(@PathVariable("id") Integer id) {
        LayananResponse response = layananService.deleteLayanan(id);
        return ResponseEntity.status(HttpStatus.OK).body(new BaseResponse<>(response));
    }
}
