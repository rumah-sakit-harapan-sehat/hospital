package com.harapansehat.hospital.repository;

import com.harapansehat.hospital.entity.Layanan;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LayananRepository extends JpaRepository<Layanan, Integer> {

    Boolean existsByNamaLayananIgnoreCase(String namaLayanan);

}
