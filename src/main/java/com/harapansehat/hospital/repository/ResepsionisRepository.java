package com.harapansehat.hospital.repository;

import com.harapansehat.hospital.entity.Resepsionis;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ResepsionisRepository extends JpaRepository<Resepsionis, Integer> {
}
