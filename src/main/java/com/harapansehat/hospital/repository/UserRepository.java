package com.harapansehat.hospital.repository;

import com.harapansehat.hospital.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Long> {
    Optional<User> findByUsername(String username);

    Optional<User> findByResepsionisId(Integer resepsionisId);

    boolean existsByUsername(String username);

}
