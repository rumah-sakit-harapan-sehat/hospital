package com.harapansehat.hospital.repository;

import com.harapansehat.hospital.entity.Spesialisasi;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SpesialisasiRepository extends JpaRepository<Spesialisasi, Integer> {

    boolean existsByNamaSpesialisIgnoreCase(String namaSpesialis);

}
