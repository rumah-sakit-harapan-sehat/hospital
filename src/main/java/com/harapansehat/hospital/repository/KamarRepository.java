package com.harapansehat.hospital.repository;

import com.harapansehat.hospital.entity.Kamar;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface KamarRepository extends JpaRepository<Kamar, Integer> {

    Boolean existsByJenisKamarIgnoreCase(String jenisKamar);
}
