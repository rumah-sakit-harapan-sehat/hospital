package com.harapansehat.hospital.repository;

import com.harapansehat.hospital.entity.Transaksi;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;


@Repository
public interface TransaksiRepository extends JpaRepository<Transaksi, Integer> {

   Boolean existsByTreatmentId(Integer treatmentId);
}
