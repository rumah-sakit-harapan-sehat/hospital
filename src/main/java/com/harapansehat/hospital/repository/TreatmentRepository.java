package com.harapansehat.hospital.repository;

import com.harapansehat.hospital.dto.treatment.InfoTreatment;
import com.harapansehat.hospital.entity.Treatment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TreatmentRepository extends JpaRepository<Treatment, Integer> {

    @Query("SELECT new com.harapansehat.hospital.dto.treatment.InfoTreatment" +
            "(t.tanggalKonsul, t.id, d.namaDokter, s.namaSpesialis, l.namaLayanan, " +
            "rm.tinggiBadan, rm.beratBadan, rm.tensiDarah, rm.alergi, rm.gejala, " +
            "t.diagnosis, t.pesan, t.status)" +
            " FROM RekamMedis rm JOIN rm.treatment t JOIN t.pasien p JOIN t.dokter d JOIN d.spesialisasi s JOIN t.layanan l WHERE p.id=:id")
    List<InfoTreatment> findDetailTreatment(@Param("id") Integer id);
}
