package com.harapansehat.hospital.repository;

import com.harapansehat.hospital.entity.Pasien;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PasienRepository extends JpaRepository<Pasien, Integer> {
    boolean existsByNik(String nik);
}
