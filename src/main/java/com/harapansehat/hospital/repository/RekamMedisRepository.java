package com.harapansehat.hospital.repository;

import com.harapansehat.hospital.dto.rekamMedis.InfoRekamMedis;
import com.harapansehat.hospital.entity.RekamMedis;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface RekamMedisRepository extends JpaRepository<RekamMedis, Integer> {

    @Query("SELECT new com.harapansehat.hospital.dto.rekamMedis.InfoRekamMedis" +
            "(p.nik, p.namaPasien, p.alamat, p.jenisKelamin, p.tanggalLahir, r.tinggiBadan, r.beratBadan, r.tensiDarah, r.gejala, r.alergi) " +
            "FROM RekamMedis r JOIN r.pasien p WHERE r.id=:id")
    List<InfoRekamMedis> findDetailRekamMedis(@Param("id") Integer id);

    Optional<RekamMedis> findByTreatmentId(Integer treatmentId);
}
