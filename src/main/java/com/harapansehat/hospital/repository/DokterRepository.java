package com.harapansehat.hospital.repository;

import com.harapansehat.hospital.dto.dokter.DokterResponse;
import com.harapansehat.hospital.entity.Dokter;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DokterRepository extends JpaRepository<Dokter, Integer> {
    @Query("SELECT new com.harapansehat.hospital.dto.dokter.DokterResponse" +
            "(d.id, d.nip, d.namaDokter, d.jenisKelamin, d.alamat, s.namaSpesialis, d.tanggalLahir, d.isDeleted) " +
            "FROM Dokter d JOIN d.spesialisasi s")
    List<DokterResponse> findAllDokter();

    boolean existsByNip(String nip);
}
