package com.harapansehat.hospital.repository;

import com.harapansehat.hospital.entity.RincianObat;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RincianObatRepository extends JpaRepository<RincianObat, Integer> {
}
