package com.harapansehat.hospital.repository;

import com.harapansehat.hospital.entity.Obat;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ObatRepository extends JpaRepository<Obat, Integer> {

    Boolean existsByNamaObatIgnoreCase(String namaObat);

}
