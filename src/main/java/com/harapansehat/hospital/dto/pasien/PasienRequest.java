package com.harapansehat.hospital.dto.pasien;

import lombok.Data;

import javax.validation.constraints.NotEmpty;

@Data
public class PasienRequest {

    @NotEmpty(message = "{registration_nik_not_empty}")
    private String nik;

    @NotEmpty(message = "{registration_name_not_empty}")
    private String namaPasien;

    @NotEmpty(message = "{registration_address_not_empty}")
    private String alamat;

    @NotEmpty(message = "{registration_gender_not_empty}")
    private String jenisKelamin;

    @NotEmpty(message = "{registration_dateofbirth_not_empty}")
    private String tanggalLahir;

    @NotEmpty(message = "{registration_password_not_empty}")
    private String password;
}
