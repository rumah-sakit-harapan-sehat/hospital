package com.harapansehat.hospital.dto.pasien;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@AllArgsConstructor
public class PasienResponse {
    private Integer id;
    private String nik;
    private String namaPasien;
    private String alamat;
    private String jenisKelamin;
    private String tanggalLahir;
    private Boolean isDeleted;
}
