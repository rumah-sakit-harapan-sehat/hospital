package com.harapansehat.hospital.dto.dokter;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@AllArgsConstructor
public class DokterResponse {
    private Integer id;
    private String nip;
    private String namaDokter;
    private String jenisKelamin;
    private String alamat;
    private String namaSpesialis;
    private String tanggalLahir;
    private Boolean isDeleted;
}
