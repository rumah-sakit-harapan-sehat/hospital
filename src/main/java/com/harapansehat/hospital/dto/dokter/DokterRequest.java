package com.harapansehat.hospital.dto.dokter;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
public class DokterRequest {

    @NotNull(message = "{registration_specialization_not_empty}")
    private Integer spesialisId;

    @NotEmpty(message = "{registration_password_not_empty}")
    private String password;

    @NotEmpty(message = "{registration_nip_not_empty}")
    private String nip;

    @NotEmpty(message = "{registration_name_not_empty}")
    private String namaDokter;

    @NotEmpty(message = "{registration_gender_not_empty}")
    private String jenisKelamin;

    @NotEmpty(message = "{registration_address_not_empty}")
    private String alamat;

    @NotEmpty(message = "{registration_dateofbirth_not_empty}")
    private String tanggalLahir;
}
