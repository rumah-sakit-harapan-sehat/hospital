package com.harapansehat.hospital.dto.layanan;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@AllArgsConstructor
public class LayananResponse {
    private Integer id;
    private String namaLayanan;
    private Boolean isDeleted;
}
