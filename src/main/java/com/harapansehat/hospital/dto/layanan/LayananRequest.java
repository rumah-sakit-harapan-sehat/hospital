package com.harapansehat.hospital.dto.layanan;

import lombok.Data;

import javax.validation.constraints.NotEmpty;

@Data
public class LayananRequest {
    @NotEmpty(message = "{service_name_not_empty}")
    private String namaLayanan;
}
