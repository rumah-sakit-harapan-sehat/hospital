package com.harapansehat.hospital.dto.kamar;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;

@Data
@Builder
@AllArgsConstructor
public class KamarResponse {
    private Integer id;
    private String jenisKamar;
    private BigDecimal hargaKamar;
    private String deskripsi;
    private Boolean isDeleted;
}
