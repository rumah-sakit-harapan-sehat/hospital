package com.harapansehat.hospital.dto.kamar;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Data
public class KamarRequest {
    @NotEmpty(message = "{room_type_not_empty}")
    private String jenisKamar;
    @NotNull(message = "{room_price_not_empty}")
    private BigDecimal hargaKamar;
    @NotEmpty(message = "{room_description_not_empty}")
    private String deskripsi;
}
