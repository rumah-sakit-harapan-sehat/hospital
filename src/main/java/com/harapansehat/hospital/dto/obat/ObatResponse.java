package com.harapansehat.hospital.dto.obat;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;

@Data
@Builder
@AllArgsConstructor
public class ObatResponse {
    private Integer id;
    private String namaObat;
    private String jenisObat;
    private BigDecimal harga;
    private String keterangan;
    private Boolean isDeleted;
}
