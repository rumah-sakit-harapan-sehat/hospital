package com.harapansehat.hospital.dto.obat;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Data
public class ObatRequest {
    @NotEmpty(message = "{medicine_name_not_empty}")
    private String namaObat;
    @NotEmpty(message = "{medicine_type_not_empty}")
    private String jenisObat;
    @NotNull(message = "{medicine_price_not_empty}")
    private BigDecimal harga;
    @NotEmpty(message = "{medicine_information_not_empty}")
    private String keterangan;
}
