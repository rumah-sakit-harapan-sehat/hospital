package com.harapansehat.hospital.dto.spesialisasi;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Data
public class SpesialisRequest {
    @NotEmpty(message = "{specialization_name_not_empty}")
    private String namaSpesialis;

    @NotNull(message = "{specialization_price_not_empty}")
    private BigDecimal hargaSpesialis;

    @NotEmpty(message = "{specialization_urlimage_not_empty}")
    private String urlImage;
}
