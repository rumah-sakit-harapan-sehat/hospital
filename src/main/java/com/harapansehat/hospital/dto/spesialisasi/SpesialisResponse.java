package com.harapansehat.hospital.dto.spesialisasi;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;

@Data
@Builder
@AllArgsConstructor
public class SpesialisResponse {
    private Integer id;
    private String namaSpesialis;
    private BigDecimal hargaSpesialis;
    private String urlImage;
    private Boolean isDeleted;
}
