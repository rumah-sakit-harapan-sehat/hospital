package com.harapansehat.hospital.dto.visit;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Data
public class VisitRequest {
    @NotNull(message = "{patient_id_not_empty}")
    private Integer pasienId;
    @NotNull(message = "{doctor_id_not_empty}")
    private Integer dokterId;
    @NotNull(message = "{consultation_date_not_empty}")
    private Date tanggalKonsul;
}
