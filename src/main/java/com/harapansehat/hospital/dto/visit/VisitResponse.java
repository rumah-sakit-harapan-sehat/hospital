package com.harapansehat.hospital.dto.visit;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.util.Date;

@Data
@Builder
@AllArgsConstructor
public class VisitResponse {
    private Integer visitId;
    private Integer pasienId;
    private Integer dokterId;
    private Date tanggalKonsul;
}
