package com.harapansehat.hospital.dto.transaksi;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.harapansehat.hospital.entity.Kamar;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
public class InfoTransaksi {
    private String nik;
    private String namaPasien;
    private String alamat;

    private String namaDokter;
    private String namaSpesialis;

    private String namaResepsionis;

    @JsonFormat(pattern = "dd/MM/yyyy")
    private Date tanggalTransaksi;

    private BigDecimal biayaAdmin;
    private BigDecimal biayaDokter;
    private List<RincianTransaksiObat> obat;
    private Kamar kamar;
    private Integer totalHariRawatInap;
    private BigDecimal biayaOperasi;
    private byte[] qrCode;
}
