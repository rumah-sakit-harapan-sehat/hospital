package com.harapansehat.hospital.dto.transaksi;

import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;

@Data
@Builder
public class RincianTransaksiObat {
    private String namaObat;
    private BigDecimal hargaObat;
    private Integer kuantitas;
}
