package com.harapansehat.hospital.dto.transaksi;

import lombok.AllArgsConstructor;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Data
@AllArgsConstructor
public class TransaksiRequest {
    @NotNull(message = "{treatment_id_not_empty}")
    private Integer treatmentId;
    @NotNull(message = "{receptionist_id_not_empty}")
    private Integer resepsionisId;
    @NotNull(message = "{admin_fee_not_empty}")
    private BigDecimal biayaAdmin;
    private BigDecimal biayaOperasi;
    private Integer totalHariRawatInap;
}
