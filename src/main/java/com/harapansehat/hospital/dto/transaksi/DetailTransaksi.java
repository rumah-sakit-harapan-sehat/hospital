package com.harapansehat.hospital.dto.transaksi;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
public class DetailTransaksi {
    private String nik;
    private String namaPasien;
    private String alamat;
    private List<InfoTransaksi> listTransaksi;
}
