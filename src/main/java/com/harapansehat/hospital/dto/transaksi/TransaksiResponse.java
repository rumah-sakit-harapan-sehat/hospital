package com.harapansehat.hospital.dto.transaksi;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

@Data
@Builder
@AllArgsConstructor
public class TransaksiResponse {
    private Integer id;
    private Integer treatmentId;
    private Integer pasienId;
    private Integer dokterId;
    private Integer resepsionisId;
    private String namaResepsionis;
    private BigDecimal biayaObat;
    private BigDecimal biayaDokter;
    private BigDecimal biayaLayanan;
    private BigDecimal biayaAdmin;
    @JsonFormat(pattern = "dd/MM/yyyy")
    private Date tanggalTransaksi;
}
