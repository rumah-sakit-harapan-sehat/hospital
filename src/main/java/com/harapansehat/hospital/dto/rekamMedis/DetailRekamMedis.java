package com.harapansehat.hospital.dto.rekamMedis;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
public class DetailRekamMedis {
    private String nik;
    private String namaPasien;
    private List<InfoRekamMedis> listRekamMedis;
}
