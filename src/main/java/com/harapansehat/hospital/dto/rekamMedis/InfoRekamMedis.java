package com.harapansehat.hospital.dto.rekamMedis;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@AllArgsConstructor
public class InfoRekamMedis {
    private String nik;
    private String namaPasien;
    private String alamat;
    private String jenisKelamin;
    private String tanggalLahir;
    private Double tinggiBadan;
    private Double beratBadan;
    private String tensiDarah;
    private String gejala;
    private String alergi;
}
