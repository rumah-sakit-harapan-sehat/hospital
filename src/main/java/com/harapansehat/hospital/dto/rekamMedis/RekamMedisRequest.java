package com.harapansehat.hospital.dto.rekamMedis;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
public class RekamMedisRequest {
    @NotNull(message = "{patient_id_not_empty}")
    private Integer pasienId;
    @NotNull(message = "{treatment_id_not_empty}")
    private Integer treatmentId;
    @NotNull(message = "{height_not_empty}")
    private Double tinggiBadan;
    @NotNull(message = "{weight_not_empty}")
    private Double beratBadan;
    @NotEmpty(message = "{blood_pressure_not_empty}")
    private String tensiDarah;
    @NotEmpty(message = "{symptom_not_empty}")
    private String gejala;
    @NotEmpty(message = "{allergy_not_empty}")
    private String alergi;
}
