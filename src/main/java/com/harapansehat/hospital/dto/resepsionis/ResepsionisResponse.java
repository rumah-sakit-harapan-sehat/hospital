package com.harapansehat.hospital.dto.resepsionis;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@AllArgsConstructor
public class ResepsionisResponse {
    private Integer id;
    private String nama;
    private String alamat;
    private String jenisKelamin;
    private Boolean isDeleted;
}
