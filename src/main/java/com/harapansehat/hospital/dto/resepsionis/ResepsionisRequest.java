package com.harapansehat.hospital.dto.resepsionis;

import lombok.Data;

import javax.validation.constraints.NotEmpty;

@Data
public class ResepsionisRequest {
    @NotEmpty(message = "{registration_name_not_empty}")
    private String namaResepsionis;
    @NotEmpty(message = "{registration_address_not_empty}")
    private String alamat;
    @NotEmpty(message = "{registration_gender_not_empty}")
    private String jenisKelamin;
    @NotEmpty(message = "{registration_username_not_empty}")
    private String username;
    @NotEmpty(message = "{registration_password_not_empty}")
    private String password;
}
