package com.harapansehat.hospital.dto.login;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Date;

@Data
@AllArgsConstructor
public class LoginResponse {
    private Integer userId;
    private String token;
    private Date tanggalLogin;
    private String role;
}