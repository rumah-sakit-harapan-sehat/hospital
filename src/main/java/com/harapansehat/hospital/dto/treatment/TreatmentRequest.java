package com.harapansehat.hospital.dto.treatment;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;

@Data
public class TreatmentRequest {
    @NotNull(message = "{patient_id_not_empty}")
    private Integer pasienId;
    @NotNull(message = "{doctor_id_not_empty}")
    private Integer dokterId;
    @NotNull(message = "{medicine_not_empty}")
    private List<DataRincianObat> obat;
    @NotNull(message = "{service_id_not_empty}")
    private Integer layananId;
    @NotNull(message = "{consultation_date_not_empty}")
    private Date tanggalKonsul;
    @NotEmpty(message = "{diagnosis_not_empty}")
    private String diagnosis;
    @NotEmpty(message = "{message_not_empty}")
    private String pesan;
    private Integer kamarId;
}
