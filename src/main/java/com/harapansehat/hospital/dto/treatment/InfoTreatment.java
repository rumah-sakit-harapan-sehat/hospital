package com.harapansehat.hospital.dto.treatment;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;

@Data
@NoArgsConstructor
public class InfoTreatment {
    private Date tanggalKonsul;
    private Integer treatmentId;
    private String namaDokter;
    private String spesialisasi;
    private String namaLayanan;
    private Double tinggiBadan;
    private Double beratBadan;
    private String tensiDarah;
    private String alergi;
    private String gejala;
    private String diagnosis;
    private List<DataRincianObat> obat;
    private String pesan;
    private String status;

    public InfoTreatment(Date tanggalKonsul, Integer treatmentId, String namaDokter, String spesialisasi, String namaLayanan,
                         Double tinggiBadan, Double beratBadan, String tensiDarah, String alergi,
                         String gejala, String diagnosis, String pesan, String status) {
        this.tanggalKonsul = tanggalKonsul;
        this.treatmentId = treatmentId;
        this.namaDokter = namaDokter;
        this.spesialisasi = spesialisasi;
        this.namaLayanan = namaLayanan;
        this.tinggiBadan = tinggiBadan;
        this.beratBadan = beratBadan;
        this.tensiDarah = tensiDarah;
        this.alergi = alergi;
        this.gejala = gejala;
        this.diagnosis = diagnosis;
        this.pesan = pesan;
        this.status = status;
    }
}
