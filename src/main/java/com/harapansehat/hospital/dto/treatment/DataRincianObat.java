package com.harapansehat.hospital.dto.treatment;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class DataRincianObat {
    private Integer obatId;
    private Integer kuantitas;
}
