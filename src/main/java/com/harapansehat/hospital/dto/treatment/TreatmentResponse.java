package com.harapansehat.hospital.dto.treatment;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.harapansehat.hospital.entity.Obat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
public class TreatmentResponse {
    private Integer id;
    private Integer pasienId;
    private String namaPasien;
    private Integer dokterId;
    private String namaDokter;
    private String spesialisasi;
    private List<DataRincianObat> obat;
    private Integer layananId;
    private String diagnosis;
    private String pesan;
    private String status;
    @JsonFormat(pattern = "YYYY-MM-DD")
    private Date tanggalKonsul;
}
