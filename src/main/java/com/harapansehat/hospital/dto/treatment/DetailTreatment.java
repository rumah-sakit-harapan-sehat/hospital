package com.harapansehat.hospital.dto.treatment;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
public class DetailTreatment {
    private String namaPasien;
    private String tanggalLahir;
    private String jenisKelamin;
    private String alamat;
    private List<InfoTreatment> listTreatment;
}
