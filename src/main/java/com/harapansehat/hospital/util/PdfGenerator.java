package com.harapansehat.hospital.util;

import com.harapansehat.hospital.dto.rekamMedis.DetailRekamMedis;
import com.harapansehat.hospital.dto.rekamMedis.InfoRekamMedis;
import com.harapansehat.hospital.dto.transaksi.InfoTransaksi;
import com.harapansehat.hospital.dto.transaksi.RincianTransaksiObat;
import com.harapansehat.hospital.dto.treatment.DetailTreatment;
import com.harapansehat.hospital.dto.treatment.InfoTreatment;
import com.lowagie.text.*;
import com.lowagie.text.Font;
import com.lowagie.text.Image;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;
import com.lowagie.text.pdf.draw.LineSeparator;

import javax.servlet.http.HttpServletResponse;
import java.awt.*;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Locale;

public class PdfGenerator {

    private static PdfPCell createCell(String content, Font font) {
        PdfPCell cell = new PdfPCell();
        cell.setPhrase(new Phrase(content, font));
        return cell;
    }
    public void generate(DetailTreatment detailTreatment, HttpServletResponse servletResponse) {
        try (Document document = new Document(PageSize.A4)) {
            DateFormat dateFormat = new SimpleDateFormat("dd MMMM yyyy", new Locale("id", "ID"));
            PdfWriter.getInstance(document, servletResponse.getOutputStream());
            document.open();

            Font fontTitle = FontFactory.getFont(FontFactory.TIMES_ROMAN);
            fontTitle.setSize(20);
            fontTitle.setColor(Color.black);

            Paragraph paragraph = new Paragraph("REPORTING TREATMENT", fontTitle);
            paragraph.setAlignment(Element.ALIGN_CENTER);
            document.add(paragraph);

            document.add(new Paragraph("\n"));
            document.add(new Chunk(new LineSeparator()));

            Font fontSubTitle = FontFactory.getFont(FontFactory.TIMES_ROMAN);
            fontSubTitle.setSize(14);
            fontSubTitle.setColor(Color.black);

            Font fontText = FontFactory.getFont(FontFactory.TIMES_ROMAN);
            fontText.setSize(12);
            fontText.setColor(Color.black);

            document.add(new Paragraph("DATA PASIEN", fontSubTitle));
            document.add(new Paragraph("Nama Pasien   : " + detailTreatment.getNamaPasien(), fontText));
            document.add(new Paragraph("Tanggal Lahir : " + detailTreatment.getTanggalLahir(), fontText));
            document.add(new Paragraph("Jenis Kelamin : " + detailTreatment.getJenisKelamin(), fontText));
            document.add(new Paragraph("Alamat            : " + detailTreatment.getAlamat(), fontText));
            document.add(new Paragraph("\n"));

            document.add(new Paragraph("DATA TREATMENT", fontSubTitle));

            PdfPTable table = new PdfPTable(2);
            table.setWidthPercentage(100 );
            table.setWidths(new int[] {7,7});
            table.setSpacingBefore(5);

            PdfPCell cell = new PdfPCell();
            cell.setBackgroundColor(Color.BLUE);
            cell.setPadding(5);

            Font font = FontFactory.getFont(FontFactory.TIMES_ROMAN);
            font.setColor(Color.WHITE);

            cell.setPhrase(new Phrase("Data Dokter", font));
            table.addCell(cell);
            cell.setPhrase(new Phrase("Rekam Medis Pasien", font));
            table.addCell(cell);

            for (InfoTreatment response : detailTreatment.getListTreatment()) {
                table.addCell(createCell(
                        "Nama Dokter  : " + response.getNamaDokter() + "\n" +
                                "Spesialisasi     : " + response.getSpesialisasi(), fontText));
                table.addCell(createCell(
                        "Tanggal Treatment - " + dateFormat.format(response.getTanggalKonsul()) + "\n" +
                                "Tinggi Badan : " + response.getTinggiBadan() + "\n" +
                                "Berat Badan   : " + response.getBeratBadan() + "\n" +
                                "Tensi Darah   : " + response.getTensiDarah() + "\n" +
                                "Alergi             : " + response.getAlergi() + "\n" +
                                "Gejala            : " + response.getGejala() + "\n" +
                                "Diagnosis      : " + response.getDiagnosis(), fontText));
            }
            document.add(table);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void generateTransaksi(InfoTransaksi infoTransaksi, HttpServletResponse servletResponse) {
        try (Document document = new Document(PageSize.A4)) {
            PdfWriter.getInstance(document, servletResponse.getOutputStream());
            document.open();

            Font fontTitle = FontFactory.getFont(FontFactory.TIMES_ROMAN);
            fontTitle.setSize(20);
            fontTitle.setColor(Color.black);

            Paragraph paragraph = new Paragraph("RUMAH SAKIT HARAPAN SEHAT", fontTitle);
            paragraph.setAlignment(Element.ALIGN_CENTER);
            document.add(paragraph);

            document.add(new Paragraph("\n"));
            document.add(new Chunk(new LineSeparator()));

            Font fontSubTitle = FontFactory.getFont(FontFactory.TIMES_ROMAN);
            fontSubTitle.setSize(14);
            fontSubTitle.setColor(Color.black);

            Font fontText = FontFactory.getFont(FontFactory.TIMES_ROMAN);
            fontText.setSize(12);
            fontText.setColor(Color.black);

            document.add(new Paragraph("DATA PASIEN", fontSubTitle));
            document.add(new Paragraph("NIK                  : " + infoTransaksi.getNik(), fontText));
            document.add(new Paragraph("Nama Pasien     : " + infoTransaksi.getNamaPasien(), fontText));
            document.add(new Paragraph("Alamat              : " + infoTransaksi.getAlamat(), fontText));
            document.add(new Chunk(new LineSeparator()));

            document.add(new Paragraph("DATA DOKTER", fontSubTitle));
            document.add(new Paragraph("Nama Dokter     : " + infoTransaksi.getNamaDokter(), fontText));
            document.add(new Paragraph("Spesialisasi    : " + infoTransaksi.getNamaSpesialis(), fontText));
            document.add(new Chunk(new LineSeparator()));

            document.add(new Paragraph("DATA TRANSAKSI", fontSubTitle));
            document.add(new Paragraph("Nama Resepsionis    : " + infoTransaksi.getNamaResepsionis(), fontText));
            document.add(new Paragraph("Tanggal Transaksi   : " + infoTransaksi.getTanggalTransaksi(), fontText));
            document.add(new Chunk(new LineSeparator()));

            PdfPTable table = new PdfPTable(4);
            table.setWidthPercentage(100);
            table.setWidths(new int[] {6,3,3,3});
            table.setSpacingBefore(5);

            PdfPCell cell = new PdfPCell();
            cell.setBackgroundColor(Color.WHITE);
            cell.setPadding(5);

            Font font = FontFactory.getFont(FontFactory.TIMES_ROMAN);
            font.setColor(Color.BLACK);

            document.add(new Paragraph("RINCIAN TRANSAKSI", fontSubTitle));
            cell.setPhrase(new Phrase("Nama", font));
            table.addCell(cell);
            cell.setPhrase(new Phrase("Kuantitas", font));
            table.addCell(cell);
            cell.setPhrase(new Phrase("Harga", font));
            table.addCell(cell);
            cell.setPhrase(new Phrase("Total", font));
            table.addCell(cell);

            BigDecimal totalBiayaTransaksi = new BigDecimal(0);
            Image qrCode = Image.getInstance(infoTransaksi.getQrCode());
            qrCode.scaleAbsolute(200,200);

            table.addCell(createCell("Biaya Admin", fontText));
            table.addCell(createCell("1", fontText));
            table.addCell(createCell(infoTransaksi.getBiayaAdmin().toString(), fontText));
            table.addCell(createCell(infoTransaksi.getBiayaAdmin().toString(), fontText));
            totalBiayaTransaksi = totalBiayaTransaksi.add(infoTransaksi.getBiayaAdmin());
            table.addCell(createCell("Biaya Dokter", fontText));
            table.addCell(createCell("1", fontText));
            table.addCell(createCell(infoTransaksi.getBiayaDokter().toString(), fontText));
            table.addCell(createCell(infoTransaksi.getBiayaDokter().toString(), fontText));
            totalBiayaTransaksi = totalBiayaTransaksi.add(infoTransaksi.getBiayaDokter());

            if (infoTransaksi.getBiayaOperasi() != null) {
                table.addCell(createCell("Biaya Operasi", fontText));
                table.addCell(createCell("1", fontText));
                table.addCell(createCell(infoTransaksi.getBiayaOperasi().toString(), fontText));
                table.addCell(createCell(infoTransaksi.getBiayaOperasi().toString(), fontText));
                totalBiayaTransaksi = totalBiayaTransaksi.add(infoTransaksi.getBiayaOperasi());
            }

            if (infoTransaksi.getTotalHariRawatInap() != null) {
                table.addCell(createCell("Biaya Rawat Inap", fontText));
                table.addCell(createCell(infoTransaksi.getTotalHariRawatInap().toString(), fontText));
                table.addCell(createCell(infoTransaksi.getKamar().getHargaKamar().toString(), fontText));
                table.addCell(createCell(infoTransaksi.getKamar().getHargaKamar().multiply(new BigDecimal(infoTransaksi.getTotalHariRawatInap())).toString(), fontText));
                totalBiayaTransaksi = totalBiayaTransaksi.add(infoTransaksi.getKamar().getHargaKamar().multiply(new BigDecimal(infoTransaksi.getTotalHariRawatInap())));
            }

            for (RincianTransaksiObat rincianObat : infoTransaksi.getObat()) {
                table.addCell(createCell(rincianObat.getNamaObat(), fontText));
                table.addCell(createCell(rincianObat.getKuantitas().toString(), fontText));
                table.addCell(createCell(rincianObat.getHargaObat().toString(), fontText));
                table.addCell(createCell(rincianObat.getHargaObat().multiply(new BigDecimal(rincianObat.getKuantitas())).toString(), fontText));
                totalBiayaTransaksi = totalBiayaTransaksi.add(rincianObat.getHargaObat().multiply(new BigDecimal(rincianObat.getKuantitas())));
            }

            document.add(table);
            document.add(new Paragraph("Total Biaya Transaksi   : " + totalBiayaTransaksi, fontText));
            document.add(qrCode);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void generateRekamMedis(DetailRekamMedis detailRekamMedis, HttpServletResponse servletResponse) {
        try (Document document = new Document(PageSize.A4)) {
            PdfWriter.getInstance(document, servletResponse.getOutputStream());
            document.open();

            Font fontTitle = FontFactory.getFont(FontFactory.TIMES_ROMAN);
            fontTitle.setSize(20);

            Paragraph paragraph = new Paragraph("Reporting Rekam Medis", fontTitle);
            paragraph.setAlignment(Element.ALIGN_CENTER);
            document.add(paragraph);

            PdfPTable table = new PdfPTable(7);
            table.setWidthPercentage(100 );
            table.setWidths(new int[] {3,3,3,3,3,3,3});
            table.setSpacingBefore(5);

            PdfPCell cell = new PdfPCell();
            cell.setBackgroundColor(Color.BLUE);
            cell.setPadding(5);

            Font font = FontFactory.getFont(FontFactory.TIMES_ROMAN);
            font.setColor(Color.WHITE);

            cell.setPhrase(new Phrase("Nama Pasien", font));
            table.addCell(cell);
            cell.setPhrase(new Phrase("Alamat", font));
            table.addCell(cell);
            cell.setPhrase(new Phrase("Jenis Kelamin", font));
            table.addCell(cell);
            cell.setPhrase(new Phrase("Tanggal Lahir", font));
            table.addCell(cell);
            cell.setPhrase(new Phrase("Tinggi Badan", font));
            table.addCell(cell);
            cell.setPhrase(new Phrase("Berat Badan", font));
            table.addCell(cell);
            cell.setPhrase(new Phrase("Alergi", font));
            table.addCell(cell);

            for (InfoRekamMedis response : detailRekamMedis.getListRekamMedis()) {
                table.addCell(response.getNamaPasien());
                table.addCell(response.getAlamat());
                table.addCell(response.getJenisKelamin());
                table.addCell(response.getTanggalLahir());
                table.addCell(String.valueOf(response.getTinggiBadan()));
                table.addCell(String.valueOf(response.getBeratBadan()));
                table.addCell(response.getAlergi());
            }
            document.add(table);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
