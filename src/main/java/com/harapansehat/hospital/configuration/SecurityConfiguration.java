package com.harapansehat.hospital.configuration;

import com.harapansehat.hospital.entity.UserRole;
import com.harapansehat.hospital.security.jwt.JwtAuthenticationEntryPoint;
import com.harapansehat.hospital.security.jwt.JwtAuthenticationFilter;
import com.harapansehat.hospital.security.service.UserDetailsServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.servlet.http.HttpServletResponse;

@Configuration
@EnableWebSecurity
@RequiredArgsConstructor
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {
    private final UserDetailsServiceImpl userDetailsServiceImpl;
    private final JwtAuthenticationFilter jwtAuthenticationFilter;
    private final JwtAuthenticationEntryPoint unauthorizedHandler;

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.cors().and().csrf().disable()
                .addFilterBefore(jwtAuthenticationFilter, UsernamePasswordAuthenticationFilter.class)
                .authorizeRequests()
                .antMatchers("/treatment/add").hasAuthority(UserRole.PASIEN.toString())
                .antMatchers("/rekamMedis/add").hasAuthority(UserRole.DOKTER.toString())
                .antMatchers("/transaksi/add").hasAuthority(UserRole.RESEPSIONIS.toString())
                .antMatchers(HttpMethod.PUT, "/pasien/{id}").hasAuthority(UserRole.PASIEN.toString())
                .antMatchers(HttpMethod.DELETE, "/pasien/{id}").hasAuthority(UserRole.PASIEN.toString())
                .antMatchers(HttpMethod.PUT, "/dokter/{id}", "/treatment/{id}").hasAuthority(UserRole.DOKTER.toString())
                .antMatchers(HttpMethod.DELETE, "/dokter/{id}").hasAuthority(UserRole.DOKTER.toString())
                .antMatchers(HttpMethod.PUT, "/resepsionis/{id}").hasAuthority(UserRole.RESEPSIONIS.toString())
                .antMatchers(HttpMethod.DELETE, "/resepsionis/{id}").hasAuthority(UserRole.RESEPSIONIS.toString())
                .antMatchers("/spesialis/**", "/obat/**", "/register/**", "/login", "/resepsionis/**", "/kamar/**", "/layanan/**").permitAll()
                .anyRequest().authenticated().and()
                .exceptionHandling()
                .accessDeniedHandler((request, response, accessDeniedException) -> {
                    response.setContentType("application/json");
                    response.setStatus(HttpServletResponse.SC_FORBIDDEN);
                    response.getWriter().write("{\"error\": \"Access denied: " + accessDeniedException.getMessage() + "\"}");
                })
                .authenticationEntryPoint((request, response, authException) -> {
                    response.setContentType("application/json");
                    response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
                    response.getWriter().write("{\"error\": \"Unauthorized: " + authException.getMessage() + "\"}");
                })
                .and()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
    }

    @Autowired
    public void globalUserDetails(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsServiceImpl).passwordEncoder(new BCryptPasswordEncoder());
    }
}
