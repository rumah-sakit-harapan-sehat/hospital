package com.harapansehat.hospital.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.time.LocalDateTime;

@RestControllerAdvice
public class ResourceNotFoundControllerAdvice {

    @ExceptionHandler(ResourceNotFoundException.class)
    ResponseEntity<ApiExceptionResponse> handleResourceNotFoundException(ResourceNotFoundException exception) {

        final ApiExceptionResponse response = new ApiExceptionResponse(Boolean.FALSE, exception.getErrorMessage(), HttpStatus.NOT_FOUND, LocalDateTime.now());

        return ResponseEntity.status(response.getStatus()).body(response);
    }
}
