package com.harapansehat.hospital.exception;

import com.harapansehat.hospital.controller.UserController;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.time.LocalDateTime;

@RestControllerAdvice(basePackageClasses = UserController.class)
public class BadRequestControllerAdvice {
    @ExceptionHandler(BadRequestException.class)
    ResponseEntity<ApiExceptionResponse> handleBadRequestException(BadRequestException exception) {

        final ApiExceptionResponse response = new ApiExceptionResponse(Boolean.FALSE, exception.getErrorMessage(), HttpStatus.BAD_REQUEST, LocalDateTime.now());

        return ResponseEntity.status(response.getStatus()).body(response);
    }
}
