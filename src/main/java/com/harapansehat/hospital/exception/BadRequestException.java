package com.harapansehat.hospital.exception;

import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
public class BadRequestException extends RuntimeException {
    private final String errorMessage;
}
