package com.harapansehat.hospital.exception;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;

import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ApiExceptionResponse {
    private Boolean success;
    private String message;
    private HttpStatus status;
    private LocalDateTime time;
}
