package com.harapansehat.hospital.entity;

import javax.persistence.*;

import lombok.Data;

@Data
@Entity
public class Pasien {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String nik;
    private String namaPasien;
    private String alamat;
    private String jenisKelamin;
    private String tanggalLahir;

    private Boolean isDeleted;
}
