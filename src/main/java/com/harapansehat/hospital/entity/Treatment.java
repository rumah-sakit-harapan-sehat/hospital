package com.harapansehat.hospital.entity;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;

@Data
@Entity
public class Treatment {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String pesan;
    private String status;
    @JsonFormat(pattern = "YYYY-MM-DD")
    private Date tanggalKonsul;
    private String diagnosis;

    @OneToOne
    private Dokter dokter;

    @OneToOne
    private Pasien pasien;

    @OneToOne
    private Layanan layanan;

    @OneToOne
    private Spesialisasi spesialisasi;

    @OneToOne
    private Kamar kamar;

    private Boolean isDeleted;
}
