package com.harapansehat.hospital.entity;

public enum UserRole {
    DOKTER, PASIEN, RESEPSIONIS
}
