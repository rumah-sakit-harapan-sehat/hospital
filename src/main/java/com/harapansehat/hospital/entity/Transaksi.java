package com.harapansehat.hospital.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import javax.persistence.*;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

@Data
@Entity
public class Transaksi {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private BigDecimal biayaAdmin;
    private BigDecimal biayaOperasi;
    private Integer totalHariRawatInap;
    @JsonFormat(pattern = "dd/MM/yyyy")
    private Date tanggalTransaksi;

    @OneToOne
    private Treatment treatment;

    @ManyToOne
    private Resepsionis resepsionis;

    private Boolean isDeleted;
}
