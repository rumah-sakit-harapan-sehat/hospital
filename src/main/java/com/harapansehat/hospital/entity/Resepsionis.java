package com.harapansehat.hospital.entity;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
public class Resepsionis {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String namaResepsionis;
    private String alamat;
    private String jenisKelamin;

    private Boolean isDeleted;
}
