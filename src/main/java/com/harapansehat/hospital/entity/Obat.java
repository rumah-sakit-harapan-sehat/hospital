package com.harapansehat.hospital.entity;

import javax.persistence.*;
import lombok.Data;

import java.math.BigDecimal;

@Data
@Entity
public class Obat {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String namaObat;
    private String jenisObat;
    private BigDecimal harga;
    private String keterangan;

    private Boolean isDeleted;
}
