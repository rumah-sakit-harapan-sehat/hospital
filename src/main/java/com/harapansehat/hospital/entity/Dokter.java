package com.harapansehat.hospital.entity;

import javax.persistence.*;
import lombok.Data;

@Data
@Entity
public class Dokter {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String nip;
    private String namaDokter;
    private String jenisKelamin;
    private String alamat;
    private String tanggalLahir;

    @OneToOne
    private Spesialisasi spesialisasi;

    private Boolean isDeleted;

}
