package com.harapansehat.hospital.entity;

import lombok.*;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "users")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String username;
    private String password;
    private String token;
    private Date tanggalLogin;

    @OneToOne
    private Pasien pasien;

    @OneToOne
    private Dokter dokter;

    @CreationTimestamp
    protected Date createdAt;

    @UpdateTimestamp
    protected Date updatedAt;

    @Enumerated(EnumType.STRING)
    private UserRole userRole;

    @OneToOne
    private Resepsionis resepsionis;

    private Boolean isDeleted;
}