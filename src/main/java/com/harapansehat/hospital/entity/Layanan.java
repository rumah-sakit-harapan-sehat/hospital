package com.harapansehat.hospital.entity;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
public class Layanan {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String namaLayanan;

    private Boolean isDeleted;
}
