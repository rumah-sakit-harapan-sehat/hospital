package com.harapansehat.hospital.entity;

import javax.persistence.*;
import lombok.Data;

@Data
@Entity
public class RekamMedis {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private Double tinggiBadan;
    private Double beratBadan;
    private String tensiDarah;
    private String gejala;
    private String alergi;

    @ManyToOne
    private Pasien pasien;

    @OneToOne Treatment treatment;

    private Boolean isDeleted;
}
