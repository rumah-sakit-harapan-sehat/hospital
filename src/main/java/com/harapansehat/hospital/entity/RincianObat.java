package com.harapansehat.hospital.entity;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
public class RincianObat {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @ManyToOne
    private Obat obat;

    private Integer kuantitas;

    @ManyToOne
    private Treatment treatment;
}
