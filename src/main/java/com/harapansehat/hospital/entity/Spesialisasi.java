package com.harapansehat.hospital.entity;

import javax.persistence.*;

import lombok.Data;

import java.math.BigDecimal;

@Data
@Entity
public class Spesialisasi {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String namaSpesialis;
    private BigDecimal harga;
    private String urlImage;
    private Boolean isDeleted;
}
