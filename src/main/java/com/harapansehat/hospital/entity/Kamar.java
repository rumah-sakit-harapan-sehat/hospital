package com.harapansehat.hospital.entity;

import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;

@Data
@Entity
public class Kamar {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String jenisKamar;
    private BigDecimal hargaKamar;
    private String deskripsi;

    private Boolean isDeleted;
}
