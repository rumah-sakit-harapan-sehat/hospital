package com.harapansehat.hospital.security.mapper;

import com.harapansehat.hospital.dto.dokter.DokterRequest;
import com.harapansehat.hospital.dto.pasien.PasienRequest;
import com.harapansehat.hospital.dto.resepsionis.ResepsionisRequest;
import com.harapansehat.hospital.entity.Dokter;
import com.harapansehat.hospital.entity.Pasien;
import com.harapansehat.hospital.entity.Resepsionis;
import com.harapansehat.hospital.entity.User;
import com.harapansehat.hospital.security.dto.AuthenticatedUserDto;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface UserMapper {
    UserMapper INSTANCE = Mappers.getMapper(UserMapper.class);

    Dokter convertToDokter(DokterRequest dokterRequest);

    Pasien convertToPasien(PasienRequest pasienRequest);

    Resepsionis convertToResepsionis(ResepsionisRequest resepsionisRequest);

    AuthenticatedUserDto convertToAuthenticatedUserDto(User user);

    User convertToUser(AuthenticatedUserDto authenticatedUserDto);

    User convertToUser(User user);
}
