package com.harapansehat.hospital.security.dto;

import com.harapansehat.hospital.entity.UserRole;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class AuthenticatedUserDto {
    private String name;

    private String username;

    private String password;

    private UserRole userRole;
}
