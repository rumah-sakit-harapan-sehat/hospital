package com.harapansehat.hospital.security.jwt;

import com.harapansehat.hospital.entity.User;
import com.harapansehat.hospital.entity.UserRole;
import com.harapansehat.hospital.repository.UserRepository;
import com.harapansehat.hospital.security.dto.AuthenticatedUserDto;
import com.harapansehat.hospital.dto.login.LoginRequest;
import com.harapansehat.hospital.dto.login.LoginResponse;
import com.harapansehat.hospital.security.mapper.UserMapper;
import com.harapansehat.hospital.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.stereotype.Service;

import java.util.Date;

@Slf4j
@Service
@RequiredArgsConstructor
public class JwtTokenService {

    private final UserService userService;

    private final UserRepository userRepository;

    private final JwtTokenManager jwtTokenManager;

    private final AuthenticationManager authenticationManager;

    public LoginResponse getLoginResponse(LoginRequest loginRequest) {

        final String username = loginRequest.getUsername();
        final String password = loginRequest.getPassword();
        final User userExist = userService.userExistValidation(username);

        final UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = new UsernamePasswordAuthenticationToken(username, password);

        authenticationManager.authenticate(usernamePasswordAuthenticationToken);

        final AuthenticatedUserDto authenticatedUserDto = userService.findAuthenticatedUserByUsername(username);

        final User user = UserMapper.INSTANCE.convertToUser(authenticatedUserDto);
        final String token = jwtTokenManager.generateToken(user);

        user.setId(userExist.getId());
        user.setTanggalLogin(new Date());
        user.setCreatedAt(userExist.getCreatedAt());
        user.setIsDeleted(false);
        user.setToken(token);

        Integer idUser = 0;

        if (user.getUserRole().equals(UserRole.DOKTER)) {
            user.setDokter(userExist.getDokter());
            idUser = userExist.getDokter().getId();
        } else if (user.getUserRole().equals(UserRole.PASIEN)) {
            user.setPasien(userExist.getPasien());
            idUser = userExist.getPasien().getId();
        } else if (user.getUserRole().equals(UserRole.RESEPSIONIS)) {
            user.setResepsionis(userExist.getResepsionis());
            idUser = userExist.getResepsionis().getId();
        }

        userRepository.save(user);

        return new LoginResponse(idUser, token, user.getTanggalLogin(), user.getUserRole().toString());
    }
}
