package com.harapansehat.hospital.Testing.repository;

import com.harapansehat.hospital.RumahSakitHarapanSehatApplication;
import com.harapansehat.hospital.entity.Pasien;
import com.harapansehat.hospital.repository.PasienRepository;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest(classes = RumahSakitHarapanSehatApplication.class)
public class PasienRepositoryTests {

    @Autowired
    private PasienRepository pasienRepository;

    Pasien pasien = new Pasien();

    public Pasien generatPasien(String nik, String namaPasien, String jenisKelamin, String alamat, String tanggalLahir){
        pasien.setNik(nik);
        pasien.setNamaPasien(namaPasien);
        pasien.setJenisKelamin(jenisKelamin);
        pasien.setAlamat(alamat);
        pasien.setTanggalLahir(tanggalLahir);
        return pasien;
    }
}
