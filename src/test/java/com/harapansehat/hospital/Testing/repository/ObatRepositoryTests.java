package com.harapansehat.hospital.Testing.repository;

import com.harapansehat.hospital.RumahSakitHarapanSehatApplication;
import com.harapansehat.hospital.entity.Obat;
import com.harapansehat.hospital.repository.ObatRepository;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.math.BigDecimal;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest(classes = RumahSakitHarapanSehatApplication.class)
class ObatRepositoryTests {

    @Autowired
    private ObatRepository obatRepository;

    Obat obat = new Obat();

    @Test
     void testSavePositifCase(){
        obat.setId(1);
        obatRepository.save(obat);
        Obat o = obatRepository.findById(obat.getId()).get();
        assertEquals(obat.getId(), o.getId());
    }

    @Test
     void testNegatifPositifCase(){
        obat.setId(1);
        obatRepository.save(obat);
        Obat o = obatRepository.findById(obat.getId()).get();
        assertEquals(obat.getId(), o.getId());
    }

    public Obat generatObat(String namaObat, String jenisObat, BigDecimal harga, String keterangan){
        obat.setNamaObat(namaObat);
        obat.setJenisObat(jenisObat);
        obat.setHarga(harga);
        obat.setKeterangan(keterangan);
        return obat;
    }

    @Test
     void saveObat(){
        Obat o = generatObat("bodrek", "kapsul", BigDecimal.valueOf(5000.00).setScale(2), "obat pusing obat apa ja dah bisa, bisa mati.");
        Obat saveObat = obatRepository.save(o);
        Optional<Obat> optional = obatRepository.findById(saveObat.getId());

        assertTrue(optional.isPresent());
        assertEquals("bodrek", optional.get().getNamaObat());
        assertEquals("kapsul", optional.get().getJenisObat());
        assertEquals(o.getHarga(), optional.get().getHarga());
        assertEquals("obat pusing obat apa ja dah bisa, bisa mati.", optional.get().getKeterangan());
    }

    @Test
     void testUpdateObat(){
        Obat o = generatObat("bodrek", "kapsul", BigDecimal.valueOf(5000.00).setScale(2), "obat pusing obat apa ja dah bisa, bisa mati.");
        Obat saveObat = obatRepository.save(o);
        o.setNamaObat("micsa");
        o.setJenisObat("Kap");
        o.setHarga(new BigDecimal(10000.00).setScale(2));
        o.setKeterangan("INI OBAT UNTUK MATI SYAHID.");
        obatRepository.save(saveObat);

        Optional<Obat> optional = obatRepository.findById(saveObat.getId());
        assertTrue(optional.isPresent());
        assertEquals("micsa", optional.get().getNamaObat());
        assertEquals("Kap", optional.get().getJenisObat());
        assertEquals(o.getHarga(), optional.get().getHarga());
        assertEquals("INI OBAT UNTUK MATI SYAHID.", optional.get().getKeterangan());
    }

}
