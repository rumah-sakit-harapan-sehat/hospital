package com.harapansehat.hospital.Testing.repository;

import com.harapansehat.hospital.RumahSakitHarapanSehatApplication;
import com.harapansehat.hospital.entity.Dokter;
import com.harapansehat.hospital.entity.Kamar;
import com.harapansehat.hospital.repository.DokterRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest(classes = RumahSakitHarapanSehatApplication.class)
class DokterRepositoryTests {

    @Autowired
    DokterRepository dokterRepository;

    Dokter dokter = new Dokter();

    public Dokter generateDokter(String nip, String namaDokter, String jenisKelamin, String alamat, String tanggalLahir){
        dokter.setNip(nip);
        dokter.setNamaDokter(namaDokter);
        dokter.setJenisKelamin(jenisKelamin);
        dokter.setAlamat(alamat);
        dokter.setTanggalLahir(tanggalLahir);
        return dokter;
    }

    @Test
    void testSaveDokter() {
        Dokter dokter = generateDokter("12345", "giri", "laki-laki", "bogor", "2004-07-04");
        Dokter saved = dokterRepository.save(dokter);
        Optional<Dokter> optionalDokter = dokterRepository.findById(saved.getId());

        Assertions.assertTrue(optionalDokter.isPresent());
        Assertions.assertEquals("12345", optionalDokter.get().getNip());
        Assertions.assertEquals("giri", optionalDokter.get().getNamaDokter());
        Assertions.assertEquals("laki-laki", optionalDokter.get().getJenisKelamin());
        Assertions.assertEquals("bogor", optionalDokter.get().getAlamat());
        Assertions.assertEquals("2004-07-04", optionalDokter.get().getTanggalLahir());
    }

    @Test
    void testUpdateDokter() {
        Dokter dokter = generateDokter("12345", "giri", "laki-laki", "bogor", "2004-07-04");
        dokter.setNip("1234");
        dokter.setNamaDokter("hamzah");
        dokter.setJenisKelamin("laki");
        dokter.setAlamat("Bogor Timur");
        dokter.setTanggalLahir("04-07-2004");
        dokterRepository.save(dokter);

        Dokter saved = dokterRepository.save(dokter);
        Optional<Dokter> optionalDokter = dokterRepository.findById(saved.getId());

        Assertions.assertTrue(optionalDokter.isPresent());
        Assertions.assertEquals("1234", optionalDokter.get().getNip());
        Assertions.assertEquals("hamzah", optionalDokter.get().getNamaDokter());
        Assertions.assertEquals("laki", optionalDokter.get().getJenisKelamin());
        Assertions.assertEquals("Bogor Timur", optionalDokter.get().getAlamat());
        Assertions.assertEquals("04-07-2004", optionalDokter.get().getTanggalLahir());
    }

    @Test
    void testDeleteDokter() {
        Dokter dokter = generateDokter("12345", "giri", "laki-laki", "bogor", "2004-07-04");
        Dokter saved = dokterRepository.save(dokter);
        dokterRepository.delete(saved);

        Optional<Dokter> optionalDokter = dokterRepository.findById(saved.getId());
        Assertions.assertFalse(optionalDokter.isPresent());
    }
}
