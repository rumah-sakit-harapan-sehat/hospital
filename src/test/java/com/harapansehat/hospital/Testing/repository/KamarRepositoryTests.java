package com.harapansehat.hospital.Testing.repository;

import com.harapansehat.hospital.RumahSakitHarapanSehatApplication;
import com.harapansehat.hospital.entity.Kamar;
import com.harapansehat.hospital.repository.KamarRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.math.BigDecimal;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest(classes = RumahSakitHarapanSehatApplication.class)
class KamarRepositoryTests {

    @Autowired
    KamarRepository kamarRepository;

    Kamar kamar = new Kamar();

    @Test
    void testSavePositifCase(){
        kamar.setId(1);
        kamarRepository.save(kamar);
        Kamar k = kamarRepository.findById(kamar.getId()).get();
        assertEquals(kamar.getId(), k.getId());
    }

    @Test
    void testNegatifPositifCase(){
        kamar.setId(1);
        kamarRepository.save(kamar);
        Kamar k = kamarRepository.findById(kamar.getId()).get();
        assertEquals(kamar.getId(), k.getId());
    }

    public Kamar generatKamar(String namaKamar, BigDecimal hargaKamar, String deskripsi){
        kamar.setJenisKamar(namaKamar);
        kamar.setHargaKamar(hargaKamar);
        kamar.setDeskripsi(deskripsi);
        return kamar;
    }

    @Test
     void testSaveKamar(){
        Kamar k = generatKamar("VIP", BigDecimal.valueOf(50000.00).setScale(2), "Ini class vip");
        Kamar saveKamar = kamarRepository.save(k);
        Optional<Kamar> optional = kamarRepository.findById(saveKamar.getId());

        assertTrue(optional.isPresent());
        assertEquals("VIP", optional.get().getJenisKamar());
        assertEquals(k.getHargaKamar(), optional.get().getHargaKamar());
        assertEquals("Ini class vip", optional.get().getDeskripsi());
    }

    @Test
     void testUpdateKamar(){
        Kamar k = generatKamar("VIP", BigDecimal.valueOf(50000.00).setScale(2), "Ini class vip");
        Kamar saveKamar = kamarRepository.save(k);
        k.setJenisKamar("CLASS1");
        k.setHargaKamar(new BigDecimal("10000.00").setScale(2));
        k.setDeskripsi("Ini CLASS1");
        kamarRepository.save(saveKamar);

        Optional<Kamar> optional = kamarRepository.findById(saveKamar.getId());
        assertTrue(optional.isPresent());
        assertEquals("CLASS1", optional.get().getJenisKamar());
        assertEquals(k.getHargaKamar(), optional.get().getHargaKamar());
        assertEquals("Ini CLASS1", optional.get().getDeskripsi());
    }
}
