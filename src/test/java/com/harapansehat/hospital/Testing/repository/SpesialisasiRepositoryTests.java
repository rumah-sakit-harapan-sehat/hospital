package com.harapansehat.hospital.Testing.repository;

import com.harapansehat.hospital.RumahSakitHarapanSehatApplication;
import com.harapansehat.hospital.entity.Obat;
import com.harapansehat.hospital.entity.Spesialisasi;
import com.harapansehat.hospital.repository.SpesialisasiRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest(classes = RumahSakitHarapanSehatApplication.class)
class SpesialisasiRepositoryTests {

    @Autowired
    private SpesialisasiRepository spesialisasiRepository;

    Spesialisasi spesialis = new Spesialisasi();

    @Test
    void testSavePositifCase(){
        spesialis.setId(2);
        spesialisasiRepository.save(spesialis);
        Spesialisasi s = spesialisasiRepository.findById(spesialis.getId()).get();
        assertEquals(spesialis.getId(), s.getId());
    }

    @Test
    void testNegatifPositifCase(){
        spesialis.setId(2);
        spesialisasiRepository.save(spesialis);
        Spesialisasi s = spesialisasiRepository.findById(spesialis.getId()).get();
        assertEquals(spesialis.getId(), s.getId());
    }

    public Spesialisasi generatSpesialis(String namaSpesialis){
        spesialis.setNamaSpesialis(namaSpesialis);
        return spesialis;
    }

    @Test
     void testSaveSpesialis(){
        Spesialisasi s = generatSpesialis("INI");
        Spesialisasi saveSpesialis = spesialisasiRepository.save(s);
        Optional<Spesialisasi> optional = spesialisasiRepository.findById(saveSpesialis.getId());

        assertTrue(optional.isPresent());
        assertEquals("INI", optional.get().getNamaSpesialis());
    }

    @Test
     void testUpdateSpesialis(){
        Spesialisasi s = generatSpesialis("INI");
        Spesialisasi saveSpesialis = spesialisasiRepository.save(s);
        s.setNamaSpesialis("ITU");
        spesialisasiRepository.save(saveSpesialis);

        Optional<Spesialisasi> optional = spesialisasiRepository.findById(saveSpesialis.getId());
        assertTrue(optional.isPresent());
        assertEquals("ITU", optional.get().getNamaSpesialis());
    }
}
