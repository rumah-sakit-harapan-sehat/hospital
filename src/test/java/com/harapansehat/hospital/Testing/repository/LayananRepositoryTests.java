package com.harapansehat.hospital.Testing.repository;

import com.harapansehat.hospital.RumahSakitHarapanSehatApplication;
import com.harapansehat.hospital.entity.Layanan;
import com.harapansehat.hospital.repository.LayananRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest(classes = RumahSakitHarapanSehatApplication.class)
class LayananRepositoryTests {

    @Autowired
    LayananRepository layananRepository;

    Layanan layanan = new Layanan();

    @Test
    void testSavePositifCase(){
        layanan.setId(1);
        layananRepository.save(layanan);
        Layanan l = layananRepository.findById(layanan.getId()).get();
        assertEquals(layanan.getId(), l.getId());
    }

    @Test
     void testNegatifPositifCase(){
        layanan.setId(1);
        layananRepository.save(layanan);
        Layanan l = layananRepository.findById(layanan.getId()).get();
        assertEquals(layanan.getId(), l.getId());
    }

    public Layanan generatLayanan(String namaLayanan){
        layanan.setNamaLayanan(namaLayanan);
        return layanan;
    }

    @Test
     void testSaveLayanan(){
        Layanan l = generatLayanan("RawatInap");
        Layanan saveLayanan = layananRepository.save(l);
        Optional<Layanan> optional = layananRepository.findById(saveLayanan.getId());

        assertTrue(optional.isPresent());
        assertEquals("RawatInap", optional.get().getNamaLayanan());
    }

    @Test
     void testUpdateLayanan() {
        Layanan l = generatLayanan("RawatInap");
        Layanan saveLayanan = layananRepository.save(l);
        l.setNamaLayanan("UGD");
        layananRepository.save(saveLayanan);

        Optional<Layanan> optional = layananRepository.findById(saveLayanan.getId());
        assertTrue(optional.isPresent());
        assertEquals("UGD", optional.get().getNamaLayanan());
    }
}
