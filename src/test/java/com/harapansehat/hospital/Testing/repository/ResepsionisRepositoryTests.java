package com.harapansehat.hospital.Testing.repository;

import com.harapansehat.hospital.RumahSakitHarapanSehatApplication;
import com.harapansehat.hospital.entity.Resepsionis;
import com.harapansehat.hospital.repository.ResepsionisRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Optional;

@SpringBootTest(classes = RumahSakitHarapanSehatApplication.class)
class ResepsionisRepositoryTests {

    @Autowired
    ResepsionisRepository resepsionisRepository;

    Resepsionis resepsionis = new Resepsionis();

    public Resepsionis generatResepsionis(String namaResepsionis, String alamat, String jenisKelamin){
        resepsionis.setNamaResepsionis(namaResepsionis);
        resepsionis.setAlamat(alamat);
        resepsionis.setJenisKelamin(jenisKelamin);
        return resepsionis;
    }

    @Test
    void testSaveResepsionis() {
        Resepsionis resepsionis = generatResepsionis("giri", "bogor", "laki-laki");
        Resepsionis saveResep = resepsionisRepository.save(resepsionis);
        Optional<Resepsionis> optionalResepsionis = resepsionisRepository.findById(saveResep.getId());

        Assertions.assertTrue(optionalResepsionis.isPresent());
        Assertions.assertEquals("giri", optionalResepsionis.get().getNamaResepsionis());
        Assertions.assertEquals("bogor", optionalResepsionis.get().getAlamat());
        Assertions.assertEquals("laki-laki", optionalResepsionis.get().getJenisKelamin());
    }

    @Test
    void testUpdateResepsionis() {
        Resepsionis resepsionis = generatResepsionis("giri", "bogor", "laki-laki");
        resepsionis.setNamaResepsionis("hamzah");
        resepsionis.setAlamat("bandung");
        resepsionis.setJenisKelamin("Laki-laki");
        resepsionisRepository.save(resepsionis);

        Resepsionis saveResep = resepsionisRepository.save(resepsionis);
        Optional<Resepsionis> optionalResepsionis = resepsionisRepository.findById(saveResep.getId());

        Assertions.assertTrue(optionalResepsionis.isPresent());
        Assertions.assertEquals("hamzah", optionalResepsionis.get().getNamaResepsionis());
        Assertions.assertEquals("bandung", optionalResepsionis.get().getAlamat());
        Assertions.assertEquals("Laki-laki", optionalResepsionis.get().getJenisKelamin());
    }
}
