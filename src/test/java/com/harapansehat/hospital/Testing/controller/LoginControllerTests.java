package com.harapansehat.hospital.Testing.controller;

import com.harapansehat.hospital.controller.LoginController;
import com.harapansehat.hospital.dto.BaseResponse;
import com.harapansehat.hospital.dto.login.LoginRequest;
import com.harapansehat.hospital.dto.login.LoginResponse;
import com.harapansehat.hospital.security.jwt.JwtTokenService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Date;

import static org.mockito.Mockito.when;

class LoginControllerTests {

    @InjectMocks
    LoginController loginController;

    @Mock
    JwtTokenService jwtTokenService;


    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void testSaveLogin() {
        LoginRequest request = new LoginRequest();
        LoginResponse mockResponse = new LoginResponse(1, "kjfadjhfaj", new Date(2023-12-11), "pasien");

        when(jwtTokenService.getLoginResponse(request)).thenReturn(mockResponse);
        ResponseEntity<BaseResponse<LoginResponse>> responseEntity = loginController.login(request);

        Assertions.assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        Assertions.assertTrue(responseEntity.getBody().getSuccess());
        Assertions.assertEquals("Operation success", responseEntity.getBody().getMessage());
        Assertions.assertEquals(mockResponse, responseEntity.getBody().getData());
    }
}
