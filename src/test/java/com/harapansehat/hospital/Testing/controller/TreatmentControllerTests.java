package com.harapansehat.hospital.Testing.controller;

import com.harapansehat.hospital.controller.TreatmentController;
import com.harapansehat.hospital.dto.BaseResponse;
import com.harapansehat.hospital.dto.treatment.DataRincianObat;
import com.harapansehat.hospital.dto.treatment.DetailTreatment;
import com.harapansehat.hospital.dto.treatment.TreatmentRequest;
import com.harapansehat.hospital.dto.treatment.TreatmentResponse;
import com.harapansehat.hospital.dto.visit.VisitRequest;
import com.harapansehat.hospital.dto.visit.VisitResponse;
import com.harapansehat.hospital.entity.Obat;
import com.harapansehat.hospital.service.TreatmentService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockHttpServletResponse;

import javax.servlet.http.HttpServletResponse;
import javax.xml.crypto.Data;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import static org.mockito.Mockito.*;

class TreatmentControllerTests {

    @InjectMocks
    TreatmentController treatmentController;

    @Mock
    TreatmentService treatmentService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void testGetAllTreatment() {
        TreatmentService treatmentService = mock(TreatmentService.class);
        TreatmentController treatmentController = new TreatmentController(treatmentService);

       List<DataRincianObat> obat = new ArrayList<>();

        List<TreatmentResponse> mockResponse = Arrays.asList(
                new TreatmentResponse(1, 1, "giri", 1, "hamzah", "THT", obat, 1, "NYERI", "GWS", "INCOMING", new Date()),
                new TreatmentResponse(2, 1, "giri", 1, "hamzah", "THT", obat, 1, "NYERI", "GWS", "INCOMING", new Date())
        );

        when(treatmentService.getAllTreatment()).thenReturn(mockResponse);
        ResponseEntity<BaseResponse<List<TreatmentResponse>>> responseEntity = treatmentController.getAllTreatment();

        Assertions.assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        Assertions.assertTrue(responseEntity.getBody().getSuccess());
        Assertions.assertEquals("Operation success", responseEntity.getBody().getMessage());
        Assertions.assertEquals(mockResponse, responseEntity.getBody().getData());

        verify(treatmentService, times(1)).getAllTreatment();
    }

    @Test
    void testGetTreatmentByDokterId() {
        TreatmentService treatmentService = mock(TreatmentService.class);
        TreatmentController treatmentController = new TreatmentController(treatmentService);

        Integer dokterId = 1;
        List<DataRincianObat> obat = new ArrayList<>();
        List<TreatmentResponse> mockTreatment = Arrays.asList(
                new TreatmentResponse(1, 1, "giri", 1, "hamzah", "THT", obat, 1, "NYERI", "GWS", "INCOMING", new Date())
        );

        when(treatmentService.getAllTreatmentByDokterId(dokterId)).thenReturn(mockTreatment);
        ResponseEntity<BaseResponse<List<TreatmentResponse>>> responseEntity = treatmentController.getAllTreatmentByDokterId(dokterId);

        Assertions.assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        Assertions.assertTrue(responseEntity.getBody().getSuccess());
        Assertions.assertEquals("Operation success", responseEntity.getBody().getMessage());
        Assertions.assertEquals(mockTreatment, responseEntity.getBody().getData());

        verify(treatmentService, times(1)).getAllTreatmentByDokterId(dokterId);
    }

    @Test
    void testTreatmentByPasienId() {
        TreatmentService treatmentService = mock(TreatmentService.class);
        TreatmentController treatmentController = new TreatmentController(treatmentService);

        Integer pasienId = 1;
        List<DataRincianObat> obat = new ArrayList<>();
        List<TreatmentResponse> mockTreatment = Arrays.asList(
                new TreatmentResponse(1, 1, "giri", 1, "hamzah", "THT", obat, 1, "NYERI", "GWS", "INCOMING", new Date())
        );

        when(treatmentService.getAllTreatmentByPasienId(pasienId)).thenReturn(mockTreatment);
        ResponseEntity<BaseResponse<List<TreatmentResponse>>> responseEntity = treatmentController.getAllTreatmentByPasienId(pasienId);

        Assertions.assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        Assertions.assertTrue(responseEntity.getBody().getSuccess());
        Assertions.assertEquals("Operation success", responseEntity.getBody().getMessage());
        Assertions.assertEquals(mockTreatment, responseEntity.getBody().getData());

        verify(treatmentService, times(1)).getAllTreatmentByPasienId(pasienId);
    }

    @Test
    void testSaveTreatment() {
        VisitRequest request = new VisitRequest();
        VisitResponse mockResponse = new VisitResponse(1, 1, 1, new Date());

        when(treatmentService.addTreatment(request)).thenReturn(mockResponse);
        ResponseEntity<BaseResponse<VisitResponse>> responseEntity = treatmentController.addTreatment(request);

        Assertions.assertEquals(HttpStatus.CREATED, responseEntity.getStatusCode());
        Assertions.assertTrue(responseEntity.getBody().getSuccess());
        Assertions.assertEquals("Operation success", responseEntity.getBody().getMessage());
        Assertions.assertEquals(mockResponse, responseEntity.getBody().getData());

        verify(treatmentService, times(1)).addTreatment(request);
    }

    @Test
    void testGetTreatmentById() {
        TreatmentService treatmentService = mock(TreatmentService.class);
        TreatmentController treatmentController = new TreatmentController(treatmentService);

        Integer getTreatmentById = 1;
        DetailTreatment detailTreatment = new DetailTreatment("giri", "rawat inap", "laki-laki", "bogor", new ArrayList<>());

        when(treatmentService.getDetailTreatment(getTreatmentById)).thenReturn(detailTreatment);
        ResponseEntity<BaseResponse<DetailTreatment>> responseEntity = treatmentController.getDetailTreatment(getTreatmentById);

        Assertions.assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        Assertions.assertTrue(responseEntity.getBody().getSuccess());
        Assertions.assertEquals("Operation success", responseEntity.getBody().getMessage());
        Assertions.assertEquals(detailTreatment, responseEntity.getBody().getData());

        verify(treatmentService, times(1)).getDetailTreatment(getTreatmentById);
    }

    @Test
    void testUpdateTreatment() {
        TreatmentService treatmentService = mock(TreatmentService.class);
        TreatmentController treatmentController = new TreatmentController(treatmentService);

        List<DataRincianObat> obat = new ArrayList<>();
        Integer treatmentId = 1;
        TreatmentRequest request = new TreatmentRequest();
        TreatmentResponse mockResponse = new TreatmentResponse(treatmentId, 1, "giri", 1, "hamzah", "THT", obat, 1, "NYERI", "GWS", "INCOMING", new Date());

        when(treatmentService.updateTreatment(treatmentId, request)).thenReturn(mockResponse);
        ResponseEntity<BaseResponse<TreatmentResponse>> responseEntity = treatmentController.updateTreatment(treatmentId, request);

        Assertions.assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        Assertions.assertTrue(responseEntity.getBody().getSuccess());
        Assertions.assertEquals("Operation success", responseEntity.getBody().getMessage());
        Assertions.assertEquals(mockResponse, responseEntity.getBody().getData());

        verify(treatmentService, times(1)).updateTreatment(treatmentId, request);
    }

    @Test
    void testPDFTreatment() {
        int treatmentId = 1;
        DetailTreatment detailTreatment = new DetailTreatment("giri", "2004-07-04", "laki-laki", "bogor", new ArrayList<>());
        when(treatmentService.getDetailTreatment(treatmentId)).thenReturn(detailTreatment);

        MockHttpServletResponse response = new MockHttpServletResponse();

        treatmentController.generatePdfFile(treatmentId, response);
        verify(treatmentService).getDetailTreatment(treatmentId);

    }
}
