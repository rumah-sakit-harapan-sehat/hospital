package com.harapansehat.hospital.Testing.controller;

import com.harapansehat.hospital.controller.ObatController;
import com.harapansehat.hospital.dto.BaseResponse;
import com.harapansehat.hospital.dto.obat.ObatRequest;
import com.harapansehat.hospital.dto.obat.ObatResponse;
import com.harapansehat.hospital.entity.Obat;
import com.harapansehat.hospital.service.ObatService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.*;

class ObatControllerTests {

    @InjectMocks
    ObatController obatController;

    @Mock
    ObatService obatService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
     void testGetAllObat(){
        ObatService obatService = mock(ObatService.class);
        ObatController obatController = new ObatController(obatService);

        List<ObatResponse> mockResponses = Arrays.asList(
               new ObatResponse(1, "tess", "Kapsul", BigDecimal.valueOf(10000.00).setScale(2), "ini tess", false),
               new ObatResponse(2, "tess2", "tablet", BigDecimal.valueOf(50000.00).setScale(2), "ini tess2", false)
        );

        when(obatService.getAllObat()).thenReturn(mockResponses);
        ResponseEntity<BaseResponse<List<ObatResponse>>> responseEntity = obatController.getAllObat();

        Assertions.assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        Assertions.assertTrue(responseEntity.getBody().getSuccess());
        Assertions.assertEquals("Operation success", responseEntity.getBody().getMessage());
        Assertions.assertEquals(mockResponses, responseEntity.getBody().getData());
    }

    @Test
    void testGetObatBy() {
        ObatService obatService = mock(ObatService.class);
        ObatController obatController = new ObatController(obatService);

        Integer obatId = 1;
        Obat mockObat = new Obat();

        when(obatService.getObatById(obatId)).thenReturn(mockObat);
        ResponseEntity<BaseResponse<Obat>> responseEntity = obatController.getObatById(obatId);

        Assertions.assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        Assertions.assertTrue(responseEntity.getBody().getSuccess());
        Assertions.assertEquals("Operation success", responseEntity.getBody().getMessage());
        Assertions.assertEquals(mockObat, responseEntity.getBody().getData());
    }

    @Test
     void testSaveObat(){
        ObatRequest request = new ObatRequest();
        ObatResponse mockResponse = new ObatResponse(1, "BODREK", "KAPSUL", BigDecimal.valueOf(1500.00).setScale(2), "INI OBAT PUSING.", false);

        when(obatService.addObat(request)).thenReturn(mockResponse);
        ResponseEntity<BaseResponse<ObatResponse>> responseEntity = obatController.addObat(request);

        Assertions.assertEquals(HttpStatus.CREATED, responseEntity.getStatusCode());
        Assertions.assertTrue(responseEntity.getBody().getSuccess());
        Assertions.assertEquals("Operation success", responseEntity.getBody().getMessage());
        Assertions.assertEquals(mockResponse, responseEntity.getBody().getData());
    }

    @Test
     void testUpdate() {
        ObatService obatService = mock(ObatService.class);
        ObatController obatController = new ObatController(obatService);

        Integer obatId = 1;
        ObatRequest request = new ObatRequest();
        ObatResponse mockResponse = new ObatResponse(obatId, "bodrek", "Kapsul", BigDecimal.valueOf(10000.00).setScale(2), "ini obat bodre", false);

        when(obatService.updateObat(obatId, request)).thenReturn(mockResponse);
        ResponseEntity<BaseResponse<ObatResponse>> responseEntity = obatController.updateObat(obatId, request);

        Assertions.assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        Assertions.assertTrue(responseEntity.getBody().getSuccess());
        Assertions.assertEquals("Operation success", responseEntity.getBody().getMessage());
        Assertions.assertEquals(mockResponse, responseEntity.getBody().getData());

        verify(obatService, times(1)).updateObat(obatId, request);
    }

    @Test
     void testDeleteLayanan() {
        ObatService obatService = mock(ObatService.class);
        ObatController obatController = new ObatController(obatService);

        Integer obatId = 1;
        ObatResponse mockResponse = new ObatResponse(obatId, "bodrek", "kapsul", BigDecimal.valueOf(10000.00).setScale(2), "ini bodrek", false);

        when(obatService.deleteObat(obatId)).thenReturn(mockResponse);
        ResponseEntity<BaseResponse<ObatResponse>> responseEntity = obatController.deleteObat(obatId);

        Assertions.assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        Assertions.assertTrue(responseEntity.getBody().getSuccess());
        Assertions.assertEquals("Operation success", responseEntity.getBody().getMessage());
        Assertions.assertEquals(mockResponse, responseEntity.getBody().getData());

        verify(obatService, times(1)).deleteObat(obatId);
    }
}
