package com.harapansehat.hospital.Testing.controller;

import com.harapansehat.hospital.controller.DokterController;
import com.harapansehat.hospital.dto.BaseResponse;
import com.harapansehat.hospital.dto.dokter.DokterRequest;
import com.harapansehat.hospital.dto.dokter.DokterResponse;
import com.harapansehat.hospital.entity.Dokter;
import com.harapansehat.hospital.service.DokterService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.*;

class DokterControllerTests {

    @InjectMocks
    DokterController dokterController;

    @Mock
    DokterService dokterService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void testGetAllDokter() {
        DokterService dokterService = mock(DokterService.class);
        DokterController dokterController = new DokterController(dokterService);

        List<DokterResponse> mockResponse = Arrays.asList(
                new DokterResponse(1, "12345", "Giri", "Laki-laki", "Bogor", "Mencintai dia", "2004-07-04", false),
                new DokterResponse(2, "54321", "Hamzah", "Laki-laki", "Bogor", "Mencintai Dia", "2004-07-04", false)
        );

        when(dokterService.getAllDokter()).thenReturn(mockResponse);
        ResponseEntity<BaseResponse<List<DokterResponse>>> responseEntity = dokterController.getAllDokter();

        Assertions.assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        Assertions.assertTrue(responseEntity.getBody().getSuccess());
        Assertions.assertEquals("Operation success", responseEntity.getBody().getMessage());
        Assertions.assertEquals(mockResponse, responseEntity.getBody().getData());

        verify(dokterService, times(1)).getAllDokter();
    }

    @Test
    void testGetByIdDokter() {
        DokterService dokterService = mock(DokterService.class);
        DokterController dokterController = new DokterController(dokterService);

        Integer dokterId = 1;
        Dokter mockDokter = new Dokter();

        when(dokterService.getDokterById(dokterId)).thenReturn(mockDokter);
        ResponseEntity<BaseResponse<Dokter>> responseEntity = dokterController.getDokterById(dokterId);

        Assertions.assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        Assertions.assertTrue(responseEntity.getBody().getSuccess());
        Assertions.assertEquals("Operation success", responseEntity.getBody().getMessage());
        Assertions.assertEquals(mockDokter, responseEntity.getBody().getData());

        verify(dokterService, times(1)).getDokterById(dokterId);
    }

    @Test
    void testGetAllDokterByIdSpesialis() {
        DokterService dokterService = mock(DokterService.class);
        DokterController dokterController = new DokterController(dokterService);

        String spesialisasi = "tes";
        List<DokterResponse> mockResponse = Arrays.asList(
                new DokterResponse(1, "12345", "Giri", "Laki-laki", "Bogor", "Mencintai dia", "2004-07-04", false)
        );

        when(dokterService.getAllDokterBySpesialisasi(spesialisasi)).thenReturn(mockResponse);
        ResponseEntity<BaseResponse<List<DokterResponse>>> responseEntity = dokterController.getAllDokterBySpesialisasi(spesialisasi);

        Assertions.assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        Assertions.assertTrue(responseEntity.getBody().getSuccess());
        Assertions.assertEquals("Operation success", responseEntity.getBody().getMessage());
        Assertions.assertEquals(mockResponse, responseEntity.getBody().getData());

        verify(dokterService, times(1)).getAllDokterBySpesialisasi(spesialisasi);
    }

    @Test
    void testUpdateDokter()  {
        DokterService dokterService = mock(DokterService.class);
        DokterController dokterController = new DokterController(dokterService);

        Integer dokterId = 1;
        DokterRequest request = new DokterRequest();
        DokterResponse mockResponse = new DokterResponse(1, "5678910", "Giri", "Laki-laki", "Bogor", "Mencintai dia", "2004-07-04", false);

        when(dokterService.updateDokterById(dokterId, request)).thenReturn(mockResponse);
        ResponseEntity<BaseResponse<DokterResponse>> responseEntity = dokterController.updateDokter(dokterId, request);

        Assertions.assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        Assertions.assertTrue(responseEntity.getBody().getSuccess());
        Assertions.assertEquals("Operation success", responseEntity.getBody().getMessage());
        Assertions.assertEquals(mockResponse, responseEntity.getBody().getData());

        verify(dokterService, times(1)).updateDokterById(dokterId, request);
    }

    @Test
    void testDeleteDokter() {
        DokterService dokterService = mock(DokterService.class);
        DokterController dokterController = new DokterController(dokterService);

        Integer dokterId = 1;
        DokterResponse mockResponse = new DokterResponse(1, "5678910", "Giri", "Laki-laki", "Bogor", "Mencintai dia", "2004-07-04", false);
        when(dokterService.deleteDokterById(dokterId)).thenReturn(mockResponse);
        ResponseEntity<BaseResponse<DokterResponse>> responseEntity = dokterController.deleteDokter(dokterId);

        Assertions.assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        Assertions.assertTrue(responseEntity.getBody().getSuccess());
        Assertions.assertEquals("Operation success", responseEntity.getBody().getMessage());
        Assertions.assertEquals(mockResponse, responseEntity.getBody().getData());

        verify(dokterService, times(1)).deleteDokterById(dokterId);
    }
}
