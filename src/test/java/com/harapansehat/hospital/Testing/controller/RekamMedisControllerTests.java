package com.harapansehat.hospital.Testing.controller;

import com.harapansehat.hospital.controller.RekamMedisController;
import com.harapansehat.hospital.dto.BaseResponse;
import com.harapansehat.hospital.dto.rekamMedis.DetailRekamMedis;
import com.harapansehat.hospital.dto.rekamMedis.RekamMedisRequest;
import com.harapansehat.hospital.dto.rekamMedis.RekamMedisResponse;
import com.harapansehat.hospital.dto.treatment.DetailTreatment;
import com.harapansehat.hospital.entity.RekamMedis;
import com.harapansehat.hospital.service.RekamMedisService;
import com.harapansehat.hospital.util.PdfGenerator;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockHttpServletResponse;

import java.util.*;

import static org.mockito.Mockito.*;

class RekamMedisControllerTests {

    @InjectMocks
    RekamMedisController rekamMedisController;

    @Mock
    RekamMedisService rekamMedisService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void testGetAllRekamMedis() {
        RekamMedisService rekamMedisService = mock(RekamMedisService.class);
        RekamMedisController rekamMedisController = new RekamMedisController(rekamMedisService);

        List<RekamMedisResponse> mockResponse = Arrays.asList(
                new RekamMedisResponse(1, "123456789", "joy", "Bogor", "Perempuan", "2007-01-18", 160.0, 46.0, "120h", "tidak", "tidak", new Date(2023-12-16)),
                new RekamMedisResponse(2, "123456", "Giri", "Bogor", "laki-laki", "2004-07-04", 157.0, 59.0, "170h", "tidak", "tidak", new Date(2023-12-16))
        );

        when(rekamMedisService.getAll()).thenReturn(mockResponse);
        ResponseEntity<BaseResponse<List<RekamMedisResponse>>> responseEntity = rekamMedisController.getAll();

        Assertions.assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        Assertions.assertTrue(responseEntity.getBody().getSuccess());
        Assertions.assertEquals("Operation success", responseEntity.getBody().getMessage());
        Assertions.assertEquals(mockResponse, responseEntity.getBody().getData());

        verify(rekamMedisService, times(1)).getAll();
    }

    @Test
    void testRekamMedisById() {
        RekamMedisService rekamMedisService = mock(RekamMedisService.class);
        RekamMedisController rekamMedisController = new RekamMedisController(rekamMedisService);

        Integer rekamMedisId = 1;
        List<RekamMedisResponse> mockResponse = Arrays.asList(
                new RekamMedisResponse(1, "123456789", "joy", "Bogor", "Perempuan", "2007-01-18", 160.0, 46.0, "120h", "tidak", "tidak", new Date(2023-12-16))
        );

        when(rekamMedisService.getAllByPasienId(rekamMedisId)).thenReturn(mockResponse);
        ResponseEntity<BaseResponse<List<RekamMedisResponse>>> responseEntity = rekamMedisController.getAllByPasienId(rekamMedisId);

        Assertions.assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        Assertions.assertTrue(responseEntity.getBody().getSuccess());
        Assertions.assertEquals("Operation success", responseEntity.getBody().getMessage());
        Assertions.assertEquals(mockResponse, responseEntity.getBody().getData());
    }

    @Test
    void testAddRekamMedis() {
        RekamMedisRequest request = new RekamMedisRequest();
        RekamMedisResponse mockResponse = new RekamMedisResponse(1, "12345", "Giri", "Bogor", "laki-laki", "2004-07-04", 175.0, 59.0, "170h", "tidak", "tidak", new Date(2023-12-16));

        when(rekamMedisService.addRekamMedis(request)).thenReturn(mockResponse);
        ResponseEntity<BaseResponse<RekamMedisResponse>> responseEntity = rekamMedisController.addRekamMedis(request);

        Assertions.assertEquals(HttpStatus.CREATED, responseEntity.getStatusCode());
        Assertions.assertTrue(responseEntity.getBody().getSuccess());
        Assertions.assertEquals("Operation success", responseEntity.getBody().getMessage());
        Assertions.assertEquals(mockResponse, responseEntity.getBody().getData());

        verify(rekamMedisService, times(1)).addRekamMedis(request);
    }

    @Test
    void testPDFRekamMedis() {
        int rekamMedisId = 1;
        DetailRekamMedis detailRekamMedis = new DetailRekamMedis("giri", "2004-07-04", new ArrayList<>());
        when(rekamMedisService.getDetailRekamMedis(rekamMedisId)).thenReturn(detailRekamMedis);

        MockHttpServletResponse response = new MockHttpServletResponse();

        rekamMedisController.generatePdfFile(rekamMedisId, response);
        verify(rekamMedisService).getDetailRekamMedis(rekamMedisId);

    }
}
