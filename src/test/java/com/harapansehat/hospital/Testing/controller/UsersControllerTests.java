package com.harapansehat.hospital.Testing.controller;

import com.harapansehat.hospital.controller.UserController;
import com.harapansehat.hospital.dto.BaseResponse;
import com.harapansehat.hospital.dto.dokter.DokterRequest;
import com.harapansehat.hospital.dto.pasien.PasienRequest;
import com.harapansehat.hospital.dto.resepsionis.ResepsionisRequest;
import com.harapansehat.hospital.security.dto.RegistrationResponse;
import com.harapansehat.hospital.service.UserService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.mockito.Mockito.*;
import static org.mockito.Mockito.times;

class UsersControllerTests {

    @InjectMocks
    UserController userController;

    @Mock
    UserService userService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void registerDokter() {
        DokterRequest request = new DokterRequest();
        RegistrationResponse mockResponse = new RegistrationResponse();

        when(userService.registrationDokter(request)).thenReturn(mockResponse);
        ResponseEntity<BaseResponse<RegistrationResponse>> responseEntity = userController.registrationDokter(request);

        Assertions.assertEquals(HttpStatus.CREATED, responseEntity.getStatusCode());
        Assertions.assertTrue(responseEntity.getBody().getSuccess());
        Assertions.assertEquals("Operation success", responseEntity.getBody().getMessage());
        Assertions.assertEquals(mockResponse, responseEntity.getBody().getData());

        verify(userService, times(1)).registrationDokter(request);
    }

    @Test
    void registerPasien() {
        PasienRequest request = new PasienRequest();
        RegistrationResponse mockResponse = new RegistrationResponse();

        when(userService.registrationPasien(request)).thenReturn(mockResponse);
        ResponseEntity<BaseResponse<RegistrationResponse>> responseEntity = userController.registrationPasien(request);

        Assertions.assertEquals(HttpStatus.CREATED, responseEntity.getStatusCode());
        Assertions.assertTrue(responseEntity.getBody().getSuccess());
        Assertions.assertEquals("Operation success", responseEntity.getBody().getMessage());
        Assertions.assertEquals(mockResponse, responseEntity.getBody().getData());

        verify(userService, times(1)).registrationPasien(request);
    }

    @Test
    void testResepsionis() {
        ResepsionisRequest request = new ResepsionisRequest();
        RegistrationResponse mockResponse = new RegistrationResponse();

        when(userService.registrationResepsionis(request)).thenReturn(mockResponse);
        ResponseEntity<BaseResponse<RegistrationResponse>> responseEntity = userController.registrationResepsionis(request);

        Assertions.assertEquals(HttpStatus.CREATED, responseEntity.getStatusCode());
        Assertions.assertTrue(responseEntity.getBody().getSuccess());
        Assertions.assertEquals("Operation success", responseEntity.getBody().getMessage());
        Assertions.assertEquals(mockResponse, responseEntity.getBody().getData());

        verify(userService, times(1)).registrationResepsionis(request);
    }
}
