package com.harapansehat.hospital.Testing.controller;

import com.harapansehat.hospital.controller.KamarController;
import com.harapansehat.hospital.dto.BaseResponse;
import com.harapansehat.hospital.dto.kamar.KamarRequest;
import com.harapansehat.hospital.dto.kamar.KamarResponse;
import com.harapansehat.hospital.entity.Kamar;
import com.harapansehat.hospital.service.KamarService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.*;

class KamarControllerTests {

    @InjectMocks
    KamarController kamarController;

    @Mock
    KamarService kamarService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }


    @Test
     void testGetAllKamar() {
        KamarService kamarService = mock(KamarService.class);
        KamarController kamarController = new KamarController(kamarService);

        List<KamarResponse> mockResponse = Arrays.asList(
                new KamarResponse(2, "VIP", BigDecimal.valueOf(50000.00).setScale(2), "Ini class vip", false),
                new KamarResponse(3, "CLASS1", BigDecimal.valueOf(10000.00).setScale(2), "Ini CLASS1", false)
        );

        when(kamarService.getAllKamar()).thenReturn(mockResponse);
        ResponseEntity<BaseResponse<List<KamarResponse>>> responseEntity = kamarController.getAllKamar();

        Assertions.assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        Assertions.assertTrue(responseEntity.getBody().getSuccess());
        Assertions.assertEquals("Operation success", responseEntity.getBody().getMessage());
        Assertions.assertEquals(mockResponse, responseEntity.getBody().getData());


        verify(kamarService, times(1)).getAllKamar();
    }

    @Test
    void testGetKamarById() {
        KamarService kamarService = mock(KamarService.class);
        KamarController kamarController = new KamarController(kamarService);

        Integer kamarId = 1;
        Kamar mockkamar = new Kamar();

        when(kamarService.getKamarById(kamarId)).thenReturn(mockkamar);
        ResponseEntity<BaseResponse<Kamar>> responseEntity = kamarController.getKamarById(kamarId);

        Assertions.assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        Assertions.assertTrue(responseEntity.getBody().getSuccess());
        Assertions.assertEquals("Operation success", responseEntity.getBody().getMessage());
        Assertions.assertEquals(mockkamar, responseEntity.getBody().getData());

        verify(kamarService, times(1)).getKamarById(kamarId);
    }

    @Test
    void testSaveKamar(){
        KamarRequest request = new KamarRequest();
        KamarResponse mockResponse = new KamarResponse(1, "VIP", BigDecimal.valueOf(100000.00).setScale(2), "INI KAMAR VIP PASIEN.", false);

        when(kamarService.addKamar(request)).thenReturn(mockResponse);
        ResponseEntity<BaseResponse<KamarResponse>> responseEntity = kamarController.addKamar(request);

        Assertions.assertEquals(HttpStatus.CREATED, responseEntity.getStatusCode());
        Assertions.assertTrue(responseEntity.getBody().getSuccess());
        Assertions.assertEquals("Operation success", responseEntity.getBody().getMessage());
        Assertions.assertEquals(mockResponse, responseEntity.getBody().getData());
    }

    @Test
     void testUpdateKamar() {
        KamarService kamarService = mock(KamarService.class);
        KamarController kamarController = new KamarController(kamarService);

        Integer kamarId = 1;
        KamarRequest request = new KamarRequest();
        KamarResponse mockResponse = new KamarResponse(kamarId, "Updated VIP", BigDecimal.valueOf(150000.00).setScale(2), "UPDATED KAMAR VIP PASIEN.", false);

        when(kamarService.updateKamar(kamarId, request)).thenReturn(mockResponse);
        ResponseEntity<BaseResponse<KamarResponse>> responseEntity = kamarController.updateKamar(kamarId, request);

        Assertions.assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        Assertions.assertTrue(responseEntity.getBody().getSuccess());
        Assertions.assertEquals("Operation success", responseEntity.getBody().getMessage());
        Assertions.assertEquals(mockResponse, responseEntity.getBody().getData());

        verify(kamarService, times(1)).updateKamar(kamarId, request);
    }

    @Test
    void testDeleteKamar() {
        KamarService kamarService = mock(KamarService.class);
        KamarController kamarController = new KamarController(kamarService);

        Integer kamarId = 1;
        KamarResponse mockResponse = new KamarResponse(kamarId, "Updated VIP", BigDecimal.valueOf(150000.00).setScale(2), "UPDATED KAMAR VIP PASIEN.", false);

        when(kamarService.deleteKamar(kamarId)).thenReturn(mockResponse);
        ResponseEntity<BaseResponse<KamarResponse>> responseEntity = kamarController.deleteKamar(kamarId);

        Assertions.assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        Assertions.assertTrue(responseEntity.getBody().getSuccess());
        Assertions.assertEquals("Operation success", responseEntity.getBody().getMessage());
        Assertions.assertEquals(mockResponse, responseEntity.getBody().getData());

        verify(kamarService, times(1)).deleteKamar(kamarId);
    }
}
