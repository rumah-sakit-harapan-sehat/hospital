package com.harapansehat.hospital.Testing.controller;

import com.harapansehat.hospital.controller.PasienController;
import com.harapansehat.hospital.dto.BaseResponse;
import com.harapansehat.hospital.dto.pasien.PasienRequest;
import com.harapansehat.hospital.dto.pasien.PasienResponse;
import com.harapansehat.hospital.entity.Pasien;
import com.harapansehat.hospital.service.PasienService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.*;
import static org.mockito.Mockito.times;

class PasienControllerTests {

    @InjectMocks
    PasienController pasienController;

    @Mock
    PasienService pasienService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void testGetAllPasien() {
        PasienService pasienService = mock(PasienService.class);
        PasienController pasienController = new PasienController(pasienService);

        List<PasienResponse> mockResponse = Arrays.asList(
                new PasienResponse(1, "123456789", "syaifani", "Bogor", "perempuan", "2007-01-18", false),
                new PasienResponse(2, "987654321", "giri", "Bogor", "Laki-laki", "2004-17-04", false)
        );

        when(pasienService.getAllPasien()).thenReturn(mockResponse);
        ResponseEntity<BaseResponse<List<PasienResponse>>> responseEntity = pasienController.getAllPasien();

        Assertions.assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        Assertions.assertTrue(responseEntity.getBody().getSuccess());
        Assertions.assertEquals("Operation success", responseEntity.getBody().getMessage());
        Assertions.assertEquals(mockResponse, responseEntity.getBody().getData());
    }

    @Test
    void testGetByIdPasien() {
        PasienService pasienService = mock(PasienService.class);
        PasienController pasienController = new PasienController(pasienService);

        Integer pasienId = 1;
        Pasien mockResponse = new Pasien();

        when(pasienService.getPasienById(pasienId)).thenReturn(mockResponse);
        ResponseEntity<BaseResponse<Pasien>> responseEntity = pasienController.getPasienById(pasienId);

        Assertions.assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        Assertions.assertTrue(responseEntity.getBody().getSuccess());
        Assertions.assertEquals("Operation success", responseEntity.getBody().getMessage());
        Assertions.assertEquals(mockResponse, responseEntity.getBody().getData());

        verify(pasienService, times(1)).getPasienById(pasienId);
    }

    @Test
    void testUpdatePasien() {
        PasienService pasienService = mock(PasienService.class);
        PasienController pasienController = new PasienController(pasienService);

        Integer pasienId = 1;
        PasienRequest request = new PasienRequest();
        PasienResponse mockResponse = new PasienResponse(1, "123456789", "Hamzah", "Bogor", "Laki-laki", "2004-07-04", false);

        when(pasienService.updatePasienById(pasienId, request)).thenReturn(mockResponse);
        ResponseEntity<BaseResponse<PasienResponse>> responseEntity = pasienController.updatePasien(pasienId, request);

        Assertions.assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        Assertions.assertTrue(responseEntity.getBody().getSuccess());
        Assertions.assertEquals("Operation success", responseEntity.getBody().getMessage());
        Assertions.assertEquals(mockResponse, responseEntity.getBody().getData());

        verify(pasienService, times(1)).updatePasienById(pasienId, request);
    }

    @Test
    void testDeletePasien() {
        PasienService pasienService = mock(PasienService.class);
        PasienController pasienController = new PasienController(pasienService);

        Integer pasienId = 1;
        PasienResponse mockResponse = new PasienResponse(1, "123456789", "Hamzah", "Bogor", "Laki-laki", "2004-07-04", false);

        when(pasienService.deletePasienById(pasienId)).thenReturn(mockResponse);
        ResponseEntity<BaseResponse<PasienResponse>> responseEntity = pasienController.deletePasien(pasienId);

        Assertions.assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        Assertions.assertTrue(responseEntity.getBody().getSuccess());
        Assertions.assertEquals("Operation success", responseEntity.getBody().getMessage());
        Assertions.assertEquals(mockResponse, responseEntity.getBody().getData());

        verify(pasienService, times(1)).deletePasienById(pasienId);
    }
}
