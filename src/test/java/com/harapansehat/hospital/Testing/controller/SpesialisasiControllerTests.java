package com.harapansehat.hospital.Testing.controller;

import com.harapansehat.hospital.controller.SpesialisasiController;
import com.harapansehat.hospital.dto.BaseResponse;
import com.harapansehat.hospital.dto.spesialisasi.SpesialisRequest;
import com.harapansehat.hospital.dto.spesialisasi.SpesialisResponse;
import com.harapansehat.hospital.entity.Spesialisasi;
import com.harapansehat.hospital.service.SpesialisasiService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;


import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.*;
import static org.mockito.Mockito.times;

class SpesialisasiControllerTests {

    @InjectMocks
    SpesialisasiController spesialisasiController;

    @Mock
    SpesialisasiService spesialisasiService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
     void testGetAllSpesialis(){
        SpesialisasiService spesialisasiService = mock(SpesialisasiService.class);
        SpesialisasiController spesialisasiController = new SpesialisasiController(spesialisasiService);

        List<SpesialisResponse> mockResponse = Arrays.asList(
                new SpesialisResponse(1, "THT", BigDecimal.valueOf(10000.00).setScale(2), "spesialis.png", false),
                new SpesialisResponse(1, "Tulang", BigDecimal.valueOf(7000.00).setScale(2), "spesialis.png", false)
        );

        when(spesialisasiService.getAll()).thenReturn(mockResponse);
        ResponseEntity<BaseResponse<List<SpesialisResponse>>> responseEntity = spesialisasiController.getAll();

        Assertions.assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        Assertions.assertTrue(responseEntity.getBody().getSuccess());
        Assertions.assertEquals("Operation success", responseEntity.getBody().getMessage());
        Assertions.assertEquals(mockResponse, responseEntity.getBody().getData());
    }

    @Test
    void testSpesialisById() {
        SpesialisasiService spesialisasiService = mock(SpesialisasiService.class);
        SpesialisasiController spesialisasiController = new SpesialisasiController(spesialisasiService);

        Integer spesialisId = 1;
        Spesialisasi mockSpesialis = new Spesialisasi();

        when(spesialisasiService.getSpesialisasiById(spesialisId)).thenReturn(mockSpesialis);
        ResponseEntity<BaseResponse<Spesialisasi>> responseEntity = spesialisasiController.getSpesialisById(spesialisId);

        Assertions.assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        Assertions.assertTrue(responseEntity.getBody().getSuccess());
        Assertions.assertEquals("Operation success", responseEntity.getBody().getMessage());
        Assertions.assertEquals(mockSpesialis, responseEntity.getBody().getData());
    }

    @Test
     void testSaveSpesialis(){
        SpesialisRequest request = new SpesialisRequest();
        SpesialisResponse mockResponse = new SpesialisResponse(1,"Rawat Inap", BigDecimal.valueOf(100000.00).setScale(2), "spesialis.png", false);

        when(spesialisasiService.addSpesialis(request)).thenReturn(mockResponse);
        ResponseEntity<BaseResponse<SpesialisResponse>> responseEntity = spesialisasiController.addSpesialis(request);

        Assertions.assertEquals(HttpStatus.CREATED, responseEntity.getStatusCode());
        Assertions.assertTrue(responseEntity.getBody().getSuccess());
        Assertions.assertEquals("Operation success", responseEntity.getBody().getMessage());
        Assertions.assertEquals(mockResponse, responseEntity.getBody().getData());
    }

    @Test
     void testUpdateSpesialis() {
        SpesialisasiService spesialisasiService = mock(SpesialisasiService.class);
        SpesialisasiController spesialisasiController = new SpesialisasiController(spesialisasiService);

        Integer spesialisId = 1;
        SpesialisRequest request = new SpesialisRequest();
        SpesialisResponse mockResponse = new SpesialisResponse(spesialisId, "THT", BigDecimal.valueOf(10000.00).setScale(2), "spesialis.png", false);

        when(spesialisasiService.updateSpesialis(spesialisId, request)).thenReturn(mockResponse);
        ResponseEntity<BaseResponse<SpesialisResponse>> responseEntity = spesialisasiController.updateSpesialis(spesialisId, request);

        Assertions.assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        Assertions.assertTrue(responseEntity.getBody().getSuccess());
        Assertions.assertEquals("Operation success", responseEntity.getBody().getMessage());
        Assertions.assertEquals(mockResponse, responseEntity.getBody().getData());

        verify(spesialisasiService, times(1)).updateSpesialis(spesialisId, request);
    }

    @Test
     void testDeleteSpesialisasi() {
        SpesialisasiService spesialisasiService = mock(SpesialisasiService.class);
        SpesialisasiController spesialisasiController = new SpesialisasiController(spesialisasiService);

        Integer spesialisId = 1;
        SpesialisResponse mockResponse = new SpesialisResponse(spesialisId, "THT", BigDecimal.valueOf(10000.00).setScale(2), "spesialis.png", false);

        when(spesialisasiService.deleteSpesialis(spesialisId)).thenReturn(mockResponse);
        ResponseEntity<BaseResponse<SpesialisResponse>> responseEntity = spesialisasiController.deleteSpesialis(spesialisId);

        Assertions.assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        Assertions.assertTrue(responseEntity.getBody().getSuccess());
        Assertions.assertEquals("Operation success", responseEntity.getBody().getMessage());
        Assertions.assertEquals(mockResponse, responseEntity.getBody().getData());

        verify(spesialisasiService, times(1)).deleteSpesialis(spesialisId);
    }
}
