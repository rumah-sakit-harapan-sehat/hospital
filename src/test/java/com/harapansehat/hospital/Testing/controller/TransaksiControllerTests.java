package com.harapansehat.hospital.Testing.controller;

import com.google.zxing.WriterException;
import com.harapansehat.hospital.controller.TransaksiController;
import com.harapansehat.hospital.dto.BaseResponse;
import com.harapansehat.hospital.dto.transaksi.*;
import com.harapansehat.hospital.dto.treatment.DetailTreatment;
import com.harapansehat.hospital.entity.Kamar;
import com.harapansehat.hospital.entity.Obat;
import com.harapansehat.hospital.entity.Transaksi;
import com.harapansehat.hospital.service.TransaksiService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.stubbing.Answer;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.times;

class TransaksiControllerTests {

    @InjectMocks
    TransaksiController transaksiController;

    @Mock
    TransaksiService transaksiService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }


    @Test
    void testGetAllTransaksi() {
        TransaksiService transaksiService = mock(TransaksiService.class);
        TransaksiController transaksiController = new TransaksiController(transaksiService);

        List<TransaksiResponse> mockResponse = Arrays.asList(
                new TransaksiResponse(1, 1, 1, 1, 1, "nama resepsionis", BigDecimal.valueOf(50000.00).setScale(2), BigDecimal.valueOf(50000.000).setScale(2), BigDecimal.valueOf(50000.00).setScale(2), BigDecimal.valueOf(50000.00).setScale(2), new Date(2023-12-11)),
                new TransaksiResponse(2, 1, 1, 1, 1, "nama resepsionis", BigDecimal.valueOf(50000.00).setScale(2), BigDecimal.valueOf(50000.000).setScale(2), BigDecimal.valueOf(50000.00).setScale(2), BigDecimal.valueOf(50000.00).setScale(2), new Date(2023-12-11))
        );

        when(transaksiService.getAllTransaksi()).thenReturn(mockResponse);
        ResponseEntity<BaseResponse<List<TransaksiResponse>>> responseEntity = transaksiController.getAllTransaksi();

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        Assertions.assertTrue(responseEntity.getBody().getSuccess());
        assertEquals("Operation success", responseEntity.getBody().getMessage());
        assertEquals(mockResponse, responseEntity.getBody().getData());

        verify(transaksiService, times(1)).getAllTransaksi();
    }

    @Test
    void testGetTransaksiByPasienId() {
        TransaksiService transaksiService = mock(TransaksiService.class);
        TransaksiController transaksiController = new TransaksiController(transaksiService);

        Integer pasienId = 1;

        List<TransaksiResponse> mockResponse = Arrays.asList(
                new TransaksiResponse(1, 1, 1, 1, 1, "nama resepsionis", BigDecimal.valueOf(50000.00).setScale(2), BigDecimal.valueOf(50000.000).setScale(2), BigDecimal.valueOf(50000.00).setScale(2), BigDecimal.valueOf(50000.00).setScale(2), new Date(2023-12-11)),
                new TransaksiResponse(2, 1, 1, 1, 1, "nama resepsionis", BigDecimal.valueOf(50000.00).setScale(2), BigDecimal.valueOf(50000.000).setScale(2), BigDecimal.valueOf(50000.00).setScale(2), BigDecimal.valueOf(50000.00).setScale(2), new Date(2023-12-11))
        );

        when(transaksiService.getAllTransaksiByPasienId(pasienId)).thenReturn(mockResponse);
        ResponseEntity<BaseResponse<List<TransaksiResponse>>> responseEntity = transaksiController.getAllTransaksiByPasienId(pasienId);

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        Assertions.assertTrue(responseEntity.getBody().getSuccess());
        assertEquals("Operation success", responseEntity.getBody().getMessage());
        assertEquals(mockResponse, responseEntity.getBody().getData());

        verify(transaksiService, times(1)).getAllTransaksiByPasienId(pasienId);
    }

    @Test
    void testTransaksiDetail() throws IOException, WriterException {
        TransaksiService transaksiService = mock(TransaksiService.class);
        TransaksiController transaksiController = new TransaksiController(transaksiService);

        Integer transaksiId = 1;
        Kamar kamar = new Kamar();
        List<RincianTransaksiObat> obat = new ArrayList<>();
        InfoTransaksi detailTransaksi = new InfoTransaksi("12345", "Giri", "Bogor", "Hamzah", "THT", "JOY", new Date(2023-12-16), BigDecimal.valueOf(10000.00).setScale(2), BigDecimal.valueOf(20000.00).setScale(2), obat , kamar,  1, BigDecimal.valueOf(100000.00).setScale(2), "".getBytes());

        when(transaksiService.getTransaksiById(transaksiId)).thenReturn(detailTransaksi);
        ResponseEntity<BaseResponse<InfoTransaksi>> responseEntity = transaksiController.getTransaksiDetail(transaksiId);

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        Assertions.assertTrue(responseEntity.getBody().getSuccess());
        assertEquals("Operation success", responseEntity.getBody().getMessage());
        assertEquals(detailTransaksi, responseEntity.getBody().getData());

        verify(transaksiService, times(1)).getTransaksiById(transaksiId);
    }

    @Test
    void testSaveTransaksi() {
        TransaksiRequest request = new TransaksiRequest(1, 1, BigDecimal.valueOf(10000.00).setScale(2), BigDecimal.valueOf(50000.00).setScale(2), 1);
        TransaksiResponse mockResponse = new TransaksiResponse(1, 1, 1, 1, 1, "nama resepsionis", BigDecimal.valueOf(50000.00).setScale(2), BigDecimal.valueOf(50000.000).setScale(2), BigDecimal.valueOf(50000.00).setScale(2), BigDecimal.valueOf(50000.00).setScale(2), new Date(2023-12-11));

        when(transaksiService.addTransaksi(request)).thenReturn(mockResponse);
        ResponseEntity<BaseResponse<TransaksiResponse>> responseEntity = transaksiController.addTransaksi(request);

        assertEquals(HttpStatus.CREATED, responseEntity.getStatusCode());
        Assertions.assertTrue(responseEntity.getBody().getSuccess());
        assertEquals("Operation success", responseEntity.getBody().getMessage());
        assertEquals(mockResponse, responseEntity.getBody().getData());

        verify(transaksiService, times(1)).addTransaksi(request);
    }

    @Test
    void testPDFTransaksi() throws IOException, WriterException {
        int transaksiId = 1;
        Kamar kamar = new Kamar();
        List<RincianTransaksiObat> obat = new ArrayList<>();
        InfoTransaksi detailTransaksi = new InfoTransaksi("12345", "Giri", "Bogor", "Hamzah", "THT", "JOY", new Date(2023-12-16), BigDecimal.valueOf(10000.00).setScale(2), BigDecimal.valueOf(20000.00).setScale(2), obat , kamar,  1, BigDecimal.valueOf(100000.00).setScale(2), "".getBytes());
        when(transaksiService.getTransaksiById(transaksiId)).thenReturn(detailTransaksi);

        MockHttpServletResponse response = new MockHttpServletResponse();

        transaksiController.generatePdfFile(transaksiId, response);
        verify(transaksiService).getTransaksiById(transaksiId);
    }

    @Test
    void testGenerateQRCode() throws IOException, WriterException {
        int id = 1;
        byte[] mockedQRCodeBytes = new byte[]{};

        when(transaksiService.generateQRCodebyId(id)).thenReturn(mockedQRCodeBytes);

        byte[] result = transaksiController.generateQRCode(id);

        assertNotNull(result);
        verify(transaksiService).generateQRCodebyId(id);
    }
}
