package com.harapansehat.hospital.Testing.controller;

import com.harapansehat.hospital.controller.ResepsionisController;
import com.harapansehat.hospital.dto.BaseResponse;
import com.harapansehat.hospital.dto.resepsionis.ResepsionisRequest;
import com.harapansehat.hospital.dto.resepsionis.ResepsionisResponse;
import com.harapansehat.hospital.entity.Resepsionis;
import com.harapansehat.hospital.service.ResepsionisService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.*;

class  ResepsionisControllerTests {

    @InjectMocks
    ResepsionisController resepsionisController;

    @Mock
    ResepsionisService resepsionisService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void testGetAllResepsionis() {
        ResepsionisService resepsionisService = mock(ResepsionisService.class);
        ResepsionisController resepsionisController = new ResepsionisController(resepsionisService);

        List<ResepsionisResponse> mockReseponse = Arrays.asList(
                new ResepsionisResponse(1, "Giri", "Bogor", "laki-laki", false),
                new ResepsionisResponse(2, "hamzah", "Bogor", "perempuan", false)
        );

        when(resepsionisService.getAll()).thenReturn(mockReseponse);
        ResponseEntity<BaseResponse<List<ResepsionisResponse>>> responseEntity = resepsionisController.getAll();

        Assertions.assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        Assertions.assertTrue(responseEntity.getBody().getSuccess());
        Assertions.assertEquals("Operation success", responseEntity.getBody().getMessage());
        Assertions.assertEquals(mockReseponse, responseEntity.getBody().getData());

        verify(resepsionisService, times(1)).getAll();
    }

    @Test
    void testGetResepsionisById() {
        ResepsionisService resepsionisService = mock(ResepsionisService.class);
        ResepsionisController resepsionisController = new ResepsionisController(resepsionisService);

        Integer resepsionisById = 1;
        Resepsionis mockResepsionis = new Resepsionis();

        when(resepsionisService.getResepsionisById(resepsionisById)).thenReturn(mockResepsionis);
        ResponseEntity<BaseResponse<Resepsionis>> responseEntity = resepsionisController.getResepsionisById(resepsionisById);

        Assertions.assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        Assertions.assertTrue(responseEntity.getBody().getSuccess());
        Assertions.assertEquals("Operation success", responseEntity.getBody().getMessage());
        Assertions.assertEquals(mockResepsionis, responseEntity.getBody().getData());
    }

    @Test
    void testUpdateResepsionis() {
        ResepsionisService resepsionisService = mock(ResepsionisService.class);
        ResepsionisController resepsionisController = new ResepsionisController(resepsionisService);

        Integer resepsionisId = 1;
        ResepsionisRequest request = new ResepsionisRequest();
        ResepsionisResponse mockResponse = new ResepsionisResponse(resepsionisId, "Giri", "Bogor", "laki-laki", false);

        when(resepsionisService.updateResepsionis(resepsionisId, request)).thenReturn(mockResponse);
        ResponseEntity<BaseResponse<ResepsionisResponse>> responseEntity = resepsionisController.updateResepsionis(resepsionisId, request);

        Assertions.assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        Assertions.assertTrue(responseEntity.getBody().getSuccess());
        Assertions.assertEquals("Operation success", responseEntity.getBody().getMessage());
        Assertions.assertEquals(mockResponse, responseEntity.getBody().getData());

        verify(resepsionisService, times(1)).updateResepsionis(resepsionisId, request);
    }

    @Test
    void testDeleteResepsionis() {
        ResepsionisService resepsionisService = mock(ResepsionisService.class);
        ResepsionisController resepsionisController = new ResepsionisController(resepsionisService);

        Integer resepsionisId = 1;
        ResepsionisResponse mockResponse = new ResepsionisResponse(resepsionisId, "Giri", "Bogor", "laki-laki", false);

        when(resepsionisService.deleteResepsionis(resepsionisId)).thenReturn(mockResponse);
        ResponseEntity<BaseResponse<ResepsionisResponse>> responseEntity = resepsionisController.deleteResepsionis(resepsionisId);

        Assertions.assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        Assertions.assertTrue(responseEntity.getBody().getSuccess());
        Assertions.assertEquals("Operation success", responseEntity.getBody().getMessage());
        Assertions.assertEquals(mockResponse, responseEntity.getBody().getData());

        verify(resepsionisService, times(1)).deleteResepsionis(resepsionisId);
    }
}
