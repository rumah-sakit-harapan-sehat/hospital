package com.harapansehat.hospital.Testing.controller;

import com.harapansehat.hospital.controller.MenuController;
import com.harapansehat.hospital.entity.Menu;
import com.harapansehat.hospital.service.MenuService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.*;
import static org.mockito.Mockito.times;

class MenuControllerTests {

    @InjectMocks
    MenuController menuController;

    @Mock
    MenuService menuService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void testGetMenu() {
        MenuService menuService = mock(MenuService.class);
        MenuController menuController = new MenuController(menuService);

        List<Menu> menuList = Arrays.asList(new Menu());

        when(menuService.getAllMenu()).thenReturn(menuList);
        ResponseEntity<List<Menu>> responseEntity = menuController.getAllMenu();

        Assertions.assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        Assertions.assertEquals(menuList, responseEntity.getBody());

        verify(menuService, times(1)).getAllMenu();
    }
}
