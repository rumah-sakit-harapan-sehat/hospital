package com.harapansehat.hospital.Testing.controller;

import com.harapansehat.hospital.controller.LayananController;
import com.harapansehat.hospital.dto.BaseResponse;
import com.harapansehat.hospital.dto.layanan.LayananRequest;
import com.harapansehat.hospital.dto.layanan.LayananResponse;
import com.harapansehat.hospital.entity.Layanan;
import com.harapansehat.hospital.service.LayananService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.*;
import static org.mockito.Mockito.times;

class LayananControllerTests {

    @InjectMocks
    LayananController layananController;

    @Mock
    LayananService layananService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
     void testGetAllLayanan(){
        LayananService layananService = mock(LayananService.class);
        LayananController layananController = new LayananController(layananService);

        List<LayananResponse> mockResponses = Arrays.asList(
                new LayananResponse(1,"Rawa Inap", false),
                new LayananResponse(2,"Oparasi", false)
        );

        when(layananService.getAllLayanan()).thenReturn(mockResponses);
        ResponseEntity<BaseResponse<List<LayananResponse>>> responseEntity = layananController.getAllLayanan();

        Assertions.assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        Assertions.assertTrue(responseEntity.getBody().getSuccess());
        Assertions.assertEquals("Operation success", responseEntity.getBody().getMessage());
        Assertions.assertEquals(mockResponses, responseEntity.getBody().getData());

        verify(layananService, times(1)).getAllLayanan();
    }

    @Test
    void testGetLayananById() {
        LayananService layananService = mock(LayananService.class);
        LayananController layananController = new LayananController(layananService);

        Integer layananId = 1;
        Layanan mockLayanan = new Layanan();

        when(layananService.getLayananById(layananId)).thenReturn(mockLayanan);
        ResponseEntity<BaseResponse<Layanan>> responseEntity = layananController.getLayananById(layananId);

        Assertions.assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        Assertions.assertTrue(responseEntity.getBody().getSuccess());
        Assertions.assertEquals("Operation success", responseEntity.getBody().getMessage());
        Assertions.assertEquals(mockLayanan, responseEntity.getBody().getData());

        verify(layananService, times(1)).getLayananById(layananId);
    }

    @Test
     void testSaveLayanan(){
        LayananRequest request = new LayananRequest();
        LayananResponse mockResponse = new LayananResponse(1, "Rawat Inap", false);

        when(layananService.addLayanan(request)).thenReturn(mockResponse);
        ResponseEntity<BaseResponse<LayananResponse>> responseEntity = layananController.addLayanan(request);

        Assertions.assertEquals(HttpStatus.CREATED, responseEntity.getStatusCode());
        Assertions.assertTrue(responseEntity.getBody().getSuccess());
        Assertions.assertEquals("Operation success", responseEntity.getBody().getMessage());
        Assertions.assertEquals(mockResponse, responseEntity.getBody().getData());
    }

    @Test
     void testUpdateLayanan() throws Exception {
        LayananService layananService = mock(LayananService.class);
        LayananController layananController = new LayananController(layananService);

        Integer layananId = 1;
        LayananRequest request = new LayananRequest();
        LayananResponse mockResponse = new LayananResponse(layananId, "Rawat Inap", false);

        when(layananService.updateLayanan(layananId, request)).thenReturn(mockResponse);
        ResponseEntity<BaseResponse<LayananResponse>> responseEntity = layananController.updateLayanan(layananId, request);

        Assertions.assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        Assertions.assertTrue(responseEntity.getBody().getSuccess());
        Assertions.assertEquals("Operation success", responseEntity.getBody().getMessage());
        Assertions.assertEquals(mockResponse, responseEntity.getBody().getData());

        verify(layananService, times(1)).updateLayanan(layananId, request);
    }

    @Test
     void testDeleteLayanan() throws Exception {
        LayananService layananService = mock(LayananService.class);
        LayananController layananController = new LayananController(layananService);

        Integer layananId = 1;
        LayananResponse mockResponse = new LayananResponse(layananId, "VIP", false);

        when(layananService.deleteLayanan(layananId)).thenReturn(mockResponse);
        ResponseEntity<BaseResponse<LayananResponse>> responseEntity = layananController.deleteLayanan(layananId);

        Assertions.assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        Assertions.assertTrue(responseEntity.getBody().getSuccess());
        Assertions.assertEquals("Operation success", responseEntity.getBody().getMessage());
        Assertions.assertEquals(mockResponse, responseEntity.getBody().getData());

        verify(layananService, times(1)).deleteLayanan(layananId);
    }
}
