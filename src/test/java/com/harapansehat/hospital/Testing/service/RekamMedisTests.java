package com.harapansehat.hospital.Testing.service;

import com.harapansehat.hospital.dto.rekamMedis.RekamMedisRequest;
import com.harapansehat.hospital.dto.rekamMedis.RekamMedisResponse;
import com.harapansehat.hospital.entity.Pasien;
import com.harapansehat.hospital.entity.RekamMedis;
import com.harapansehat.hospital.entity.Transaksi;
import com.harapansehat.hospital.entity.Treatment;
import com.harapansehat.hospital.repository.PasienRepository;
import com.harapansehat.hospital.repository.RekamMedisRepository;
import com.harapansehat.hospital.repository.TreatmentRepository;
import com.harapansehat.hospital.service.RekamMedisService;
import com.harapansehat.hospital.service.TreatmentService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.web.servlet.tags.form.PasswordInputTag;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

class RekamMedisTests {

    @InjectMocks
    RekamMedisService rekamMedisService;

    @Mock
    RekamMedisRepository rekamMedisRepository;

    @Mock
    TreatmentRepository treatmentRepository;

    @Mock
    PasienRepository pasienRepository;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void testTreatmentExistValidation() {
        RekamMedis rekamMedis = new RekamMedis();
        rekamMedis.setId(1);

        when(rekamMedisRepository.findById(Mockito.anyInt())).thenReturn(Optional.of(rekamMedis));

        assertEquals(rekamMedis, rekamMedisService.rekamMedisExistValidation(1));
    }

    @Test
    void testGetRekamMedis(){
        List<RekamMedis> rekamMedisList = new ArrayList<>();
        Treatment treatment = new Treatment();
        treatment.setId(1);

        Pasien pasien = new Pasien();
        pasien.setId(1);
        pasien.setNamaPasien("giri");

        RekamMedis rekamMedis = new RekamMedis();
        rekamMedis.setId(1);
        rekamMedis.setPasien(pasien);
        rekamMedis.setTinggiBadan(175.0);
        rekamMedis.setBeratBadan(60.0);
        rekamMedis.setTensiDarah("tes darah");
        rekamMedis.setGejala("tes gejala");
        rekamMedis.setAlergi("susu");
        rekamMedis.setTreatment(treatment);
        rekamMedis.setIsDeleted(false);
        rekamMedisList.add(rekamMedis);

        when(pasienRepository.findById(pasien.getId())).thenReturn(Optional.of(pasien));
        when(treatmentRepository.findById(treatment.getId())).thenReturn(Optional.of(treatment));

        when(rekamMedisRepository.findAll()).thenReturn(rekamMedisList);
        List<RekamMedisResponse> responseList = rekamMedisService.getAll();

        assertFalse(responseList.isEmpty());

        RekamMedisResponse response = responseList.get(0);
        assertNotNull(response);
        assertEquals("giri", response.getNamaPasien());
        assertEquals(175.0, response.getTinggiBadan());
        assertEquals(60.0, response.getBeratBadan());
        assertEquals("tes darah", response.getTensiDarah());
        assertEquals("tes gejala", response.getGejala());
        assertEquals("susu", response.getAlergi());
    }

//    @Test
//    void testGetRekamMedisByPasienId() {
//        Integer pasienId = 1;
//        List<RekamMedis> rekamMedisList = new ArrayList<>();
//        Treatment treatment = new Treatment();
//        treatment.setId(1);
//
//        Pasien pasien = new Pasien();
//        pasien.setId(1);
//        pasien.setNamaPasien("giri");
//
//        RekamMedis rekamMedis = new RekamMedis();
//        rekamMedis.setId(1);
//        rekamMedis.setPasien(pasien);
//        rekamMedis.setTinggiBadan(175.0);
//        rekamMedis.setBeratBadan(60.0);
//        rekamMedis.setTensiDarah("tes darah");
//        rekamMedis.setGejala("tes gejala");
//        rekamMedis.setAlergi("susu");
//        rekamMedis.setTreatment(treatment);
//        rekamMedis.setIsDeleted(false);
//        rekamMedisList.add(rekamMedis);
//
//        when(pasienRepository.findById(pasien.getId())).thenReturn(Optional.of(pasien));
//        when(treatmentRepository.findById(treatment.getId())).thenReturn(Optional.of(treatment));
//
//        when(rekamMedisRepository.findById(pasienId)).thenReturn(Optional.of(rekamMedis));
//
//        List<RekamMedisResponse> responseList = rekamMedisService.getAllByPasienId(pasienId);
//
//        RekamMedisResponse response = responseList.get(0);
//        assertNotNull(response);
//        assertEquals("giri", responseList);
//        assertEquals(175.0, responseList);
//        assertEquals(60.0, responseList);
//        assertEquals("tes darah", responseList);
//        assertEquals("tes gejala", responseList);
//        assertEquals("susu", responseList);
//    }

//    @Test
//    void testSaveRekamMedis() {
//        Pasien pasien = new Pasien();
//        pasien.setId(1);
//        pasien.setNamaPasien("giri");
//
//        Treatment treatment = new Treatment();
//        treatment.setId(1);
//
//        RekamMedisRequest request = new RekamMedisRequest();
//        request.setPasienId(1);
//        request.setTinggiBadan(175.0);
//        request.setBeratBadan(60.0);
//        request.setTensiDarah("60/60");
//        request.setGejala("batuk");
//        request.setAlergi("dia");
//        request.setTreatmentId(1);
//
//        when(pasienRepository.findById(pasien.getId())).thenReturn(Optional.of(pasien));
//        when(treatmentRepository.findById(treatment.getId())).thenReturn(Optional.of(treatment));
//        when(rekamMedisRepository.save(any(RekamMedis.class))).thenAnswer(invocation -> {
//            RekamMedis rekamMedis = invocation.getArgument(0);
//            rekamMedis.setId(1);
//            return rekamMedis;
//        });
//
//        RekamMedisResponse response = rekamMedisService.addRekamMedis(request);
//
//        assertNotNull(response.getId());
//        assertNotEquals(request.getPasienId(), response.getNamaPasien());
//        assertEquals(request.getTinggiBadan(), response.getTinggiBadan());
//        assertEquals(request.getBeratBadan(), response.getBeratBadan());
//        assertEquals(request.getTensiDarah(), response.getTensiDarah());
//        assertEquals(request.getGejala(), response.getGejala());
//        assertEquals(request.getAlergi(), response.getAlergi());
//        assertEquals(request.getTreatmentId(), response.getTanggalTreatment());
//    }
}
