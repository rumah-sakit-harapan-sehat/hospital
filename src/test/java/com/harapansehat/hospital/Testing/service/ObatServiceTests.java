package com.harapansehat.hospital.Testing.service;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.harapansehat.hospital.dto.obat.ObatResponse;
import com.harapansehat.hospital.entity.Obat;
import com.harapansehat.hospital.repository.ObatRepository;
import com.harapansehat.hospital.service.ObatService;
import  com.harapansehat.hospital.dto.obat.ObatRequest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

class ObatServiceTests {

    @InjectMocks
    private ObatService obatService;


    @Mock
    private ObatRepository obatRepository;


    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void testGetObat() {
        List<Obat> obatList = new ArrayList<>();
        Obat obat = new Obat();
        obat.setId(1);
        obat.setNamaObat("kapsul");
        obat.setJenisObat("kapsul");
        obat.setHarga(BigDecimal.valueOf(5000.00).setScale(2));
        obat.setKeterangan("ket");
        obat.setIsDeleted(false);
        obatList.add(obat);

        when(obatRepository.findAll()).thenReturn(obatList);
        List<ObatResponse> obatResponses = obatService.getAllObat();

        assertFalse(obatResponses.isEmpty());

        ObatResponse response = obatResponses.get(0);
        assertNotNull(response);
        assertEquals("kapsul", response.getNamaObat());
        assertEquals("kapsul", response.getJenisObat());
        assertEquals(BigDecimal.valueOf(5000.00).setScale(2), response.getHarga());
        assertEquals("ket", response.getKeterangan());
        assertEquals(false, response.getIsDeleted());

        verify(obatRepository, times(1)).findAll();
    }

    @Test
    void testObatById() {
        Integer obatId = null;
        Obat obat = new Obat();
        obat.setNamaObat("kapsul");
        obat.setJenisObat("kapsul");
        obat.setHarga(BigDecimal.valueOf(5000.00).setScale(2));
        obat.setKeterangan("ket");
        obat.setIsDeleted(false);

        when(obatRepository.findById(obatId)).thenReturn(Optional.of(obat));

        Obat mockResponse = obatService.getObatById(obatId);

        assertNotNull(mockResponse);
        assertEquals(obatId, mockResponse.getId());
        assertEquals("kapsul", mockResponse.getNamaObat());
        assertEquals("kapsul", mockResponse.getJenisObat());
        assertEquals(BigDecimal.valueOf(5000.00).setScale(2), mockResponse.getHarga());
        assertEquals("ket", mockResponse.getKeterangan());
        assertEquals(false, mockResponse.getIsDeleted());
    }

    @Test
    void testSaveObat() {
        ObatRequest request = new ObatRequest();

        when(obatRepository.save(any(Obat.class))).thenAnswer(invocation -> {
            Obat saveObat = invocation.getArgument(0);
            saveObat.setId(1);
            return saveObat;
        });

        ObatResponse response = obatService.addObat(request);

        assertNotNull(response.getId());
        assertEquals(request.getNamaObat(), response.getNamaObat());
        assertEquals(request.getJenisObat(), response.getJenisObat());
        assertEquals(request.getHarga(), response.getHarga());
        assertEquals(request.getKeterangan(), response.getKeterangan());
    }

    @Test
    void testUpateObat() {
        ObatRequest request = new ObatRequest();
        Integer obatId = 1;

        Obat obat = new Obat();
        obat.setId(obatId);
        obat.setNamaObat("tess1");
        obat.setJenisObat("kapsul");
        obat.setHarga(BigDecimal.valueOf(4000.00).setScale(2));
        obat.setKeterangan("ket tess1");
        obat.setIsDeleted(false);

        when(obatRepository.findById(obatId)).thenReturn(Optional.of(obat));
        when(obatRepository.save(any(Obat.class))).thenAnswer(invocation -> {
            Obat saveObat = invocation.getArgument(0);
            saveObat.setId(1);
            return saveObat;
        });

        ObatResponse mockResponse = obatService.updateObat(obatId, request);

        assertNotNull(mockResponse);
        assertEquals(obatId, mockResponse.getId());
        assertEquals(request.getNamaObat(), mockResponse.getNamaObat());
        assertEquals(request.getJenisObat(), mockResponse.getJenisObat());
        assertEquals(request.getHarga(), mockResponse.getHarga());
        assertEquals(request.getKeterangan(), mockResponse.getKeterangan());
        assertEquals(false, mockResponse.getIsDeleted());
    }

    @Test
    void testObatDelete() {
        Integer obatId = 1;
        Obat obat = new Obat();
        obat.setId(obatId);
        obat.setNamaObat("bodrek");
        obat.setJenisObat("kapsul");
        obat.setHarga(BigDecimal.valueOf(1000.00).setScale(2));
        obat.setKeterangan("INI OBAT BODREK KAPSUL");
        obat.setIsDeleted(false);

        when(obatRepository.findById(obatId)).thenReturn(Optional.of(obat));
        ObatResponse response = obatService.deleteObat(obatId);

        assertEquals(1, response.getId());
    }
}
