package com.harapansehat.hospital.Testing.service;

import com.harapansehat.hospital.dto.kamar.KamarRequest;
import com.harapansehat.hospital.dto.kamar.KamarResponse;
import com.harapansehat.hospital.entity.Kamar;
import com.harapansehat.hospital.exception.ResourceNotFoundException;
import com.harapansehat.hospital.repository.KamarRepository;
import com.harapansehat.hospital.repository.TreatmentRepository;
import com.harapansehat.hospital.service.KamarService;
import com.harapansehat.hospital.util.ExceptionMessageAccessor;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;


class KamarServiceTests {
    @Mock
    private KamarRepository kamarRepository;

    @Mock
    private TreatmentRepository treatmentRepository;

    @InjectMocks
    private KamarService kamarService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void testKamarExistValidation() {
        Integer kamarId = 1;
        Kamar kamar = new Kamar();
        kamar.setId(1);
        kamar.setJenisKamar("VIP");
        kamar.setHargaKamar(BigDecimal.valueOf(100000.00).setScale(2));
        kamar.setDeskripsi("VIP BANGET ADA PANTAINYA");
        kamar.setIsDeleted(false);

        when(kamarRepository.findById(kamarId)).thenReturn(Optional.of(kamar));
        Kamar kamarExist = kamarService.kamarExistValidation(kamarId);

        assertNotNull(kamarExist);
        assertEquals(1, kamarExist.getId());
        assertEquals("VIP", kamarExist.getJenisKamar());
        assertEquals(BigDecimal.valueOf(100000.00).setScale(2), kamarExist.getHargaKamar());
        assertEquals("VIP BANGET ADA PANTAINYA", kamarExist.getDeskripsi());
        assertEquals(false, kamarExist.getIsDeleted());
    }

    @Test
    void testGetAllKamar() {
        List<Kamar> kamarList = new ArrayList<>();
        Kamar kamar = new Kamar();
        kamar.setId(1);
        kamar.setJenisKamar("VIP");
        kamar.setHargaKamar(BigDecimal.valueOf(100000.00).setScale(2));
        kamar.setDeskripsi("VIP BANGET ADA PANTAINYA");
        kamar.setIsDeleted(false);
        kamarList.add(kamar);

        when(kamarRepository.findAll()).thenReturn(kamarList);
        List<KamarResponse> responseList = kamarService.getAllKamar();

        assertFalse(responseList.isEmpty());

        KamarResponse response = responseList.get(0);
        assertNotNull(response);
        assertEquals("VIP", response.getJenisKamar());
        assertEquals(BigDecimal.valueOf(100000.00).setScale(2), response.getHargaKamar());
        assertEquals("VIP BANGET ADA PANTAINYA", response.getDeskripsi());
        assertEquals(false, response.getIsDeleted());

        verify(kamarRepository, times(1)).findAll();
    }

    @Test
    void testKamarById() {
        Integer kamarId = 1;
        Kamar kamar = new Kamar();
        kamar.setId(1);
        kamar.setJenisKamar("VIP");
        kamar.setHargaKamar(BigDecimal.valueOf(100000.00).setScale(2));
        kamar.setDeskripsi("VIP BANGET ADA PANTAINYA");
        kamar.setIsDeleted(false);

        when(kamarRepository.findById(kamarId)).thenReturn(Optional.of(kamar));

        Kamar mockResponse = kamarService.getKamarById(kamarId);
        assertNotNull(mockResponse);
        assertEquals("VIP", mockResponse.getJenisKamar());
        assertEquals(BigDecimal.valueOf(100000.00).setScale(2), mockResponse.getHargaKamar());
        assertEquals("VIP BANGET ADA PANTAINYA", mockResponse.getDeskripsi());
        assertEquals(false, mockResponse.getIsDeleted());

    }

    @Test
    void addKamar() {
        KamarRequest request = new KamarRequest();

        when(kamarRepository.save(any(Kamar.class))).thenAnswer(invocation -> {
            Kamar saveKamar = invocation.getArgument(0);
            saveKamar.setId(1);
            return saveKamar;
        });

        KamarResponse response = kamarService.addKamar(request);

        assertNotNull(response.getId());
        assertEquals(request.getJenisKamar(), response.getJenisKamar());
        assertEquals(request.getHargaKamar(), response.getHargaKamar());
        assertEquals(request.getDeskripsi(), response.getDeskripsi());
    }

    @Test
    void updateKamar() {
        KamarRequest request = new KamarRequest();
        Integer kamarId = 1;

        Kamar kamar = new Kamar();
        kamar.setId(kamarId);
        kamar.setJenisKamar("Sample Jenis Kamar");
        kamar.setHargaKamar(BigDecimal.valueOf(100.00));
        kamar.setDeskripsi("Sample Deskripsi");
        kamar.setIsDeleted(false);

        when(kamarRepository.findById(kamarId)).thenReturn(Optional.of(kamar));
        when(kamarRepository.save(any(Kamar.class))).thenAnswer(incovation -> {
            Kamar saveKamar = incovation.getArgument(0);
            saveKamar.setId(1);
            return saveKamar;
        });

        KamarResponse mockResponse = kamarService.updateKamar(kamarId, request);

        assertNotNull(mockResponse);
        assertEquals(kamarId, mockResponse.getId());
        assertEquals(request.getJenisKamar(), mockResponse.getJenisKamar());
        assertEquals(request.getHargaKamar(), mockResponse.getHargaKamar());
        assertEquals(request.getDeskripsi(), mockResponse.getDeskripsi());
        assertEquals(false, mockResponse.getIsDeleted());
    }

    @Test
    void testKamarDelete() {
        Integer kamarId = 1;
        Kamar kamar = new Kamar();
        kamar.setId(kamarId);
        kamar.setJenisKamar("VIP");
        kamar.setHargaKamar(BigDecimal.valueOf(50000.00).setScale(2));
        kamar.setDeskripsi("INI KAMAR VIP");
        kamar.setIsDeleted(false);

        when(kamarRepository.findById(kamarId)).thenReturn(Optional.of(kamar));
        KamarResponse response = kamarService.deleteKamar(kamarId);

        assertEquals(1, response.getId());
    }
}
