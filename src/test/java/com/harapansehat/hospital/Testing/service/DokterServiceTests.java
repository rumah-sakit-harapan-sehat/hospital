package com.harapansehat.hospital.Testing.service;

import com.harapansehat.hospital.dto.dokter.DokterRequest;
import com.harapansehat.hospital.dto.dokter.DokterResponse;
import com.harapansehat.hospital.entity.Dokter;
import com.harapansehat.hospital.entity.RekamMedis;
import com.harapansehat.hospital.entity.Spesialisasi;
import com.harapansehat.hospital.repository.DokterRepository;
import com.harapansehat.hospital.repository.SpesialisasiRepository;
import com.harapansehat.hospital.security.dto.RegistrationResponse;
import com.harapansehat.hospital.service.DokterService;
import com.harapansehat.hospital.service.UserService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class DokterServiceTests {

    @InjectMocks
    UserService userService;

    @Mock
    DokterRepository dokterRepository;

    @Mock
    DokterService dokterService;

    @Mock
    SpesialisasiRepository spesialisasiRepository;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void testTreatmentExistValidation() {
        Dokter dokter = new Dokter();
        dokter.setId(1);

        when(dokterRepository.findById(Mockito.anyInt())).thenReturn(Optional.of(dokter));

//        assertEquals(dokter, dokterService.dokterExistValidation(1));
    }

//    @Test
//    void testGetDokter() {
//        List<Dokter> dokterList = new ArrayList<>();
//
//        Spesialisasi spesialisasi = new Spesialisasi();
//        spesialisasi.setId(1);
//        spesialisasi.setNamaSpesialis("THT");
//
//        Dokter dokter = new Dokter();
//        dokter.setId(1);
//        dokter.setNip("12345");
//        dokter.setNamaDokter("giri");
//        dokter.setJenisKelamin("laki-laki");
//        dokter.setAlamat("bogor");
//        dokter.setTanggalLahir("2004-07-04");
//        dokter.setSpesialisasi(spesialisasi);
//        dokter.setIsDeleted(true);
//        dokterList.add(dokter);
//
//        when(spesialisasiRepository.findById(spesialisasi.getId())).thenReturn(Optional.of(spesialisasi));
//        when(dokterRepository.findAll()).thenReturn(dokterList);
//
//        List<DokterResponse> dokterResponses = dokterService.getAllDokter();
//
////        assertFalse(dokterResponses.isEmpty());
//
//        DokterResponse response = dokterResponses.get(0);
//        assertNotNull(response);
//        assertEquals("12345", response.getNip());
//        assertEquals("giri", response.getNamaDokter());
//        assertEquals("laki-laki", response.getJenisKelamin());
//        assertEquals("bogor", response.getAlamat());
//        assertEquals("2004-07-04", response.getTanggalLahir());
//        assertEquals("THT", response.getNamaSpesialis());
//        assertEquals(true, response.getIsDeleted());
//
//        verify(dokterRepository, times(1)).findAll();
//    }

//    @Test
//    void testSaveDokter() {
//        DokterRequest request = new DokterRequest();
//        request.setNip("12345");
//        request.setNamaDokter("giri");
//        request.setPassword("12345");
//        request.setJenisKelamin("laki-laki");
//        request.setAlamat("bogor");
//        request.setTanggalLahir("2004-07-04");
//        request.setSpesialisId(1);
//
//        Spesialisasi spesialisasi = new Spesialisasi();
//        spesialisasi.setId(1);
//        spesialisasi.setNamaSpesialis("THT");
//        spesialisasi.setHarga(BigDecimal.valueOf(10000.00).setScale(2));
//        spesialisasi.setUrlImage("tht.png");
//
//        when(spesialisasiRepository.findById(request.getSpesialisId())).thenReturn(Optional.of(spesialisasi));
//
////        when(dokterRepository.save(any(Dokter.class))).thenAnswer(invocation -> {
////            Dokter saveDokter = invocation.getArgument(0);
////            saveDokter.setId(1);
////            return saveDokter;
////        });
//
//        RegistrationResponse response = userService.registrationDokter(request);
//        Assertions.assertNotNull(response.getMessage());
//        Assertions.assertEquals(request.getNip(), response.getMessage());
//        Assertions.assertEquals(request.getNamaDokter(), response.getMessage());
//        Assertions.assertEquals(request.getSpesialisId(), response.getMessage());
//        Assertions.assertEquals(request.getPassword(), response.getMessage());
//        Assertions.assertEquals(request.getAlamat(), response.getMessage());
//        Assertions.assertEquals(request.getTanggalLahir(), response.getMessage());
//        Assertions.assertEquals(request.getJenisKelamin(), response.getMessage());
//    }
}
