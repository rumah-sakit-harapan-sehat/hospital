package com.harapansehat.hospital.Testing.service;

import com.google.zxing.WriterException;
import com.harapansehat.hospital.dto.transaksi.TransaksiRequest;
import com.harapansehat.hospital.dto.transaksi.TransaksiResponse;
import com.harapansehat.hospital.entity.Transaksi;
import com.harapansehat.hospital.exception.ResourceNotFoundException;
import com.harapansehat.hospital.repository.TransaksiRepository;
import com.harapansehat.hospital.service.TransaksiService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

class TransaksiServiceTests {

    @InjectMocks
    TransaksiService transaksiService;

    @Mock
    TransaksiRepository transaksiRepository;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void testTransaksiExistValidation() {
        Transaksi transaksi = new Transaksi();
        transaksi.setId(1);

        when(transaksiRepository.findById(Mockito.anyInt())).thenReturn(Optional.of(transaksi));

        assertEquals(transaksi, transaksiService.transaksiExistValidation(1));
    }

    @Test
    void testSaveTransaksi() {
        // Mock data
        Transaksi transaksi1 = new Transaksi();
        List<Transaksi> transaksiList = List.of(transaksi1);

        // Mock TransaksiRepository
        when(transaksiRepository.findAll()).thenReturn(transaksiList);

        // Call the method
        List<TransaksiResponse> actualList = transaksiService.getAllTransaksi();

        // Assertions
        // Verify that transaksiRepository.findAll was called
        verify(transaksiRepository).findAll();

        // Use assertions to verify that the actual list matches the expected list
        // (You may need to override equals() method in TransaksiResponse for meaningful comparisons)
//        assertEquals(, actualList);
    }
}
