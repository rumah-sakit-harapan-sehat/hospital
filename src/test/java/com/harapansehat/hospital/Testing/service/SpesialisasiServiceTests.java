package com.harapansehat.hospital.Testing.service;

import com.harapansehat.hospital.dto.spesialisasi.SpesialisRequest;
import com.harapansehat.hospital.dto.spesialisasi.SpesialisResponse;
import com.harapansehat.hospital.entity.Spesialisasi;
import com.harapansehat.hospital.repository.SpesialisasiRepository;
import com.harapansehat.hospital.service.SpesialisasiService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

class SpesialisasiServiceTests {

    @InjectMocks
    SpesialisasiService spesialisasiService;

    @Mock
    SpesialisasiRepository spesialisasiRepository;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void testGetSpesialis() {
        List<Spesialisasi> spesialisasiList = new ArrayList<>();
        Spesialisasi spesialisasi = new Spesialisasi();
        spesialisasi.setId(1);
        spesialisasi.setNamaSpesialis("THT");
        spesialisasi.setHarga(BigDecimal.valueOf(100000.00).setScale(2));
        spesialisasi.setUrlImage("spesialis.png");
        spesialisasi.setIsDeleted(false);
        spesialisasiList.add(spesialisasi);

        when(spesialisasiRepository.findAll()).thenReturn(spesialisasiList);
        List<SpesialisResponse> spesialisResponses = spesialisasiService.getAll();

        assertFalse(spesialisResponses.isEmpty());

        SpesialisResponse response = spesialisResponses.get(0);
        assertNotNull(response);
        assertEquals("THT", response.getNamaSpesialis());
        assertEquals(BigDecimal.valueOf(100000.00).setScale(2), response.getHargaSpesialis());
        assertEquals("spesialis.png", response.getUrlImage());
        assertEquals(false, response.getIsDeleted());


        verify(spesialisasiRepository, times(1)).findAll();
    }

    @Test
    void testGetSpesialisById() {
        Integer spesialisId = 1;
        Spesialisasi spesialisasi = new Spesialisasi();
        spesialisasi.setId(1);
        spesialisasi.setNamaSpesialis("THT");
        spesialisasi.setHarga(BigDecimal.valueOf(100000.00).setScale(2));
        spesialisasi.setUrlImage("spesialis.png");
        spesialisasi.setIsDeleted(false);

        when(spesialisasiRepository.findById(spesialisId)).thenReturn(Optional.of(spesialisasi));

        Spesialisasi mockResponse = spesialisasiService.getSpesialisasiById(spesialisId);
        assertNotNull(mockResponse);
        assertEquals("THT", mockResponse.getNamaSpesialis());
        assertEquals(BigDecimal.valueOf(100000.00).setScale(2), mockResponse.getHarga());
        assertEquals("spesialis.png", mockResponse.getUrlImage());
        assertEquals(false, mockResponse.getIsDeleted());
    }

    @Test
    void testSaveSpesialisasi() {
        SpesialisRequest request = new SpesialisRequest();

        when(spesialisasiRepository.save(any(Spesialisasi.class))).thenAnswer(invocation -> {
            Spesialisasi saveSpesialis = invocation.getArgument(0);
            saveSpesialis.setId(1);
            return saveSpesialis;
        });

        SpesialisResponse response = spesialisasiService.addSpesialis(request);
        assertNotNull(response.getId());
        assertEquals(request.getNamaSpesialis(), response.getNamaSpesialis());
        assertEquals(request.getHargaSpesialis(), response.getHargaSpesialis());
    }

    @Test
    void testUpdateSpesialis() {
        SpesialisRequest request = new SpesialisRequest();
        Integer spesialisId = 1;

        Spesialisasi spesialisasi = new Spesialisasi();
        spesialisasi.setId(spesialisId);
        spesialisasi.setNamaSpesialis("THT");
        spesialisasi.setHarga(BigDecimal.valueOf(100000.00).setScale(2));
        spesialisasi.setUrlImage("spesialis.png");
        spesialisasi.setIsDeleted(false);

        when(spesialisasiRepository.findById(spesialisId)).thenReturn(Optional.of(spesialisasi));
        when(spesialisasiRepository.save(any(Spesialisasi.class))).thenAnswer(invocation -> {
            Spesialisasi saveSpesialis = invocation.getArgument(0);
            saveSpesialis.setId(1);
            return saveSpesialis;
        });

        SpesialisResponse mockResponse = spesialisasiService.updateSpesialis(spesialisId, request);
        assertNotNull(mockResponse);
        assertEquals(spesialisId, mockResponse.getId());
        assertEquals(request.getNamaSpesialis(), mockResponse.getNamaSpesialis());
        assertEquals(request.getHargaSpesialis(), mockResponse.getHargaSpesialis());
        assertEquals(request.getUrlImage(), mockResponse.getUrlImage());
        assertEquals(false, mockResponse.getIsDeleted());
    }

    @Test
    void testDeleteSpesialis() {
        Integer spesialisId = 1;
        Spesialisasi spesialisasi = new Spesialisasi();
        spesialisasi.setId(spesialisId);
        spesialisasi.setNamaSpesialis("THT");
        spesialisasi.setHarga(BigDecimal.valueOf(100000.00).setScale(2));
        spesialisasi.setUrlImage("spesialis.png");

        when(spesialisasiRepository.findById(spesialisId)).thenReturn(Optional.of(spesialisasi));
        SpesialisResponse response = spesialisasiService.deleteSpesialis(spesialisId);

        assertEquals(1, response.getId());
    }
}
