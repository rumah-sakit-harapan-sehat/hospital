package com.harapansehat.hospital.Testing.service;

import com.harapansehat.hospital.dto.treatment.TreatmentResponse;
import com.harapansehat.hospital.dto.visit.VisitRequest;
import com.harapansehat.hospital.dto.visit.VisitResponse;
import com.harapansehat.hospital.entity.*;
import com.harapansehat.hospital.repository.*;
import com.harapansehat.hospital.service.TreatmentService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

class TreatmentServiceTests {

    @InjectMocks
    TreatmentService treatmentService;

    @Mock
    TreatmentRepository treatmentRepository;

    @Mock
    PasienRepository pasienRepository;

    @Mock
    LayananRepository layananRepository;

    @Mock
    SpesialisasiRepository spesialisasiRepository;

    @Mock
    DokterRepository dokterRepository;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void testTransaksiExistValidation() {
        Treatment treatment = new Treatment();
        treatment.setId(1);

        when(treatmentRepository.findById(Mockito.anyInt())).thenReturn(Optional.of(treatment));

        assertEquals(treatment, treatmentService.treatmentExistValidation(1));
    }

    @Test
    void testGetTreatment() {
        List<Treatment> treatmentList = new ArrayList<>();
        Spesialisasi spesialisasi = new Spesialisasi();
        spesialisasi.setId(1);
        spesialisasi.setNamaSpesialis("tht");

        Dokter dokter = new Dokter();
        dokter.setId(1);
        dokter.setNamaDokter("giri");
        dokter.setSpesialisasi(spesialisasi);

        Pasien pasien = new Pasien();
        pasien.setId(1);
        pasien.setNamaPasien("hamzah");

        Layanan layanan = new Layanan();
        layanan.setId(1);
        layanan.setNamaLayanan("rawat inap");

        Treatment treatment = new Treatment();
        treatment.setId(1);
        treatment.setDokter(dokter);
        treatment.setPasien(pasien);
        treatment.setLayanan(layanan);
        treatment.setSpesialisasi(dokter.getSpesialisasi());
        treatment.setStatus("incoming");
        treatment.setDiagnosis("pusing");
        treatment.setTanggalKonsul(new Date());
        treatment.setPesan("gws");
        treatment.setIsDeleted(false);
        treatmentList.add(treatment);

        when(layananRepository.findById(layanan.getId())).thenReturn(Optional.of(layanan));
        when(pasienRepository.findById(pasien.getId())).thenReturn(Optional.of(pasien));
        when(dokterRepository.findById(dokter.getId())).thenReturn(Optional.of(dokter));
        when(spesialisasiRepository.findById(spesialisasi.getId())).thenReturn(Optional.of(spesialisasi));

        when(treatmentRepository.findAll()).thenReturn(treatmentList);
        List<TreatmentResponse> responseList = treatmentService.getAllTreatment();

        assertFalse(responseList.isEmpty());

        TreatmentResponse response = responseList.get(0);
        assertNotNull(response);
        assertEquals(1, response.getDokterId());
        assertEquals("giri", response.getNamaDokter());
        assertEquals(1, response.getPasienId());
        assertEquals("hamzah", response.getNamaPasien());
//        assertEquals(1, response.getLayananId());
        assertEquals("tht", spesialisasi.getNamaSpesialis());
        assertEquals("incoming", response.getStatus());
//        assertEquals("pusing", response.getDiagnosis());
//        assertEquals(new Date(), response.getTanggalKonsul());
//        assertEquals("gws", response.getPesan());
    }

//    @Test
//    void testTreatmentByDokterId() {
//
//    }

//    @Test
//    void testSaveTreatment() {
//        VisitRequest request = new VisitRequest();
//
//        when(treatmentRepository.save(any(Treatment.class))).thenAnswer(invocation -> {
//            Treatment saveTreatment = invocation.getArgument(0);
//            saveTreatment.setId(1);
//            return saveTreatment;
//        });
//
//        VisitResponse response = treatmentService.addTreatment(request);
//        Assertions.assertNotNull(response.getVisitId());
//        Assertions.assertEquals(request.getDokterId(), response.getDokterId());
//        Assertions.assertEquals(request.getPasienId(), response.getPasienId());
//        Assertions.assertEquals(request.getTanggalKonsul(), response.getTanggalKonsul());
//    }
}
