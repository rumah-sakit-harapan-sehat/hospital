package com.harapansehat.hospital.Testing.service;

import com.harapansehat.hospital.dto.resepsionis.ResepsionisRequest;
import com.harapansehat.hospital.dto.resepsionis.ResepsionisResponse;
import com.harapansehat.hospital.entity.Resepsionis;
import com.harapansehat.hospital.entity.User;
import com.harapansehat.hospital.repository.ResepsionisRepository;
import com.harapansehat.hospital.repository.UserRepository;
import com.harapansehat.hospital.service.ResepsionisService;
import com.harapansehat.hospital.service.UserService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class ResepsionisServiceTests {

    @InjectMocks
    ResepsionisService resepsionisService;

    @Mock
    ResepsionisRepository resepsionisRepository;

    @Mock
    UserRepository userRepository;

    @Mock
    UserService userService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void testGetResepsionis() {
        List<Resepsionis> resepsionisList = new ArrayList<>();
        Resepsionis resepsionis = new Resepsionis();
        resepsionis.setId(1);
        resepsionis.setNamaResepsionis("giri");
        resepsionis.setJenisKelamin("laki-laki");
        resepsionis.setAlamat("bogor");
        resepsionis.setIsDeleted(false);
        resepsionisList.add(resepsionis);

        when(resepsionisRepository.findAll()).thenReturn(resepsionisList);
        List<ResepsionisResponse> resepsionisResponses = resepsionisService.getAll();

        assertFalse(resepsionisResponses.isEmpty());

        ResepsionisResponse response = resepsionisResponses.get(0);
        assertNotNull(response);
        assertEquals("giri", response.getNama());
        assertEquals("laki-laki", response.getJenisKelamin());
        assertEquals("bogor", response.getAlamat());
        assertEquals(false, response.getIsDeleted());

        verify(resepsionisRepository, times(1)).findAll();
    }

    @Test
    void testResepsionisById() {
        Integer resepsionisId = 1;
        Resepsionis resepsionis = new Resepsionis();
        resepsionis.setId(1);
        resepsionis.setNamaResepsionis("giri");
        resepsionis.setJenisKelamin("laki-laki");
        resepsionis.setAlamat("bogor");
        resepsionis.setIsDeleted(false);

        when(resepsionisRepository.findById(resepsionisId)).thenReturn(Optional.of(resepsionis));

        Resepsionis mockResponse = resepsionisService.getResepsionisById(resepsionisId);

        assertNotNull(mockResponse);
        assertEquals(resepsionisId, mockResponse.getId());
        assertEquals("giri", mockResponse.getNamaResepsionis());
        assertEquals("laki-laki", mockResponse.getJenisKelamin());
        assertEquals("bogor", mockResponse.getAlamat());
        assertEquals(false, mockResponse.getIsDeleted());
    }

    @Test
    void testUpdateResepsionis() {
        Integer id = 1;
        ResepsionisRequest request = new ResepsionisRequest();
        request.setUsername("giri");
        request.setPassword("giri123");

        Resepsionis resepsionis = new Resepsionis();
        resepsionis.setId(id);
        resepsionis.setIsDeleted(false);

        when(resepsionisRepository.findById(id)).thenReturn(Optional.of(resepsionis));

        ResepsionisResponse result = resepsionisService.updateResepsionis(id, request);

        assertEquals(id, result.getId());
        assertEquals(resepsionis.getNamaResepsionis(), result.getNama());
        assertEquals(resepsionis.getJenisKelamin(), result.getJenisKelamin());

        verify(resepsionisRepository, times(1)).findById(id);
        verify(resepsionisRepository, times(1)).save(any(Resepsionis.class));
        verify(userService, times(1)).updatePassword(request.getPassword(), request.getUsername());

    }

    @Test
    void testDeleteResep() {

        Integer id = 1;
        Resepsionis resepsionis = new Resepsionis();
        resepsionis.setId(1);
        resepsionis.setIsDeleted(false);

        User user = new User();
        user.setIsDeleted(false);

        when(resepsionisRepository.findById(id)).thenReturn(Optional.of(resepsionis));
        when(userRepository.findByResepsionisId(id)).thenReturn(Optional.of(user));

        ResepsionisResponse response = resepsionisService.deleteResepsionis(id);

        assertEquals(id, response.getId());
        assertEquals(resepsionis.getNamaResepsionis(), response.getNama());
        assertEquals(resepsionis.getJenisKelamin(), response.getJenisKelamin());

        verify(resepsionisRepository, times(1)).findById(id);
        verify(resepsionisRepository, times(1)).save(resepsionis);
        verify(userRepository, times(1)).findByResepsionisId(id);
        verify(userRepository, times(1)).save(user);

    }
}
