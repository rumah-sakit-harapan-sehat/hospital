package com.harapansehat.hospital.Testing.service;

import com.harapansehat.hospital.dto.layanan.LayananRequest;
import com.harapansehat.hospital.dto.layanan.LayananResponse;
import com.harapansehat.hospital.entity.Layanan;
import com.harapansehat.hospital.repository.LayananRepository;
import com.harapansehat.hospital.service.LayananService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

class LayananServiceTests {

    @InjectMocks
    LayananService layananService;

    @Mock
    LayananRepository layananRepository;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void testGetLayanan() {
        List<Layanan> layananList = new ArrayList<>();
        Layanan layanan = new Layanan();
        layanan.setId(1);
        layanan.setNamaLayanan("rawat inap");
        layanan.setIsDeleted(false);
        layananList.add(layanan);

        when(layananRepository.findAll()).thenReturn(layananList);
        List<LayananResponse> layananResponses = layananService.getAllLayanan();

        assertFalse(layananResponses.isEmpty());

        LayananResponse response = layananResponses.get(0);
        assertNotNull(response);
        assertEquals("rawat inap", response.getNamaLayanan());
        assertEquals(false, response.getIsDeleted());

        verify(layananRepository, times(1)).findAll();
    }

    @Test
    void testLayananById() {
        Integer layananId = 1;
        Layanan layanan = new Layanan();
        layanan.setId(1);
        layanan.setNamaLayanan("rawat inap");
        layanan.setIsDeleted(false);

        when(layananRepository.findById(layananId)).thenReturn(Optional.of(layanan));

        Layanan mockResponse = layananService.getLayananById(layananId);
        assertNotNull(mockResponse);
        assertEquals("rawat inap", mockResponse.getNamaLayanan());
        assertEquals(false, mockResponse.getIsDeleted());
    }

    @Test
    void testSaveLayanan() {
        LayananRequest request = new LayananRequest();

        when(layananRepository.save(any(Layanan.class))).thenAnswer(invocatiaon -> {
            Layanan saveLayanan = invocatiaon.getArgument(0);
            saveLayanan.setId(1);
            return saveLayanan;
        });

        LayananResponse response = layananService.addLayanan(request);

        assertNotNull(response.getId());
        assertEquals(request.getNamaLayanan(), response.getNamaLayanan());
    }

//    @Test
//    void checkNamaLayanan() {
//        LayananRequest request = new LayananRequest();
//        String namaLayanan = "Rawat Inap";
//        Layanan layanan = new Layanan();
//        layanan.setId(1);
//        layanan.setNamaLayanan(namaLayanan);
//        layanan.setIsDeleted(false);
//
//        when(layananService.validateNamaLayanan(request)).thenReturn(layanan);
//
//    }

    @Test
    void testUpdateLayanan() {
        LayananRequest request = new LayananRequest();
        Integer layananId = 1;

        Layanan layanan = new Layanan();
        layanan.setId(layananId);
        layanan.setNamaLayanan("rawat inap");
        layanan.setIsDeleted(false);

        when(layananRepository.findById(layananId)).thenReturn(Optional.of(layanan));
        when(layananRepository.save(any(Layanan.class))).thenAnswer(invocation -> {
            Layanan saveLayanan = invocation.getArgument(0);
            saveLayanan.setId(1);
            return saveLayanan;
        });

        LayananResponse mockResponse = layananService.updateLayanan(layananId, request);

        assertNotNull(mockResponse);
        assertEquals(layananId, mockResponse.getId());
        assertEquals(request.getNamaLayanan(), mockResponse.getNamaLayanan());
        assertEquals(false, mockResponse.getIsDeleted());
    }

    @Test
    void testLayananDelete() {
        Integer layananId = 1;
        Layanan layanan = new Layanan();
        layanan.setId(layananId);
        layanan.setNamaLayanan("rawat inap");

        when(layananRepository.findById(layananId)).thenReturn(Optional.of(layanan));
        LayananResponse response = layananService.deleteLayanan(layananId);

        assertEquals(1, response.getId());
    }

    @Test
    void testGetAllLayanans() {
        List<Layanan> layananList = new ArrayList<>();
        Layanan layanan = new Layanan();
        layanan.setId(1);
        layanan.setNamaLayanan("rawat inap");
        layanan.setIsDeleted(false);
        layananList.add(layanan);

        when(layananRepository.findAll()).thenReturn(layananList);
        List<LayananResponse> layananResponses = layananService.getAllLayanan();

        assertFalse(layananResponses.isEmpty());

        LayananResponse response = layananResponses.get(0);
        assertNotNull(response);
        assertEquals("rawat inap", response.getNamaLayanan());
        assertEquals(false, response.getIsDeleted());

        verify(layananRepository, times(1)).findAll();
    }
}
