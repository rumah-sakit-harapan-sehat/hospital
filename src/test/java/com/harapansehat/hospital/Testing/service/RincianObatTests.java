package com.harapansehat.hospital.Testing.service;

import com.harapansehat.hospital.dto.treatment.DataRincianObat;
import com.harapansehat.hospital.entity.Obat;
import com.harapansehat.hospital.entity.RincianObat;
import com.harapansehat.hospital.entity.Treatment;
import com.harapansehat.hospital.repository.RincianObatRepository;
import com.harapansehat.hospital.repository.TreatmentRepository;
import com.harapansehat.hospital.service.RincianObatService;
import com.harapansehat.hospital.service.TreatmentService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

public class RincianObatTests {

    @InjectMocks
    private RincianObatService rincianObatService;

    @Mock
    private RincianObatRepository rincianObatRepository;

    @Mock
    private TreatmentService treatmentService;

    @Mock
    private TreatmentRepository treatmentRepository;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void testGetAllRincianObat() {
//        List<RincianObat> rincianObatList = new ArrayList<>();

//        Obat obat = new Obat();
//        obat.setId(1);
//        obat.setNamaObat("bodrek");
//        obat.setJenisObat("kapsul");
//        obat.setHarga(BigDecimal.valueOf(5000.00).setScale(2));
//        obat.setKeterangan("ket");
//        obat.setIsDeleted(false);
//
//        RincianObat rincianObat = new RincianObat();
//        rincianObat.setId(1);
//        rincianObat.setObat(obat);
//        rincianObat.setKuantitas(1);
//        rincianObatList.add(rincianObat);
//
//        when(rincianObatRepository.findAll()).thenReturn(rincianObatList);
//        List<DataRincianObat> dataRincianObatList = rincianObatService.getAllRincianObatByTreatmentId(treatmentId);
//
//        assertFalse(dataRincianObatList.isEmpty());
//
//        DataRincianObat response = dataRincianObatList.get(0);
//        assertNotNull(response);
//        assertEquals(1, response.getObatId());
//        assertEquals(1, response.getKuantitas());

//        verify(rincianObatRepository, times(1)).findAll();
    }

}
