//package com.harapansehat.hospital.Testing.service;
//
//import com.harapansehat.hospital.dto.dokter.DokterRequest;
//import com.harapansehat.hospital.dto.pasien.PasienRequest;
//import com.harapansehat.hospital.dto.resepsionis.ResepsionisRequest;
//import com.harapansehat.hospital.exception.BadRequestException;
//import com.harapansehat.hospital.repository.DokterRepository;
//import com.harapansehat.hospital.repository.PasienRepository;
//import com.harapansehat.hospital.repository.UserRepository;
//import com.harapansehat.hospital.service.DokterService;
//import com.harapansehat.hospital.service.PasienService;
//import com.harapansehat.hospital.service.ResepsionisService;
//import com.harapansehat.hospital.service.UserValidationService;
//import com.harapansehat.hospital.util.ExceptionMessageAccessor;
//import org.junit.Test;
//import org.junit.jupiter.api.BeforeEach;
//import org.mockito.InjectMocks;
//import org.mockito.Mock;
//import org.mockito.MockitoAnnotations;
//
//import static org.junit.jupiter.api.Assertions.assertEquals;
//import static org.junit.jupiter.api.Assertions.assertThrows;
//import static org.mockito.Mockito.*;
//
//public class UserValidationServiceTests {
//
//    @Mock
//    PasienRepository pasienRepository;
//
//    @Mock
//    UserValidationService userValidationService;
//
//    @Mock
//    UserRepository userRepository;
//
//    @Mock
//    ResepsionisService resepsionisService;
//
//    @InjectMocks
//    PasienService pasienService;
//
//    @InjectMocks
//    private DokterService dokterService;
//
//    @Mock
//    private DokterRepository dokterRepository;
//
//    @Mock
//    private ExceptionMessageAccessor exceptionMessageAccessor;
//
//    @BeforeEach
//    void setUp() {
//        MockitoAnnotations.openMocks(this);
//    }
//
//    static final String EXISTING_NIK = "123456789";
//    static final String EXISTING_USERNAME = "giri";
//    private static final String EXISTING_NIP = "12345";
//
//    @Test
//    public void testValidateDokterWithExistingNip() {
//        DokterRequest request = new DokterRequest();
//        request.setNip(EXISTING_NIP);
//
//        when(dokterRepository.existsByNip(EXISTING_NIP)).thenReturn(true);
//        when(exceptionMessageAccessor.getMessage(null, "NIP_ALREADY_EXISTS")).thenReturn("NIP already exists.");
//
//        BadRequestException exception = assertThrows(BadRequestException.class, () -> userValidationService.validateDokter(request));
//
//        verify(dokterRepository, times(1)).existsByNip(EXISTING_NIP);
//        verify(exceptionMessageAccessor, times(1)).getMessage(null, "NIP_ALREADY_EXISTS");
//        assertEquals("NIP already exists.", exception.getMessage());
//    }
//
//    @Test
//    public void testValidateDokterWithNonExistingNip() {
//        DokterRequest request = new DokterRequest();
//        request.setNip("newNip");
//
//        when(dokterRepository.existsByNip("newNip")).thenReturn(false);
//        userValidationService.validateDokter(request);
//        verify(dokterRepository, times(1)).existsByNip("newNip");
//    }
//
//
//    @Test
//    public void testValidatePasienWithExistingNik() {
//        PasienRequest request = new PasienRequest();
//        request.setNik(EXISTING_NIK);
//
//        when(pasienRepository.existsByNik(EXISTING_NIK)).thenReturn(true);
//
//        assertThrows(BadRequestException.class, () -> userValidationService.validatePasien(request));
//
//        verify(pasienRepository, times(1)).existsByNik(EXISTING_NIK);
//    }
//
//    @Test
//    public void testValidateResepsionisWithExistingUsername() {
//        ResepsionisRequest request = new ResepsionisRequest();
//        request.setUsername(EXISTING_USERNAME);
//
//        when(userRepository.existsByUsername(EXISTING_USERNAME)).thenReturn(true);
//        when(exceptionMessageAccessor.getMessage(null, "USERNAME_ALREADY_EXISTS")).thenReturn("Username already exists.");
//
//        BadRequestException exception = assertThrows(BadRequestException.class, () -> userValidationService.validateResepsionis(request));
//
//        verify(userRepository, times(1)).existsByUsername(EXISTING_USERNAME);
//        verify(exceptionMessageAccessor, times(1)).getMessage(null, "USERNAME_ALREADY_EXISTS");
//        assertEquals("Username already exists.", exception.getMessage());
//    }
//
//
//}
