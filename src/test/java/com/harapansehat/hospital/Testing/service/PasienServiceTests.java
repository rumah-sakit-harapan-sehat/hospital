package com.harapansehat.hospital.Testing.service;

import com.harapansehat.hospital.dto.pasien.PasienResponse;
import com.harapansehat.hospital.entity.Pasien;
import com.harapansehat.hospital.repository.PasienRepository;
import com.harapansehat.hospital.service.PasienService;
import com.harapansehat.hospital.service.UserService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

class PasienServiceTests {

    @Mock
    PasienService pasienService;

    @Mock
    UserService userService;

    @Mock
    PasienRepository pasienRepository;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }

//    @Test
//    void testGetPasien() {
//        List<Pasien> pasienList = new ArrayList<>();
//        Pasien pasien = new Pasien();
//        pasien.setId(1);
//        pasien.setNik("12345");
//        pasien.setNamaPasien("giri");
//        pasien.setJenisKelamin("laki-laki");
//        pasien.setAlamat("bogor");
//        pasien.setTanggalLahir("2004-07-04");
//        pasien.setIsDeleted(false);
//        pasienList.add(pasien);
//
//        when(pasienRepository.findAll()).thenReturn(pasienList);
//        List<PasienResponse> responseList = pasienService.getAllPasien();
//
//        assertFalse(responseList.isEmpty());
//
//        PasienResponse response = responseList.get(0);
//        assertNotNull(response);
//        assertEquals("12345", response.getNik());
//        assertEquals("giri", response.getNamaPasien());
//        assertEquals("laki-laki", response.getTanggalLahir());
//        assertEquals("bogor", response.getAlamat());
//        assertEquals("12345", response.getNik());
//        assertEquals("2004-07-04", response.getTanggalLahir());
//        assertEquals(false, response.getIsDeleted());
//    }

//    @Test
//    void testGetPasienById() {
//        Integer pasienId = 1;
//        Pasien expectedPasien = new Pasien();
//
//        when(pasienService.pasienExistValidation(pasienId)).thenReturn(expectedPasien);
//
//        Pasien actualPasien = pasienService.getPasienById(pasienId);
//
//        verify(pasienService).pasienExistValidation(pasienId);
//
//        assertEquals(expectedPasien, actualPasien);
//    }

//    @Test
//    void testPasienById() {
//        Integer pasienId = 1;
//        Pasien pasien = new Pasien();
//
//        when(pasienRepository.findById(pasienId)).thenReturn(Optional.of(pasien));
//
//        Pasien mockPasien = pasienService.getPasienById(pasienId);
//
//        verify(pasienRepository, times(1)).findById(pasienId);
//
//        assertEquals(pasienService, mockPasien);

//        Integer pasienId = 1;
//        Pasien pasien = new Pasien();
//        pasien.setId(pasienId);
//        pasien.setNik("12345");
//        pasien.setNamaPasien("giri");
//        pasien.setJenisKelamin("laki-laki");
//        pasien.setAlamat("bogor");
//        pasien.setTanggalLahir("2004-07-04");
//        pasien.setIsDeleted(false);
//
//        when(pasienRepository.findById(pasienId)).thenReturn(Optional.of(pasien));
//
//        Pasien mockResponse = pasienService.getPasienById(pasienId);
//
//        assertNotNull(mockResponse);
//        assertEquals(pasienId, mockResponse.getId());
//        assertEquals("12345", mockResponse.getNik());
//        assertEquals("giri", mockResponse.getNamaPasien());
//        assertEquals("laki-laki", mockResponse.getJenisKelamin());
//        assertEquals("bogor", mockResponse.getAlamat());
//        assertEquals("2004-07-04", mockResponse.getTanggalLahir());
//        assertEquals(false, mockResponse.getIsDeleted());
//    }

//    @Test
//    void testUpdatePasien() {
//        PasienRequest request = new PasienRequest();
//        Integer pasienId = 1;
//
//        Pasien pasien = new Pasien();
//        pasien.setId(pasienId);
//        pasien.setNik("12345");
//        pasien.setNamaPasien("giri");
//        pasien.setJenisKelamin("laki-laki");
//        pasien.setAlamat("bogor");
//        pasien.setTanggalLahir("2004-07-04");
//        pasien.setIsDeleted(false);
//
//        when(pasienRepository.findById(pasienId)).thenReturn(Optional.of(pasien));
//        when(pasienRepository.save(any(Pasien.class))).thenAnswer(invocation -> {
//            Pasien savePasien = invocation.getArgument(0);
//            savePasien.setId(1);
//            return savePasien;
//        });
//
//        PasienResponse mockResponse = pasienService.updatePasienById(pasienId, request);
//
//        assertNotNull(mockResponse);
//        assertEquals(pasienId, mockResponse.getId());
//        assertEquals(request.getNik(), mockResponse.getNik());
//        assertEquals(request.getNamaPasien(), mockResponse.getNamaPasien());
//        assertEquals(request.getAlamat(), mockResponse.getAlamat());
//        assertEquals(request.getJenisKelamin(), mockResponse.getJenisKelamin());
//        assertEquals(request.getTanggalLahir(), mockResponse.getTanggalLahir());
//        assertEquals(false, mockResponse.getIsDeleted());
//    }
//
//    @Test
//    void testDeletePasien() {
//        Integer pasienId = 1;
//        Pasien pasien = new Pasien();
//        pasien.setId(pasienId);
//        pasien.setNik("12345");
//        pasien.setNamaPasien("giri");
//        pasien.setJenisKelamin("laki-laki");
//        pasien.setAlamat("bogor");
//        pasien.setTanggalLahir("2004-07-04");
//        pasien.setIsDeleted(false);
//
//        when(pasienRepository.findById(pasienId)).thenReturn(Optional.of(pasien));
//        PasienResponse response = pasienService.deletePasienById(pasienId);
//
//        assertEquals(1, response.getId());
//    }
}
